<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class static_pages_controller extends Controller
{
    public function about_us(){
        return view('about_us');
    }
    public function gallery(){
        return view('gallery');
    }
    public function blog(){
        return view('blog');
    }
    public function contact(){
        return view('contact');
    }
    public function career(){
        return view('career');
    }
    public function products(){
        return view('products');
    }
    public function awards(){
        return view('awards');
    }
    public function team(){
        return view('team');
    }
    public function testimonials(){
        return view('testimonials');
    }
    public function iffalcon_32F2A(){
        return view('iffalcon_32F2A');
    }
    public function iffalcon_43k31(){
        return view('iffalcon_43k31');
    }
    public function vivo_v17(){
        return view('vivo_v17');
    }
    public function iffalcon_65v2a(){
        return view('iffalcon_65v2a');
    }
    public function iffalcon_50k31(){
        return view('iffalcon_50k31');
    }
    public function iffalcon_40f2a(){
        return view('iffalcon_40f2a');
    }
    public function iffalcon_32e3(){
        return view('iffalcon_32e3');
    }
    public function iffalcon_49f2a(){
        return view('iffalcon_49f2a');
    }
    public function iffalcon_55k31(){
        return view('iffalcon_55k31');
    }
    public function vivo_s1pro(){
        return view('vivo_s1pro');
    }
    public function vivo_y19(){
        return view('vivo_y19');
    }
    public function vivo_y91i(){
        return view('vivo_y91i');
    }
    public function vivo_y91i3gb(){
        return view('vivo_y91i3gb');
    }
    public function vivo_y15(){
        return view('vivo_y15');
    }
    public function vivo_v12(){
        return view('vivo_v12');
    }
    public function vivo_y11(){
        return view('vivo_y11');
    }
    public function vivo_s1(){
        return view('vivo_s1');
    }


    //micromax one_note

    public function micromax_onenote()
    {
        return view('micromax_one_note');
    }
    public function micromax_Q4002N()
    {
        return view('Q4002N_Bharat');
    }
    public function Q402bharat_2()
    {
        return view('Q402bharat_2');
    }
    public function Qbharat_5()
    {
        return view('Qbharat_5');
    }
    public function Q204bharat_4()
    {
        return view('Q204bharat_4');
    }
    public function N11()
    {
        return view('N11');
    }
    public function N12()
    {
        return view('N12');
    }
    public function X378()
    {
        return view('X378');
    }
    public function X388()
    {
        return view('X388');
    }
    public function X412(){
        return view('X412');
    }
    public function X419()
    {
        return view('X419');
    }
    public function X421()
    {
        return view('X421');
    }
    public function X744()
    {
        return view('X744');
    }
    public function X809()
    {
        return view('X809');
    }


    //Nikon  camera

    public function Dzoom_70()
    {
        return view('Nikon/DZoom_70');
    }
    public function nikon_D5600_1815()
    {
        return view('/Nikon/D3500_1815');
    }
    public function nikon_D5600_Dzoom()
    {
        return view('/Nikon/D5600_Dzoom');
    }
    public function nikon_D5600kit_18140()
    {
        return view('/Nikon/D5600kit_18140');
    }
    public function nikon_D5600kit_1855()
    {
        return view('/Nikon/D5600kit_1855');
    }
    public function nikon_D750()
    {
        return view('/Nikon/D750');
    }
    public function nikon_D850()
    {
        return view('/Nikon/D850');
    }
    public function nikon_Z6_2470()
    {
        return view('/Nikon/Z6_2470');
    }

    public function nikon_AF_Nikker70()
    {
        return view('/Nikon/AF_Nikker70');
    }
    public function nikon_AF_Nikker200()
    {
        return view('/Nikon/AF_Nikker200');
    }
    public function nikon_AF_50mm()
    {
        return view('/Nikon/AF_50mm');
    }
    public function nikon_AF_35mm()
    {
        return view('/Nikon/AF_35mm');
    }
  
}
