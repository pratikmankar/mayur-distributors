<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class contact_controller extends Controller
{
    private function on_fail($error_code = "UNKNOWN_ERROR", $msg)
    {
        $notification = array();
        $notification['status'] = false;
        $notification['message'] = $msg;
        $notification['text'] = "error";
        echo json_encode($notification);
    }

    private function on_success($response = array(), $msg)
    {
        $notification = array();
        $notification['status'] = true;
        $notification['message'] = $msg;
        $notification['text'] = "success";
        $notification['data'] = $response;
        echo json_encode($notification);
    }

    public function send_email(Request $request){

            $data=array();
            $data["Name"]=$request->c_name;
            $data["Email"]=$request->c_email;
            $data["Phone"]=$request->c_phone;
            $data["Message"]=$request->c_message;

            $email_id=env('MAIL_USERNAME');

            $name= $data["Name"];
            Mail::send('email', $data, function($message) use ($email_id, $name) {
          
            $message->to($email_id, $name)->subject('Enquiry');

        });

        if (Mail::failures())
        {
            $msg = "Something went wrong. Please try again.";
            $this->on_fail('', $msg);
         }
         else
         {
           $msg = "Your Message has been sent! We will contact you shortly.";
           $this->on_success('', $msg);
         }


    }

   
}