<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/about-us', function () {
    return view('about_us');
});
Route::get('/about-us', 'static_pages_controller@about_us');
Route::get('/gallery', 'static_pages_controller@gallery');
Route::get('/blog', 'static_pages_controller@blog');
Route::get('/contact-us', 'static_pages_controller@contact');
Route::get('/career', 'static_pages_controller@career');
Route::get('/products', 'static_pages_controller@products');
Route::get('/awards', 'static_pages_controller@awards');
Route::get('/team', 'static_pages_controller@team');
Route::get('/testimonials', 'static_pages_controller@testimonials');
Route::get('/404', 'static_pages_controller@pagenotfound');
Route::post('/send_email', 'contact_controller@send_email');

Route::get('/iffalcon-32F2A', 'static_pages_controller@iffalcon_32F2A');
Route::get('/iffalcon-43k31', 'static_pages_controller@iffalcon_43k31');
Route::get('/iffalcon-65v2a', 'static_pages_controller@iffalcon_65v2a');
Route::get('/iffalcon-50k31', 'static_pages_controller@iffalcon_50k31');
Route::get('/iffalcon-40f2a', 'static_pages_controller@iffalcon_40f2a');
Route::get('/iffalcon-32e3', 'static_pages_controller@iffalcon_32e3');
Route::get('/iffalcon-49f2a', 'static_pages_controller@iffalcon_49f2a');
Route::get('/iffalcon-55k31', 'static_pages_controller@iffalcon_55k31');
Route::get('/iffalcon-65v2a', 'static_pages_controller@iffalcon_65v2a');

Route::get('/vivo-v17', 'static_pages_controller@vivo_v17');
Route::get('/vivo-s1', 'static_pages_controller@vivo_s1');
Route::get('/vivo-s1pro', 'static_pages_controller@vivo_s1pro');
Route::get('/vivo-y19', 'static_pages_controller@vivo_y19');
Route::get('/vivo-y91i', 'static_pages_controller@vivo_y91i');
Route::get('/vivo-y91i3gb', 'static_pages_controller@vivo_y91i3gb');
Route::get('/vivo-y15', 'static_pages_controller@vivo_y15');
Route::get('/vivo-y12', 'static_pages_controller@vivo_v12');
Route::get('/vivo-y11', 'static_pages_controller@vivo_y11');


//micromax products

Route::get('/micromax-onenote', 'static_pages_controller@micromax_onenote');
Route::get('/micromax-Q4002N', 'static_pages_controller@micromax_Q4002N');
Route::get('/Q402bharat-2', 'static_pages_controller@Q402bharat_2');

Route::get('/micromax-Qbharat-5', 'static_pages_controller@Qbharat_5');
Route::get('/micromax-Q204bharat-4', 'static_pages_controller@Q204bharat_4');
Route::get('/micromax-N11', 'static_pages_controller@N11');

Route::get('/micromax-N12', 'static_pages_controller@N12');
Route::get('/micromax-X378', 'static_pages_controller@X378');
Route::get('/micromax-X388', 'static_pages_controller@X388');


Route::get('/micromax-X412', 'static_pages_controller@X412');
Route::get('/micromax-X419', 'static_pages_controller@X419');
Route::get('/micromax-X421', 'static_pages_controller@X421');


Route::get('/micromax-X744', 'static_pages_controller@X744');
Route::get('/micromax-X809', 'static_pages_controller@X809');


//Nikon

Route::get('/nikon-Dzoom70', 'static_pages_controller@Dzoom_70');
Route::get('/nikon-D5600-1815', 'static_pages_controller@nikon_D5600_1815');
Route::get('/nikon-D5600-Dzoom', 'static_pages_controller@nikon_D5600_Dzoom');
Route::get('/nikon-D5600kit-18140', 'static_pages_controller@nikon_D5600kit_18140');
Route::get('/nikon-D5600kit-1855', 'static_pages_controller@nikon_D5600kit_1855');
Route::get('/nikon-D750','static_pages_controller@nikon_D750');
Route::get('/nikon-D850','static_pages_controller@nikon_D850');
Route::get('/nikon-Z62470','static_pages_controller@nikon_Z6_2470');
Route::get('/nikon-AF-Nikker70','static_pages_controller@nikon_AF_Nikker70');
Route::get('/nikon-AF-Nikker200','static_pages_controller@nikon_AF_Nikker200');
Route::get('/nikon-AF-50mm','static_pages_controller@nikon_AF_50mm');
Route::get('/nikon-AF-35mm','static_pages_controller@nikon_AF_35mm');