@extends('layouts.layout')

@section('title')
2019 Best Distributor In Central Zone | Mayur Distributors
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="2019 Best Distributor In Central Zone | Mayur Distributors">
<meta name="description" content="In 2019 Mayur Distributors has been Awarded Best Distributor In Central Zone-Maharashtra By Vivo.First Price In Successful Launch Of Flagship Model V15pro | Entire S1-Series. etc">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors, distributors in Pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors, bbest distributor in maharashtra">
<link rel="canonical" href="{{url('/awards')}}">
<meta property="og:title" content="2019 Best Distributor In Central Zone | Mayur Distributors">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/awards">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Best Distributor Maharashtra">
<meta property="og:description"content="In 2019 Mayur Distributors has been Awarded Best Distributor In Central Zone-Maharashtra By Vivo.First Price In Successful Launch Of Flagship Model V15pro | Entire S1-Series. etc">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-48 line__height-68 font__family-montserrat">
                            Testimonials
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Testimonials</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <main class="main-container">
      
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="container mt-40 ">
                            <div class="mt-20">
                                <div class="row align-items-center justify-content-center" data-brk-library="component__image_frames">
                                    <div class="col-lg-4">
                                        <div class="text-center text-lg-left">
                                           
                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/testimonials/1.png')}}" alt="alt" class="lazyload">
                                         
                                        </div>
                                    </div>
                                    <div class="col-lg-8 text-center text-lg-left mt-30 mt-lg-0">
                                        <div class="align-items-right text-right" style="position: absolute; right:0; top:0px;">
                                            <h3>
                                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                                            </h3>
                                        </div>
                                        <div class="ml-30">
                                          
                                            <p class="font__family-open-sans font__size-22 line__height-26 mt-20 text-gray-light">
                                                For last many years I am into mobile business and have dealt with many mobile brand distributors. In all those distributors, Harsh Communication is undoubtedly one of the best distributors in Mobile Industry. Their services and policies are extremely good. Keep up the good work.

                                            </p>
                                            <h4 class="font__family-montserrat text-right font__weight-bold font__size-28 line__height-32 text-uppercase mt-30 text-gray-dark">
                                            Mr. Baljinder Singh
                                            </h4>
                                            <h5 class="font__family-montserrat text-right font__weight-bold font__size-16 line__height-32 text-captialize text-gray-dark">
                                          Sona Mobiles, Pune
                                            </h5>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
        </section>

	<section class="mt-50">
				
				<div class="position-relative pb-60 pt-50" style="background: url({{URL::to('public/img/bg-1920_1.jpg')}}) no-repeat center;background-size: cover;">
					<div class="brk-abs-bg-overlay brk-base-bg-gradient-right"></div>
					<div class="container position-relative z-index-3">
						<div class="font__family-montserrat font__weight-ultralight font__size-36 line__height-42 text-uppercase pb-15 text-white" style="border-bottom: 1px solid rgba(var(--white-rgb), 0.2);">Our Reviews
						</div>
					</div>
					<div class="brk-testimonials-double" data-brk-library="component__testimonials,slider__swiper">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="brk-testimonials-double__slider lazyload double-white" data-bg="{{URL::to('public/img/bg-1920_1.jpg')}}">
										<div class="double-pagination"></div>
										<div class="double-slider swiper-container">
											<div class="swiper-wrapper">
												<div class="swiper-slide">
													<div class="brk-testimonials-double__item">
                                                        <div class="row">
                                                     
                                                            <div class="col-lg-5 col-md-12">
                                                                <div class="mt-10" style="overflow:hidden; border-radius:20px; box-shadow: 0px 0px 10px #cfcfcf;">
                                                                    <iframe width="100%" height="240" src="https://www.youtube.com/embed/oO78bc3FGwg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7 col-md-12">
                                                                <div class="font__family-montserrat letter-spacing-20 text-left text-lg-right mt-4">
                                                                    <p>
                                                                        We are among the leading multi-mobile retailer in Pune City. We are dealing with many mobile brands distributors. I am associated with Mayur group for more than 15 years and It has been great experience in working with this group. We have received great support from this team and from Sharad Shah in particular. He is indeed an extra ordinary person.
                                                                    </p>
                                                                </div>
                                                                <div class="brk-testimonials-double__contant float-right mt-4">
                                                                    <div class="brk-testimonials-double__name text-right mr-3">
                                                                        <div class="brk-rating float-right mb-3" data-brk-library="component__elements">
                                                                            <div class="brk-rating__layer brk-dark-font-color">
                                                                                <i class="fal fa-star"></i>
                                                                                <i class="fal fa-star"></i>
                                                                                <i class="fal fa-star"></i>
                                                                                <i class="fal fa-star"></i>
                                                                                <i class="fal fa-star"></i>
                                                                            </div>
                                                                            <div class="brk-rating__imposition brk-base-font-color" style="width: 100%">
                                                                                <div class="visible">
                                                                                    <i class="fas fa-star"></i>
                                                                                    <i class="fas fa-star"></i>
                                                                                    <i class="fas fa-star"></i>
                                                                                    <i class="fas fa-star"></i>
                                                                                    <i class="fas fa-star"></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <strong class="font__family-montserrat font__weight-semibold">Manik Mobiles</strong>
                                                                        <span>Pune</span>
                                                                    </div>
                                                                    <div class="brk-testimonials-double__photo lazyload" data-bg="{{URL::to('public/img/testimonials/1.jpg')}}"></div>

                                                                </div>
                                                            </div>    

                                                        </div>
													
													</div>
                                                </div>
                                                <div class="swiper-slide">
													<div class="brk-testimonials-double__item">
                                                        <div class="row">
                                                     
                                                            <div class="col-lg-7 col-md-12">
                                                                <div class="font__family-montserrat letter-spacing-20 text-lg-left mt-4">
                                                                    <p>
                                                                        
                                                                        
                            I have been associated with Harsh Communication, Vivo since 2015. I have received enormous support from Harsh Communication in growing my business. Their sales staff has been very friendly and has adjusted with us well in terms of timely stock support and other services. 
                        
                                                                    </p>
                                                                </div>
                                                                <div class="brk-testimonials-double__contant mt-5">
                                                                    <div class="brk-testimonials-double__photo lazyload" data-bg="{{URL::to('public/img/testimonials/1.jpg')}}"></div>

                                                                    <div class="brk-testimonials-double__name mr-3">
                                                                        <div class="brk-rating mb-3" data-brk-library="component__elements">
                                                                            <div class="brk-rating__layer brk-dark-font-color">
                                                                                <i class="fal fa-star"></i>
                                                                                <i class="fal fa-star"></i>
                                                                                <i class="fal fa-star"></i>
                                                                                <i class="fal fa-star"></i>
                                                                                <i class="fal fa-star"></i>
                                                                            </div>
                                                                            <div class="brk-rating__imposition brk-base-font-color" style="width: 100%">
                                                                                <div class="visible">
                                                                                    <i class="fas fa-star"></i>
                                                                                    <i class="fas fa-star"></i>
                                                                                    <i class="fas fa-star"></i>
                                                                                    <i class="fas fa-star"></i>
                                                                                    <i class="fas fa-star"></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <strong class="font__family-montserrat font__weight-semibold">Zubeir Shaikh</strong>
                                                                        <span>New Cell 4 You, Pune</span>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>    

                                                            
                                                            <div class="col-lg-5 col-md-12">
                                                                <div class="mt-10" style="overflow:hidden; border-radius:20px; box-shadow: 0px 0px 10px #cfcfcf;">
                                                                    <iframe width="100%" height="240" src="https://www.youtube.com/embed/oO78bc3FGwg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                                </div>
                                                            </div>

                                                        </div>
													
													</div>
												</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</section>

<section class="pb-2">
    <div class="container mb-80 pt-0 mt-80">
   
        <div class="brk-testimonials-dash-two" data-brk-library="component__testimonials,slider__swiper">
            <div class="dash-two-pagination"></div>
            <div class="swiper-container dash-two-slider">
                <div class="swiper-wrapper">
                   
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/1.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            I have been associated with Harsh Communication, Vivo since 2015. I have received enormous support from Harsh Communication in growing my business. Their sales staff has been very friendly and has adjusted with us well in terms of timely stock support and other services. 
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Zubeir Shaikh,</p>
                        <p class="font__family-open-sans text-center text-lg-left">New Cell 4 You<br> Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/2.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            Its been over 4 years we are associated with Harsh Communication. Never we had any loose or bad experience regarding any services. With their professional and holistic approach, they have been very supportive and helpful.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">New Sai Mobile</p>
                        <p class="font__family-open-sans text-center text-lg-left">Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/3.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            For last many years I am into mobile business and have dealt with many mobile brand distributors. In all those distributors, Harsh Communication is undoubtedly one of the best distributors in Mobile Industry. Their services and policies are extremely good. Keep up the good work.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Baljinder Singh</p>
                        <p class="font__family-open-sans text-center text-lg-left">Sona Mobile<br> Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/2.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            We are among the leading multi-mobile retailer in Pune City. We are dealing with many mobile brands distributors. I am associated with Mayur group for more than 15 years and It has been great experience in working with this group. We have received great support from this team and from Sharad Shah in particular. He is indeed an extra ordinary person.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Manik Mobile</p>
                        <p class="font__family-open-sans text-center text-lg-left">Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/1.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            We are associated with Mayur Distributors for last 15 Years. They have supported us in all respects. We are thankful to entire team to Mayur Distributors.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Goa Beverages</p>
                        <p class="font__family-open-sans text-center text-lg-left">Sachin Pai<br> North Goa</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/people/testimonials/2.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            Mayur Distributors has very co-operative and systematic working style. It’s a pleasure to work with Mayur Distributors.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Awdita Entreprises</p>
                        <p class="font__family-open-sans text-center text-lg-left"> Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/people/testimonials/5.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            Very supportive Firm & all Firm staff. Proud to be associated with & looking forward to work with them.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">M/s. Lakade</p>
                        <p class="font__family-open-sans text-center text-lg-left">Mungurwadi and Co.<br> Kolhapur</p>
                   
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    </main>
</div>
@endsection