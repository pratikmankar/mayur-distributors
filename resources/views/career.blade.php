@extends('layouts.layout')

@section('title')
Career at Mayur Distributors | A Distributors company of consumer goods
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="Career at Mayur Distributors | A Distributors company of consumer goods">
<meta name="description" content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors, distributors in Pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors">
<link rel="canonical" href="{{url('/career')}}">
<meta property="og:title" content="Career at Mayur Distributors | A Distributors company of consumer goods">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/career">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Mayur Distributors">
<meta property="og:description"content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-48 line__height-68 font__family-montserrat">
                            Career
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Career</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <section class="pt-50">
        <div class="container">
         
            <div class="text-center mb-50">
                <h5 class="font__family-montserrat font__weight-light text-uppercase font__size-18 text-blue" data-brk-library="component__title">Be a part of our team.</h5>
                <h2 class="font__family-roboto font__weight-thin line__height-60 font__size-56 text-uppercase letter-spacing-60 mt-20" data-brk-library="component__title">Our vacancies</h2>
            </div>
            <div class="row">
                <div class="col-md-6 col-xl-3">
                    <figure class="shape-box shape-box_half" data-brk-library="component__shape_box">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/300x460_1.jpg')}}" alt="vivo mobiles distributors" class="lazyload">
                        <div class="brk-abs-overlay z-index-0 bg-black opacity-60"></div>
                        <figcaption>
                            <div class="show-cont">
                                <h3 class="font__family-montserrat font__size-36 brk-base-font-color">01 </h3>
                                <h4 class="font__family-montserrat font__weight-bold font__size-24 text-uppercase main-title">Post Name </h4>
                            </div>
                            <p class="font__family-open-sans font__size-14 text-gray">Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor consequat </p>
                            <a href="#" class="btn btn-inside-out btn-lg border-radius-30 font__weight-bold" data-brk-library="component__button">
                                <span class="before">Apply Now!</span><span class="text">Click Me</span><span class="after">Apply Now!</span>
                            </a>
                        </figcaption>
                        <span class="after"></span>
                    </figure>
                </div>
                <div class="col-md-6 col-xl-3">
                    <figure class="shape-box shape-box_half" data-brk-library="component__shape_box">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/300x460_2.jpg')}}" alt="distributors in Pune" class="lazyload">
                        <div class="brk-abs-overlay z-index-0 bg-black opacity-60"></div>
                        <figcaption>
                            <div class="show-cont">
                                <h3 class="font__family-montserrat font__size-36 brk-base-font-color">02 </h3>
                                <h4 class="font__family-montserrat font__weight-bold font__size-24 text-uppercase main-title">Post Name </h4>
                            </div>
                            <p class="font__family-open-sans font__size-14 text-gray">Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor consequat </p>
                            <a href="#" class="btn btn-inside-out btn-lg border-radius-30 font__weight-bold" data-brk-library="component__button">
                                <span class="before">Apply Now!</span><span class="text">Click Me</span><span class="after">Apply Now!</span>
                            </a>
                        </figcaption>
                        <span class="after"></span>
                    </figure>
                </div>
                <div class="col-md-6 col-xl-3">
                    <figure class="shape-box shape-box_half" data-brk-library="component__shape_box">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/300x460_3.jpg')}}" alt="telecom service distributors" class="lazyload">
                        <div class="brk-abs-overlay z-index-0 bg-black opacity-60"></div>
                        <figcaption>
                            <div class="show-cont">
                                <h3 class="font__family-montserrat font__size-36 brk-base-font-color">03 </h3>
                                <h4 class="font__family-montserrat font__weight-bold font__size-24 text-uppercase main-title">Post Name </h4>
                            </div>
                            <p class="font__family-open-sans font__size-14 text-gray">Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor consequat </p>
                            <a href="#" class="btn btn-inside-out btn-lg border-radius-30 font__weight-bold" data-brk-library="component__button">
                                <span class="before">Apply Now!</span><span class="text">Click Me</span><span class="after">Apply Now!</span>
                            </a>
                        </figcaption>
                        <span class="after"></span>
                    </figure>
                </div>
                <div class="col-md-6 col-xl-3">
                    <figure class="shape-box shape-box_half" data-brk-library="component__shape_box">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/300x460_1.jpg')}}" alt="Electronic Products" class="lazyload">
                        <div class="brk-abs-overlay z-index-0 bg-black opacity-60"></div>
                        <figcaption>
                            <div class="show-cont">
                                <h3 class="font__family-montserrat font__size-36 brk-base-font-color">04 </h3>
                                <h4 class="font__family-montserrat font__weight-bold font__size-24 text-uppercase main-title">Post Name </h4>
                            </div>
                            <p class="font__family-open-sans font__size-14 text-gray">Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor consequat </p>
                            <a href="#" class="btn btn-inside-out btn-lg border-radius-30 font__weight-bold" data-brk-library="component__button">
                                <span class="before">Apply Now!</span><span class="text">Click Me</span><span class="after">Apply Now!</span>
                            </a>
                        </figcaption>
                        <span class="after"></span>
                    </figure>
                </div>
                <div class="col-md-6 col-xl-3">
                    <figure class="shape-box shape-box_half" data-brk-library="component__shape_box">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/300x460_1.jpg')}}" alt="consumer goods company" class="lazyload">
                        <div class="brk-abs-overlay z-index-0 bg-black opacity-60"></div>
                        <figcaption>
                            <div class="show-cont">
                                <h3 class="font__family-montserrat font__size-36 brk-base-font-color">05 </h3>
                                <h4 class="font__family-montserrat font__weight-bold font__size-24 text-uppercase main-title">Post Name </h4>
                            </div>
                            <p class="font__family-open-sans font__size-14 text-gray">Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor consequat </p>
                            <a href="#" class="btn btn-inside-out btn-lg border-radius-30 font__weight-bold" data-brk-library="component__button">
                                <span class="before">Apply Now!</span><span class="text">Click Me</span><span class="after">Apply Now!</span>
                            </a>
                        </figcaption>
                        <span class="after"></span>
                    </figure>
                </div>
                <div class="col-md-6 col-xl-3">
                    <figure class="shape-box shape-box_half" data-brk-library="component__shape_box">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/300x460_2.jpg')}}" alt="consumer goods company in India" class="lazyload">
                        <div class="brk-abs-overlay z-index-0 bg-black opacity-60"></div>
                        <figcaption>
                            <div class="show-cont">
                                <h3 class="font__family-montserrat font__size-36 brk-base-font-color">06 </h3>
                                <h4 class="font__family-montserrat font__weight-bold font__size-24 text-uppercase main-title">Post Name </h4>
                            </div>
                            <p class="font__family-open-sans font__size-14 text-gray">Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor consequat </p>
                            <a href="#" class="btn btn-inside-out btn-lg border-radius-30 font__weight-bold" data-brk-library="component__button">
                                <span class="before">Apply Now!</span><span class="text">Click Me</span><span class="after">Apply Now!</span>
                            </a>
                        </figcaption>
                        <span class="after"></span>
                    </figure>
                </div>
                <div class="col-md-6 col-xl-3">
                    <figure class="shape-box shape-box_half" data-brk-library="component__shape_box">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/300x460_3.jpg')}}" alt="consumer goods" class="lazyload">
                        <div class="brk-abs-overlay z-index-0 bg-black opacity-60"></div>
                        <figcaption>
                            <div class="show-cont">
                                <h3 class="font__family-montserrat font__size-36 brk-base-font-color">07 </h3>
                                <h4 class="font__family-montserrat font__weight-bold font__size-24 text-uppercase main-title">Post Name </h4>
                            </div>
                            <p class="font__family-open-sans font__size-14 text-gray">Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor consequat </p>
                            <a href="#" class="btn btn-inside-out btn-lg border-radius-30 font__weight-bold" data-brk-library="component__button">
                                <span class="before">Apply Now!</span><span class="text">Click Me</span><span class="after">Apply Now!</span>
                            </a>
                        </figcaption>
                        <span class="after"></span>
                    </figure>
                </div>
                <div class="col-md-6 col-xl-3">
                    <figure class="shape-box shape-box_half" data-brk-library="component__shape_box">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/300x460_1.jpg')}}" alt="mayur distributors" class="lazyload">
                        <div class="brk-abs-overlay z-index-0 bg-black opacity-60"></div>
                        <figcaption>
                            <div class="show-cont">
                                <h3 class="font__family-montserrat font__size-36 brk-base-font-color">08 </h3>
                                <h4 class="font__family-montserrat font__weight-bold font__size-24 text-uppercase main-title">Post Name </h4>
                            </div>
                            <p class="font__family-open-sans font__size-14 text-gray">Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor consequat </p>
                            <a href="#" class="btn btn-inside-out btn-lg border-radius-30 font__weight-bold" data-brk-library="component__button">
                                <span class="before">Apply Now!</span><span class="text">Click Me</span><span class="after">Apply Now!</span>
                            </a>
                        </figcaption>
                        <span class="after"></span>
                    </figure>
                </div>

            </div>
        </div>
    </section>
</div>
@endsection