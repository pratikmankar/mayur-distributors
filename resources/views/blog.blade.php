@extends('layouts.layout')

@section('title')
Smart Led Tv, Consumer Goods, Electronic Products Distributors in Pune 
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="Smart Led Tv, Consumer Goods, Electronic Products Distributors in Pune">
<meta name="description" content="Mayur Distributors an award-winning distributors company in Pune for their services, our core services on Nikon India, Television, Vivo Mobiles, Micromax Mobiles, Tata Chemicals, IFFalcon Televisions by TCL. ✓Get a Free Quote Today 020-26430632">
<meta name="keywords" content="vivo mobiles distributors, Vivo, iFFalcon by TCL, led tv, smart led  tv, micromax mobiles distributors, Micromax, Nikon, nikon India, nikon distributors, Tata Chemicals, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors">
<link rel="canonical" href="{{url('/blog')}}">
<meta property="og:title" content="Smart Led Tv, Consumer Goods, Electronic Products Distributors in Pune">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/blog">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Mayur distributors iFFalcon tv by TCL">
<meta property="og:description"content="Mayur Distributors an award-winning distributors company in Pune for their services, our core services on Nikon India, Television, Vivo Mobiles, Micromax Mobiles, Tata Chemicals, IFFalcon Televisions by TCL. ✓Get a Free Quote Today 020-26430632">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-48 line__height-68 font__family-montserrat">
                            Blog
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Blog</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <main class="main-container">
        <div class="container mt-80">
            <div class="row">
                <div class="col-12 col-lg-8 col-xl-9 order-xs-2 order-lg-1">
                    <div class="brs-posts-container brs-posts-container_right-divider">
                        <div class="mb-60">
                            <div class="brs-post brs-post_slider" data-brk-library="component__blog">
                                <div class="brs-post__body">
                                    <div class="brs-post__slider-container">
                                        <div class="brs-post__slider-item">
                                            <img src="{{URL::to('public/img/blog/slider-post.png')}}" alt="telecom service distributors" class="brs-post__img">
                                            <div class="brs-post__overlay"></div>
                                        </div>
                                        <div class="brs-post__slider-item">
                                            <img src="{{URL::to('public/img/blog/hosted-video.png')}}" alt="distributors in Pune" class="brs-post__img">
                                            <div class="brs-post__overlay"></div>
                                        </div>
                                    </div>
                                    <div class="brs-post__info-wrapper brs-post__info-wrapper-bg">
                                        <div class="brs-post__information font__family-montserrat font__weight-semibold font__size-13 line__height-14">
                                            <a href="#" class="brs-post__date">
                                                <i class="far fa-clock brs-post__date-icon"></i>Aug 12, 17
                                            </a>
                                            <a href="#" class="brs-post__comments">
                                                <i class="far fa-comments brs-post__comments-icon"></i> 12
                                            </a>
                                        </div>
                                        <h2 class="brs-post__title font__family-montserrat font__size-24 font__weight-bold line__height-28 text-left">
                                            Some large title of Slider Post</h2>
                                    </div>
                                </div>
                                <div class="brs-post__description">
                                    <p class="brs-post__description__paragraph text-left font__family-open-sans font__size-16 font__weight-normal line__height-26">
                                        Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, nsequ vitae, eleifend ac, enim. Aliquam lorem ante, dapibus
                                        in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laeers. Quisque rutrum. Aenean imperdiet.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="mb-60">
                            <div class="brs-post brs-post_simple" data-brk-library="component__blog_css">
                                <div class="brs-post__img-container">
                                    <img src="{{URL::to('public/img/blog/simple-post.png')}}" alt="Electronic Products" class="brs-post__img">
                                </div>
                                <div class="brs-post__body">
                                    <div class="brs-post__information font__family-montserrat font__weight-semibold font__size-13 line__height-14">
                                        <a href="#" class="brs-post__date">
                                            <i class="far fa-clock brs-post__date-icon"></i>Aug 12, 17
                                        </a>
                                        <a href="#" class="brs-post__comments">
                                            <i class="far fa-comments brs-post__comments-icon"></i> 12
                                        </a>
                                    </div>
                                    <h2 class="brs-post__title font__family-montserrat font__size-24 font__weight-bold line__height-28 text-left">
                                        Some large title for simple post</h2>
                                    <p class="brs-post__paragraph text-left font__family-open-sans font__size-16 font__weight-normal line__height-26">
                                        Aenean vulputate eleifend tellus. Aenean leo ligula, ttitor eu, nsequ vitae, eleifend ac, enim. Aliquam
                                        lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laeers.
                                        Quisque rutrum. Aenean imperdiet.
                                    </p>
                                    <a href="#" class="brs-post__more font__family-montserrat font__size-14 font__weight-normal line__height-26 text-left">Read
                                        More</a>
                                </div>
                            </div>
                        </div>
                        <div class="mb-60">
                            <div class="brk-hosted-video" data-brk-library="component__media_embeds,fancybox">
                                <div class="brk-abs-bg-overlay brk-hosted-video__before brk-base-bg-gradient-50deg-opacity-28">
                                </div>
                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/hosted-video.png')}}" alt="mayur distributors" class="brk-hosted-video__img lazyload">
                                <div class="brk-hosted-video__body">
                                    <a class="icon__btn icon__btn_reverse icon__btn-circled icon__btn-lg fancybox brk-hosted-video__btn" href="http://dev.nikadevs.com/berserk-data/Placeholder_Video.mp4" data-brk-library="component__button">
                                        <span class="before"></span>
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                        <span class="after"></span>
                                    </a>
                                    <div class="brk-hosted-video__description">
                                        <div class="brk-hosted-video__about">
                                            <div class="brk-hosted-video__information font__family-montserrat font__weight-semibold font__size-13 line__height-14">
                                                <a href="#" class="brk-hosted-video__date brk-dark-font-color-3">
                                                    <i class="far fa-clock brk-hosted-video__date-icon"></i>Aug 12, 17
                                                </a>
                                                <a href="#" class="brk-hosted-video__comments brk-dark-font-color-3">
                                                    <i class="far fa-comments brk-hosted-video__comments-icon"></i> 12
                                                </a>
                                            </div>
                                            <h2 class="brk-hosted-video__title font__family-montserrat font__size-24 font__weight-bold line__height-28 text-lg-left text-center">Hosted Video</h2>
                                        </div>
                                        <p class="brk-hosted-video__paragraph text-left font__family-open-sans font__size-16 font__weight-normal line__height-26">Aenean vulputate eleifend tellus. Aenean leo ligula, ttitor eu, nsequ vitae, eleifend ac, enim. Aliquam lorem ante,
                                            dapibus in, viverra.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="mb-60">
                            <div class="brs-post brs-post_video" data-brk-library="component__blog_css">
                                <div class="brs-post__video-container">
                                    <iframe id="ytplayer" src="http://www.youtube.com/embed/AFtUpMTs4vI?autoplay=0&showinfo=0&origin=http://example.com" allowfullscreen class="brs-post__video">
                                    </iframe>
                                </div>
                                <div class="brs-post__about">
                                    <div class="brs-post__information font__family-montserrat font__weight-semibold font__size-13 line__height-14">
                                        <a href="#" class="brs-post__date">
                                            <i class="far fa-clock brs-post__date-icon"></i>Aug 12, 17</a>
                                        <a href="#" class="brs-post__comments">
                                            <i class="far fa-comments brs-post__comments-icon"></i> 12</a>
                                    </div>
                                    <h2 class="brs-post__title font__family-montserrat font__size-24 font__weight-bold line__height-28 text-left">
                                        Some large title of Slider Post</h2>
                                    <a href="#" class="brs-post__more font__family-montserrat font__size-14 font__weight-normal line__height-26 text-left">Read More
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="mb-60">
                            <div class="brs-post brs-post_quote" data-brk-library="component__blog_css">
                                <img class="brs-post__img" alt="vivo mobiles distributors" src="{{URL::to('public/img/blog/quote-post-1.png')}}">
                                <div class="brs-post__body">
                                    <div class="brs-post__information">
                                        <a href="#" class="brs-post__date font__family-montserrat font__weight-semibold font__size-13 line__height-14">
                                            <i class="far fa-clock brs-post__date-icon"></i>Aug 12, 17</a>
                                        <a href="#" class="brs-post__comments font__family-montserrat font__weight-semibold font__size-13 line__height-14">
                                            <i class="far fa-comments brs-post__comments-icon"></i> 12</a>
                                    </div>
                                    <p class="brs-post__paragraph text-left font__family-montserrat font__size-24 font__weight-normal line__height-32">
                                        Quote post. Phasellus viverra nulla laeers.
                                        <span class="line-through font__family-playfair font__style-italic line__height-28">
                                            Quisque enean impe imperdiet</span> Poettitor (
                                        <span class="font__family-open-sans font__weight-bold font__style-italic accent-1 letter-spacing--20">eu, nsequ vitae, eleifend ac </span>) enim. Aliquam lore ante,
                                        <span class="accent-2">dapibus in</span>, viverra quis, feugiat a.</p>
                                    <a href="#" class="brs-post__more font__family-montserrat font__size-14 font__weight-normal line__height-26 text-left">Read More
                                    </a>
                                </div>
                                <a class="brs-post__link brk-base-bg-gradient-0deg-a" href="#">
                                    <i class="fal fa-paperclip brs-post__link-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="mb-60">
                            <div class="brs-post brs-post_video" data-brk-library="component__blog_css">
                                <div class="brs-post__video-container">
                                    <iframe src="https://player.vimeo.com/video/49072365" class="brs-post__video" allowfullscreen>
                                    </iframe>
                                </div>
                                <div class="brs-post__about">
                                    <div class="brs-post__information font__family-montserrat font__weight-semibold font__size-13 line__height-14">
                                        <a href="#" class="brs-post__date">
                                            <i class="far fa-clock brs-post__date-icon"></i>Aug 12, 17</a>
                                        <a href="#" class="brs-post__comments">
                                            <i class="far fa-comments brs-post__comments-icon"></i> 12</a>
                                    </div>
                                    <h2 class="brs-post__title font__family-montserrat font__size-24 font__weight-bold line__height-28 text-left">
                                        Some large title of Slider Post</h2>
                                    <a href="#" class="brs-post__more font__family-montserrat font__size-14 font__weight-normal line__height-26 text-left">Read More
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="mt-80 mb-50">
                            <div class="brk-pagination brk-pagination-seven font__family-open-sans" data-brk-library="component__pagination">
                                <nav class="navigation pagination" role="navigation">
                                    <h2 class="screen-reader-text">Навигация по записям</h2>
                                    <div class="nav-links">
                                        <a class="prev page-numbers" href="#">Предыдущая страница</a>
                                        <a class="page-numbers" href="javascript:void(0)"><span class="meta-nav screen-reader-text">Страница </span>1</a>
                                        <span class="page-numbers current"><span class="meta-nav screen-reader-text">Страница </span>2</span>
                                        <a class="page-numbers" href="javascript:void(0)"><span class="meta-nav screen-reader-text">Страница </span>3</a>
                                        <a class="page-numbers" href="javascript:void(0)"><span class="meta-nav screen-reader-text">Страница </span>4</a>
                                        <a class="page-numbers" href="javascript:void(0)"><span class="meta-nav screen-reader-text">Страница </span>5</a>
                                        <a class="page-numbers" href="javascript:void(0)"><span class="meta-nav screen-reader-text">Страница </span>6</a>
                                        <span class="page-numbers dots">&hellip;</span>
                                        <a class="page-numbers" href="javascript:void(0)"><span class="meta-nav screen-reader-text">Страница </span>7</a>
                                        <a class="next page-numbers" href="#">Следующая страница</a>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 col-xl-3 order-xs-1 order-lg-2">
                    <div class="brs-sidebar brs-sidebar_right" data-brk-library="component__widgets">
                        <div class="brs-sidebar__title">
                            <h3 class="font__family-montserrat font__size-21 font__weight-bold line__height-22">Search</h3>
                        </div>
                        <form class="brk-sidebar-search" role="search" method="get" action="">
                            <input type="search" id="" class="brk-sidebar-search__input" name="s" maxlength="50" value="" placeholder="Search">
                            <button type="submit" class="brk-sidebar-search__submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </form>
                        <div class="brs-sidebar__title">
                            <i class="fab fa-hotjar brs-sidebar__title-icon"></i>
                            <h3 class="font__family-montserrat font__size-21 font__weight-bold line__height-22">HOT News</h3>
                        </div>
                        <div class="brs-carousel brs-carousel_news" data-brk-library="slider__slick">
                            <div class="brs-post brs-post_mini-vertical" data-brk-library="component__widgets">
                                <a href="#" class="brs-post__img-container">
                                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/news-img.png')}}" alt="vivo mobiles distributors" class="brs-post__img lazyload">
                                </a>
                                <div class="brs-post__about">
                                    <a href="#" class="brs-post__date">
                                        <i class="far fa-clock brs-post__date-icon"></i>
                                        <span class="font__family-montserrat font__size-13 line__height-14">Aug 12, 17</span>
                                    </a>
                                    <h2 class="brs-post__title font__family-montserrat font__size-16 font_weight-light line__height-18">
                                        Some large title lorem ipsum
                                    </h2>
                                    <a href="#" class="brs-post__link">
                                        <i class="fal fa-angle-right brs-post__link-icon"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="brs-post brs-post_mini-vertical" data-brk-library="component__widgets">
                                <a href="#" class="brs-post__img-container">
                                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/simple-post.png')}}" alt="distributors in Pune" class="brs-post__img lazyload">
                                </a>
                                <div class="brs-post__about">
                                    <a href="#" class="brs-post__date">
                                        <i class="far fa-clock brs-post__date-icon"></i>
                                        <span class="font__family-montserrat font__size-13 line__height-14">Aug 12, 17</span>
                                    </a>
                                    <h2 class="brs-post__title font__family-montserrat font__size-16 font_weight-light line__height-18">
                                        Some large title
                                    </h2>
                                    <a href="#" class="brs-post__link">
                                        <i class="fal fa-angle-right brs-post__link-icon"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="brs-post brs-post_mini-vertical" data-brk-library="component__widgets">
                                <a href="#" class="brs-post__img-container">
                                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/quote-post-1.png')}}" alt="telecom service distributors" class="brs-post__img lazyload">
                                </a>
                                <div class="brs-post__about">
                                    <a href="#" class="brs-post__date">
                                        <i class="far fa-clock brs-post__date-icon"></i>
                                        <span class="font__family-montserrat font__size-13 line__height-14">Aug 12, 17</span>
                                    </a>
                                    <h2 class="brs-post__title font__family-montserrat font__size-16 font_weight-light line__height-18">
                                        Some large title
                                    </h2>
                                    <a href="#" class="brs-post__link">
                                        <i class="fal fa-angle-right brs-post__link-icon"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="brs-tab brs-tab_dual" data-brk-library="component__widgets">
                            <ul class="nav nav-tabs brs-tab__header" id="news-tab" role="tablist">
                                <li class="nav-item brs-tab__header-item">
                                    <a class="nav-link active brs-tab__header-title font__family-montserrat font__size-16 font__weight-bold line__height-18" id="popular-tab" data-toggle="tab" href="#popular" role="tab" aria-controls="popular" aria-selected="true">
                                        <i class="fal fa-bolt brs-tab__header-title-icon"></i>Popular</a>
                                </li>
                                <li class="nav-item brs-tab__header-item">
                                    <a class="nav-link brs-tab__header-title font__family-montserrat font__size-16 font__weight-bold line__height-18" id="recent-tab" data-toggle="tab" href="#recent" role="tab" aria-controls="recent" aria-selected="false">
                                        <i class="far fa-clock brs-tab__header-title-icon"></i>Recent</a>
                                </li>
                            </ul>
                            <div class="tab-content brs-tab__body" id="news-tabContent">
                                <div class="tab-pane brs-tab__body-item fade show active" id="popular" role="tabpanel" aria-labelledby="popular-tab">
                                    <div class="brs-post brs-post_mini-horizontal" data-brk-library="component__widgets">
                                        <div class="brs-post__img-container">
                                            <a href="#">
                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/news-mini-1.png')}}" alt="Electronic Products" class="brs-post__img lazyload">
                                            </a>
                                        </div>
                                        <div class="brs-post__about">
                                            <a href="#" class="brs-post__date">
                                                <i class="far fa-clock brs-post__date-icon"></i>
                                                <span class="font__family-montserrat font__size-13 line__height-14 font__weight-normal">Aug 12, 17</span>
                                            </a>
                                            <a href="#">
                                                <h3 class="brs-post__title font__family-montserrat font__size-15 font__weight-normal line__height-18">Some large title</h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="brs-post brs-post_mini-horizontal" data-brk-library="component__widgets">
                                        <div class="brs-post__img-container">
                                            <a href="#">
                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/news-mini-2.png')}}" alt="nikon distributors" class="brs-post__img lazyload">
                                            </a>
                                        </div>
                                        <div class="brs-post__about">
                                            <a href="#" class="brs-post__date">
                                                <i class="far fa-clock brs-post__date-icon"></i>
                                                <span class="font__family-montserrat font__size-13 line__height-14 font__weight-normal">Aug 12, 17</span>
                                            </a>
                                            <a href="#">
                                                <h3 class="brs-post__title font__family-montserrat font__size-15 font__weight-normal line__height-18">Some large title</h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="brs-post brs-post_mini-horizontal" data-brk-library="component__widgets">
                                        <div class="brs-post__img-container">
                                            <a href="#">
                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/news-mini-3.png')}}" alt="micromax mobiles distributors" class="brs-post__img lazyload">
                                            </a>
                                        </div>
                                        <div class="brs-post__about">
                                            <a href="#" class="brs-post__date">
                                                <i class="far fa-clock brs-post__date-icon"></i>
                                                <span class="font__family-montserrat font__size-13 line__height-14 font__weight-normal">Aug 12, 17</span>
                                            </a>
                                            <a href="#">
                                                <h3 class="brs-post__title font__family-montserrat font__size-15 font__weight-normal line__height-18">Some large title</h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade brs-tab__body-item" id="recent" role="tabpanel" aria-labelledby="recent-tab">
                                    <div class="brs-post brs-post_mini-horizontal" data-brk-library="component__widgets">
                                        <div class="brs-post__img-container">
                                            <a href="#">
                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/news-mini-3.png')}}" alt="Vivo Mobiles distributors" class="brs-post__img lazyload">
                                            </a>
                                        </div>
                                        <div class="brs-post__about">
                                            <a href="#" class="brs-post__date">
                                                <i class="far fa-clock brs-post__date-icon"></i>
                                                <span class="font__family-montserrat font__size-13 line__height-14 font__weight-normal">Aug 12, 17</span>
                                            </a>
                                            <a href="#">
                                                <h3 class="brs-post__title font__family-montserrat font__size-15 font__weight-normal line__height-18">Some large title</h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="brs-post brs-post_mini-horizontal" data-brk-library="component__widgets">
                                        <div class="brs-post__img-container">
                                            <a href="#">
                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/news-mini-1.png')}}" alt="Nikon India" class="brs-post__img lazyload">
                                            </a>
                                        </div>
                                        <div class="brs-post__about">
                                            <a href="#" class="brs-post__date">
                                                <i class="far fa-clock brs-post__date-icon"></i>
                                                <span class="font__family-montserrat font__size-13 line__height-14 font__weight-normal">Aug 12, 17</span>
                                            </a>
                                            <a href="#">
                                                <h3 class="brs-post__title font__family-montserrat font__size-15 font__weight-normal line__height-18">Some large title</h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="brs-post brs-post_mini-horizontal" data-brk-library="component__widgets">
                                        <div class="brs-post__img-container">
                                            <a href="#">
                                                <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/blog/news-mini-2.png')}}" alt="distribution partner" class="brs-post__img lazyload">
                                            </a>
                                        </div>
                                        <div class="brs-post__about">
                                            <a href="#" class="brs-post__date">
                                                <i class="far fa-clock brs-post__date-icon"></i>
                                                <span class="font__family-montserrat font__size-13 line__height-14 font__weight-normal">Aug 12, 17</span>
                                            </a>
                                            <a href="#">
                                                <h3 class="brs-post__title font__family-montserrat font__size-15 font__weight-normal line__height-18">Some large title</h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="brs-sidebar__title">
                            <h3 class="font__family-montserrat font__size-21 font__weight-bold line__height-22">Tags</h3>
                        </div>
                        <ul class="brk-tags brk-tags_solid font__family-montserrat" data-brk-library="component__tags">
                            <li><a href="#" rel="tag">Network</a></li>
                            <li><a href="#" rel="tag">Development</a></li>
                            <li><a href="#" rel="tag">Bussines</a></li>
                            <li><a href="#" rel="tag">Beauty</a></li>
                            <li><a href="#" rel="tag">WordPress</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
@endsection