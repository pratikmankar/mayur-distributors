@extends('../layouts.layout')

@section('title')
Mayur Distributors | IFFALCON
@endsection


@section('headers')
    <style>
        .product-name:hover{
            color:#fff;
            transition:.1s;
        }
    </style>
@endsection

@section('content')

<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-28 line__height-68 font__family-montserrat">
                            Nikon Nikon AF-S Nikkor 50mm f/1.8G Prime for Nikon DSLR Camera Lens  (Black, 50mm)
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Nikon Nikon AF-S Nikkor 50mm f/1.8G Prime for Nikon DSLR Camera Lens  (Black, 50mm)</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<div class="main-wrapper">
    <main class="main-container pt-30">
        <div class="container">
            <div class="brk-sc-shop-item mb-30" data-brk-library="component__shop_item_page_sidebar">
                <div class="row">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <div class="brk-sc-shop-item__main">
                            <div class="row pb-20 justify-content-md-start justify-content-center">
                                <div class="col-lg-6 col-md-12 mb-4">
                                    <div class="slider-thumbnailed slick-loading fa-req">
                                        <div class="slider-thumbnailed-for arrows-modern" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "fade": true, "arrows": true, "autoplay": false, "autoplaySpeed": 4200}' data-brk-library="slider__slick" style="min-height: auto">
                                            <div>
                                                <img src="{{URL::to('public/img/products/nikon/nikon-af-s-dx-nikkor-18-140mm-f-35.jpg')}}" alt="alt">
                                            </div>
                                            <div>
                                                <img src="{{URL::to('public/img/products/nikon/nikon-af-s-dx-nikkor-18-140mm-f-351.jpg')}}" alt="alt">
                                            </div>
                                          
                                        </div>
                                        <div class="slider-thumbnailed-nav" data-slick='{"slidesToShow": 2, "slidesToScroll": 1}'>
                                           
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-12 mt-4">
                                    <div class="brk-sc-shop-item__info">
                                        <div class="brk-sc-category d-flex align-items-baseline mb-15" data-brk-library="component__elements">
                                            <span class="brk-sc-category__title font__family-montserrat font__size-14 font__weight-bold mr-10 text-uppercase">
                                                Category:
                                            </span>
                                            <div class="brk-sc-category__items d-flex align-items-center flex-wrap">
                                                <a href="javascript:void();" class="brk-dark-font-color font__family-open-sans font__size-14 mr-1">SmartCamera</a>
                                            </div>
                                        </div>
                                        <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-light line__height-32">Nikon Nikon AF-S Nikkor 50mm   
                                            <br>
                                            <span class="font__weight-bold">f/1.8G Prime for Nikon DSLR Camera Lens  (Black, 50mm)</span>
                                        </h2>
                                        <div class="brk-sc-divider mt-10 mb-20"></div>
                                        <div class="row no-gutters">
                                            <div class="col-lg-12">
                                                <h3 class="brk-color-filter__title font__family-montserrat font__weight-bold font__size-16 text-uppercase text-left">
                                                    <span>Highlights</span>
                                                </h3>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Lens</span>
                                                </h3>
                                                <p>Lens Mount: AF-S</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Designed For</span>
                                                </h3>
                                                <p>
                                                     Nikon DSLRs</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Focal Length</span>
                                                </h3>
                                                <p>90 mm</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span> Lens Type</span>
                                                </h3>
                                                <p>
                                                    Telephoto1</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Zoom Experience</span>
                                                </h3>
                                                <p>                                            
                                                </p>
                                            </div>
                                            <div class="col-lg-12">
                                                <hr class="underline-white">
                                                <div class="text-center text-lg-left mt-5">
                                                    <a href="{{url('/contact-us')}}" class="btn btn-inside-out btn-lg border-radius-25 btn-shadow ml-0 pl-50 pr-50 brk-library-rendered rendered" data-brk-library="component__button" tabindex="-1">
                                                        <span class="before">Enquire Now!</span><span class="text" style="min-width: 90.4375px;">Enquire Now!</span><span class="after">Enquire Now!</span>
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="brk-tabs brk-tabs_canted" data-brk-library="component__shop_item_page_sidebar">
                                <ul class="brk-tabs-nav font__family-montserrat font__size-14 font__weight-semibold line__height-14 text-uppercase">
                                    <li class="brk-tab">
                                        <span>Description</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Specifications</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Similar Products</span>
                                    </li>
                                </ul>
                                <div class="brk-tabs-content">
                                    <div class="brk-tab-item pt-35">

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    The AF-S DX NIKKOR 18-140 mm is a compact and portable zoom lens that offers approximately 7.8x zoom. This lens is ideal for wildlife and travel photography, thanks to its Vibration Reduction and Silent Wave Motor features.
                                                </p>
                                                
                                            </div>
                                            <div class="col-md-12 col-lg-7">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                Function</h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Suitable for nearly every situation, the AF-S DX NIKKOR 18-140 mm lens is designed to draw full potential from Nikon's high-resolution DX-format image sensors. Shoot everything from portraits to sporting events, thanks to the lens' 7.8x zoom range.
                                                </p>
                                            </div>
                                            <div class="col-md-12 col-lg-5 text-right">
                                                <img src="{{('public/img/products/nikon/nikon-af-s-dx-nikkor-18-140mmd1.jpg')}}" class="img-fluid" width="200px">
                                            </div>
                                            
                                            <div class="brk-sc-divider mt-10 mb-20"></div>

                                            <div class="col-md-12 col-lg-5">
                                                <img src="{{('public/img/products/nikon/nikon-af-s-dx-nikkor-18-140mmd2.jpg')}}" class="img-fluid mb-4" width="400px">
                                            </div>
                                            <div class="col-md-12 col-lg-7 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Zoom Range</h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                   With the AF-S DX NIKKOR lens, you get vibrant, detailed photos and videos across its entire zoom range. Zoom out for an 18 mm wide-angle view, making it ideal for group shots. With 140 mm Zoom in capture candid close-ups of your favorite player from a distance.
                                                </p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <!-- <div class="col-lg-12">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Take calls, click beautiful pictures and have uninterrupted entertainment by using this Micromax iOne phone. While its 15.44-cm display provides you with sufficient space to read news or watch videos, its 3950 mAh battery lets you listen to music or play games for hours on end.
                                                </p>
                                                
                                            </div> -->

                                            <div class="col-md-12 col-lg-7">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                VR Image Stabilization</h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This 18-140 mm lens has 4 stops of VR image stabilization so you can capture sharp and clear photos and videos in low-light conditions.
                                                </p>
                                            </div>
                                            <!-- <div class="col-md-12 col-lg-5 text-right">
                                                <img src="{{('public/img/products/micromax/longlosting.jpg')}}" class="img-fluid" width="200px">
                                            </div> -->
                                            
                                            <!-- <div class="brk-sc-divider mt-10 mb-20"></div>

                                            <div class="col-md-12 col-lg-5">
                                                <img src="{{('public/img/products/micromax/memory.jpg')}}" class="img-fluid mb-4" width="300px">
                                            </div> -->
                                            <!-- <div class="col-md-12 col-lg-7 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Expandable Memory</h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    With 64 GB of expandable memory, this phone helps you store movies, photos, music and other files in it conveniently.
                                                    With 3 GB of RAM, 32 GB of ROM and the Android Pie 9.0 operating system, this phone ensures smooth and seamless performance.    
                                                </p>
                                            </div> -->
                                        </div>

                                    </div>
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-16 font__weight-bold line__height-32">General</h2>
                                                <hr class="underline-white">
                                            </div>
                                            
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Brand</h2>
                                                <p>Nikon</p>
                                                <hr class="underline-white">
                                            </div>

                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Model Name</h2>
                                                <p>AF-S DX NIKKOR </p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Model Number</h2>
                                                <p>
                                                    D35 18-200mm </p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Image Sensor Size</h2>
                                                <p>
                                                    23.5 x 15.6</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Type</h2>
                                                <p>DSLR</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Resolution</h2>
                                                <p>

                                                    1920 x 1080</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Camera Features</h2>
                                                <p>24.2 MP</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Video Quality</h2>
                                                <p>1080p recording at 60p</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Display Type</h2>
                                                <p>TFT</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Touchscreen</h2>
                                                <p>No</p>
                                                <hr class="underline-white">
                                            </div>
                                            
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Color</h2>
                                                <p>
                                                    Black</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">OTG Compatible</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            {{-- <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Network Type</h2>
                                                <p>4G VOLTE</p>
                                                <hr class="underline-white">
                                            </div> --}}
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Launch Year</h2>
                                                <p>December 25,<br> 2018</p>
                                                <hr class="underline-white">
                                            </div>

                                            <div class="mb-3 col-md-6 col-lg-10">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">In The Box</h2>
                                                <p>
                                                   

1 Lens Of Nikon AF-S DX Nikkor 18-200mm f/3.5-5.6G ED VR II
</p>
                                                    <hr class="underline-white">
                                            </div>


                                            

                                        </div>
                                    </div>
                                    <div class="brk-tab-item pt-35">
                                         <div class="row">
                                            <div class="col-lg-12">
                                                <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "arrows": false, "responsive": [
                                                    {"breakpoint": 1200, "settings": {"slidesToShow": 4}},
                                                    {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                                                    {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                                                    {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                                                    {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                                                    ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/Nikon/n1.jpg')}}" class="img2" alt="" width="293" height="284">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/nikon-AF-Nikker70')}}">AF-S NIKKOR 70-200mm (Lens)</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/Nikon/n2.jpg')}}" class="img2" alt="mayur distributors" width="293" height="284">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/nikon-AF-Nikker200')}}"> AF-S NIKKOR 200-500mm (Lens)</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/Nikon/n3.jpg')}}" class="img2" alt="Tata Chemicals distributors" width="293" height="284">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/nikon-AF-50mm')}}">AF 50mm F/1.4D (Lens)</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/Nikon/n4.jpg')}}"  class="img2" alt="iFFalcon Televisions by TCL" width="293" height="284">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/nikon-AF-35mm')}}">AF-S DX 35mm F/1.8G (Lens)</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/Nikon/n5.jpg')}}" class="img2" alt="Vivo Mobiles" width="293" height="284">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/nikon-AF-Nikker70')}}"> Nikon AF-Nikker70 </a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/Nikon/n6.jpg')}}" class="img2" alt="Micromax Mobiles" width="293" height="284">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/nikon-AF-Nikker200')}}"> Nikon-AF-Nikker200</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/Nikon/n7.jpg')}}" class="img2" alt="distribution partner" width="293" height="284">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/nikon-AF-50mm')}}"> Nikon AF-50mm</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/Nikon/n8.jpg')}}" class="img2" alt="Nikon India" width="293" height="284">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/nikon-AF-35mm')}}"> Nikon  AF-35mm</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                      
                                                     
                                                    </div>
                                            </div>
                                        
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

    </main>
</div>

@endsection