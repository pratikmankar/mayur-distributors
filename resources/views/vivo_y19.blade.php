@extends('layouts.layout')

@section('title')
Mayur Distributors | VIVO
@endsection


@section('headers')
    <style>
        .product-name:hover{
            color:#fff;
            transition:.1s;
        }
    </style>
@endsection

@section('content')

<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-28 line__height-68 font__family-montserrat">
                            Vivo Y19 (Spring White, 128 GB)  (4 GB RAM)


                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Vivo Y19 (Spring White, 128 GB)  (4 GB RAM)

</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="main-wrapper">
    <main class="main-container pt-30">
        <div class="container">
            <div class="brk-sc-shop-item mb-30" data-brk-library="component__shop_item_page_sidebar">
                <div class="row">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <div class="brk-sc-shop-item__main">
                            <div class="row pb-20 justify-content-md-start justify-content-center">
                                <div class="col-lg-6 col-md-12 mb-4">
                                    
                                    <div class="slider-thumbnailed slick-loading fa-req">
                                        <div class="slider-thumbnailed-for arrows-modern" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "fade": true, "arrows": true, "autoplay": false, "autoplaySpeed": 4200}' data-brk-library="slider__slick" style="min-height: auto">
                                            <div>
                                                <img src="{{('public/img/products/vivo/vivo-y19.jpeg')}}" alt="alt">
                                            </div>
                                            <div>
                                                <img src="{{('public/img/products/vivo/vivo-y19-back.jpeg')}}" alt="alt">
                                            </div>
                                          
                                        </div>
                                        <div class="slider-thumbnailed-nav" data-slick='{"slidesToShow": 2, "slidesToScroll": 1}'>
                                           
                                        </div>
                                    </div>
                                    
                            </div>
                                <div class="col-lg-6 col-md-12 mt-4">
                                    <div class="brk-sc-shop-item__info">
                                      
                                        <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-light line__height-32">
                                         
                                            <span class="font__weight-bold">Vivo Y19 (Spring White, 128 GB)  (4 GB RAM)

</span>
                                        </h2>
                                        <div class="brk-sc-divider mt-10 mb-20"></div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3 class="brk-color-filter__title font__family-montserrat font__weight-bold font__size-16 text-uppercase text-left">
                                                    <span>Highlights</span>
                                                </h3>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>4 GB RAM | 128 GB ROM | Expandable Upto 256 GB
                                                    </span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>16.59 cm (6.53 inch) Full HD+ Display
                                                    </span>
                                                </h3>
                                             
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>16MP + 8MP + 2MP | 16MP Front Camera
                                                    </span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>5000 mAh Li-ion Battery
                                                    </span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>Helio P65 Processor</span>
                                                </h3>
                                            </div>
                                          
                                            <div class="col-lg-12">
                                                <hr class="underline-white">
                                                <div class="text-center text-lg-left mt-5">
                                                    <a href="{{url('/contact-us')}}" class="btn btn-inside-out btn-lg border-radius-25 btn-shadow ml-0 pl-50 pr-50 brk-library-rendered rendered" data-brk-library="component__button" tabindex="-1">
                                                        <span class="before">Enquire Now!</span><span class="text" style="min-width: 90.4375px;">Enquire Now!</span><span class="after">Enquire Now!</span>
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="brk-tabs brk-tabs_canted" data-brk-library="component__shop_item_page_sidebar">
                                <ul class="brk-tabs-nav font__family-montserrat font__size-14 font__weight-semibold line__height-14 text-uppercase">
                                    <li class="brk-tab">
                                        <span>Description</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Specifications</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Similar Products</span>
                                    </li>
                                </ul>
                                <div class="brk-tabs-content">
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                
                                                    Take your smartphone experience to the next level with the highly trendy yet functional Vivo Y19. Housing a 5000 mAh battery, this phone will stay powered up for a long time. Stay awed by its commendable performance, thanks to the Helio P65 Octa-core Processor and 4 GB of RAM. Everything on its 16.59 cm (6.53) FHD+ Halo FullView Display will look beautiful and captivating.
                                                </p>
                                                
                                            </div>
                                            <div class="col-md-12 col-lg-12">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Mighty 5000 mAh Battery

                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This powerful battery is supported by Smart Energy Management technologies that will keep the Vivo Y19 powered up for longer so that you can get more done on the go.


                                                </p>
                                            </div>
                                      
                                            
                                            <div class="brk-sc-divider mt-10 mb-20"></div>

                                           
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    Vivo’s Dual-engine Fast Charging Technology
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This technology quickly juices up the phone’s high-capacity battery in no time. And, it does so while ensuring that the phone is safe, thanks to nine layers of protection.                                          </p>
                                            </div>
                                            <div class="brk-sc-divider mt-10 mb-20"></div>

                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    Powerful Performance


                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Powered by a Helio P65 Octa-core Processor and 4 GB of RAM, this phone will execute any task seamlessly. So, watch videos, text your buddies, surf the Internet, play games and do much more without experiencing any lag. With 128 GB of internal memory, you can store more on this phone as it has enough room for your memories, movies, music and other files.


                                                </p>
                                                </div>
                                            
                                     
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    16.59 cm (6.53) FHD+ Halo FullView Display

                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    With narrow bezels and a screen-to-body ratio of 90.3%, this phone will give you a virtually limitless and immersive viewing experience.


                                                </p>
                                            </div>
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                          
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    Triple Rear Camera System

                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Featuring a 16 MP Main Camera, a 2 MP Depth Camera and an 8 MP Super-wide-angle Camera with bokeh capabilities, this triple-camera system will let you click some amazing pictures.

   
                                                </p>
                                            </div>
                                         
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                        
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    16 MP Selfie Camera

                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This front camera that has the AI Face Beauty feature lets you click gorgeous and radiant selfies effortlessly.


                                                </p>
                                            </div>
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    Ultra Game Mode

                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    With a multitude of features that are made for gaming enthusiasts, this mode takes handheld gaming to the next level. This mode won’t let alerts and messages come in the way when you are gaming.


                                                </p>
                                            </div>
                                           
                                        
                                         
                                        </div>
                                    </div>
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-16 font__weight-bold line__height-32">General</h2>
                                                <hr class="underline-white">
                                            </div>
                                        
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Model Number
                                                </h2>
                                                <p>Vivo 1915/PD1934F_EX

                                                </p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Model Name
                                                </h2>
                                                <p>Y19</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Color
                                                </h2>
                                                <p>Spring White
                                                </p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Browse Type
                                                </h2>
                                                <p>Smartphones</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    SIM Type
                                                </h2>
                                                <p>Dual Sim</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Hybrid Sim Slot
                                                </h2>
                                                <p>No</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Touchscreen</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">OTG Compatible</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Quick Charging
                                                </h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">SAR Value
                                                </h2>
                                                <p>Head - 0.720 W/kg, Body - 0.313 W/kg

                                                </p>
                                                <hr class="underline-white">
                                            </div>
                                           
                                            <div class="mb-3 col-md-6 col-lg-10">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">In The Box</h2>
                                                <p>
                                                    Handset, User Manual, Micro USB to USB Cable, USB Power Adapter, SIM Ejector Pin, Protective Case, Protective Film (1 Applied)

                                                </p>
                                                    <hr class="underline-white">
                                            </div>
                                            

                                        </div>
                                    </div>
                                      


                                    <div class="brk-tab-item pt-35">
                                        <div class="brk-grid__item Vivo" style="max-height:550px;">
                                            <div class="container mt-50">
                                                
                                                <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": false, "responsive": [
                                        {"breakpoint": 1200, "settings": {"slidesToShow": 3}},
                                        {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                                        {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                                        {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                                        {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                                        ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/vivo-v17-vivo-1919-pd1948f-ex-original-imafmth6ezsgmfgj.jpg')}}" alt="iFFalcon tv by TCL">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo V17 (8GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/vivo-s1-pro-vivo-1920-original-imafnhwp7zhvmqkk.jpg')}}" alt="vivo mobiles distributors">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo S1 Pro (8GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/s1-64-d-1907-pd1913f-ex-vivo-6-original-imafgyfqfgswgq4w.jpg')}}" alt="tata chemicals distributors">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo S1 (4GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/vivo-y19-vivo-1915-pd1934f-ex-original-imafmcpd6cezg94d.jpg')}}" alt="telecom service distributors">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y19 (4GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/y91i-32-c-1820-vivo-2-original-imafegpdpjymsrwe.jpg')}}" alt="distributors in Pune">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y91i (2GB+32GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/y91i-32-d-1820-vivo-2-original-imafegpd6zyrmaqh.jpg')}}" alt="consumer goods company">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y91i (3GB+32GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/y15-64-a-1901-vivo-4-original-imafh432tfgwqatf.jpg')}}" alt="Electronic Products">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y15 (4GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/y12-64-b-1904-pd1901ef-ex-vivo-3-original-imafh4zjvmpfdy7v.jpg')}}" alt="consumer goods">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y12 (3GB+64GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/vivo-y11-vivo-1906-pd1930cf-ex-original-imafngxhtqbfdydg.jpg')}}" alt="consumer goods company in India">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y11 (3GB+32GB)
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 
                                                </div>
                                            </div>
                                        </div>
                                     
                                    </div>


                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

    </main>
</div>

@endsection
              