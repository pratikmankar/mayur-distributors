<!DOCTYPE html>
<html lang="en" data-brk-skin="brk-blue.css')}}">

<head>
	<title>
		@yield('title')
	</title>
		@yield('metas')
	<meta name="google-site-verification" content="ghK7S1hPGF-0oEy1LVLcsoP6kqsN2bErVRjeCLCy6DI" />
	<link rel="shortcut icon" href="{{URL::to('public/img/favicon.png')}}">
	<link rel="apple-touch-icon-precomposed" href="{{URL::to('public/img/favicon.png')}}">
	<link rel="stylesheet" id="brk-direction-bootstrap" href="{{asset('public/css/assets/bootstrap.css')}}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
	<link rel="stylesheet" id="brk-skin-color" href="{{asset('public/css/skins/brk-blue.css')}}">
	<link id="brk-base-color" rel="stylesheet" href="{{asset('public/css/skins/brk-base-color.css')}}">
	<link rel="stylesheet" id="brk-direction-offsets" href="{{asset('public/css/assets/offsets.css')}}">
	<link id="brk-css-min" rel="stylesheet" href="{{asset('public/css/assets/styles.min.css')}}">
	<link rel="stylesheet" href="{{asset('public/vendor/revslider/css/settings.css')}}">
	<link rel="stylesheet" href="{{asset('public/toaster/toaster.css')}}">

	@yield('headers')

	<style>
		.brk-nav__mega-menu.brk-nav-drop-down {
			width: 70%;
			/* align-self: center; */
			/* place-self: center; */
			/* justify-self: center; */
			left: 50%;
			transform: translateX(-50%);
		}
	</style>

</head>

<body>
	
	<style>
		#rev_slider_7_1_wrapper .tp-loader.spinner2 {
			background-color: #0071fc !important;
		}
	</style>
	<style>
		.brk-btn-revs-video {
			border: solid 2px rgba(255, 255, 255, .14);
			font-family: Montserrat;
			text-transform: uppercase;
			text-decoration: none;
			font-size: 15px;
			line-height: 70px;
			color: #fff;
			font-weight: 400;
			width: 290px;
			height: 70px;
			border-radius: 37px;
			display: block;
			text-align: right;
			padding-right: 41px;
			position: relative
		}

		.brk-btn-revs-video__wr-icon {
			position: absolute;
			top: 50%;
			left: 3px;
			transform: translateY(-50%);
			width: 64px;
			height: 64px;
			border-radius: 50%;
			background-color: #fff;
			box-shadow: 0 5px 16px rgba(0, 0, 0, .3)
		}

		.brk-btn-revs-video__wr-icon:before {
			content: '';
			position: absolute;
			top: -33px;
			left: -33px;
			right: -33px;
			bottom: -33px;
			transform: scale(1);
			opacity: .1;
			background-color: #fff;
			border-radius: 50%;
			z-index: -1
		}

		.brk-btn-revs-video__wr-icon:after {
			content: '';
			position: absolute;
			top: -16px;
			left: -16px;
			right: -16px;
			bottom: -16px;
			opacity: .2;
			background-color: #fff;
			border-radius: 50%;
			z-index: -1
		}

		.brk-btn-revs-video:hover .brk-btn-revs-video__wr-icon:before {
			animation: pulse-paused 1s ease-in infinite;
			animation-delay: 0.2s
		}

		.brk-btn-revs-video:hover .brk-btn-revs-video__wr-icon:after {
			animation: pulse-paused 1s ease-in infinite
		}

		@-webkit-keyframes pulse-paused {
			0% {
				-webkit-transform: scale(1);
				transform: scale(1)
			}

			20% {
				-webkit-transform: scale(1.2);
				transform: scale(1.2)
			}

			60% {
				-webkit-transform: scale(0.9);
				transform: scale(0.9);
				opacity: 0
			}

			100% {
				-webkit-transform: scale(1);
				transform: scale(1)
			}
		}

		@keyframes pulse-paused {
			0% {
				-webkit-transform: scale(1);
				transform: scale(1)
			}

			20% {
				-webkit-transform: scale(1.2);
				transform: scale(1.2)
			}

			60% {
				-webkit-transform: scale(0.9);
				transform: scale(0.9);
				opacity: 0
			}

			100% {
				-webkit-transform: scale(1);
				transform: scale(1)
			}
		}

		.brk-btn-revs-video__icon {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			border-top: 8px solid transparent;
			border-bottom: 8px solid transparent;
			border-left: 15px solid var(--brand-primary, #2775ff)
		}

		.brk-classic.tp-bullets {
			width: auto !important;
			display: flex;
			align-items: center;
			justify-content: center;
			box-sizing: border-box
		}

		.brk-classic.tp-bullets:before {
			content: ''
		}

		.brk-classic .tp-bullet {
			position: relative !important;
			left: 0 !important;
			background-color: transparent !important;
			float: left;
			width: 12px;
			height: 12px;
			border-radius: 50%;
			border: 1px solid transparent;
			margin: 0 5px;
			padding: 0;
			cursor: pointer
		}

		.brk-classic .tp-bullet:before {
			content: '';
			width: 9px;
			height: 9px;
			border: 1px solid #fff;
			border-radius: 50%;
			cursor: pointer;
			color: transparent;
			transition-property: width, height;
			transition-duration: .2s;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			background-color: #2775FF
		}

		.brk-classic .tp-bullet:hover,
		.brk-classic .tp-bullet.selected {
			border-color: #a2a2a2;
			background-color: #fff !important
		}

		.brk-classic .tp-bullet:hover:before,
		.brk-classic .tp-bullet.selected:before {
			width: 0;
			height: 0;
			border: none
		}
	</style>
	<style>
		.mt--230 {
			margin-top: -230px;
		}

		@media (max-width: 991px) {
			.mt--230 {
				margin-top: 70px;
			}
		}

		.mt--337 {
			margin-left: -337px;
		}

		@media (max-width: 1199px) {
			.mt--337 {
				margin-left: 0;
			}
		}

		@media (min-width: 1230px) {
			.image-map-creative_agency {
				width: 170%;
				left: -47%;
				top: 33px !important;
			}

			.image-map-desc-creative_agency {
				padding-top: 22% !important;
				padding-bottom: 12.5% !important;
			}
		}
	</style>
	<script>
		function setREVStartSize(e) {
			try {
				e.c = jQuery(e.c);
				var i = jQuery(window).width(),
					t = 9999,
					r = 0,
					n = 0,
					l = 0,
					f = 0,
					s = 0,
					h = 0;
				if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
						f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
					}), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
					var u = (e.c.width(), jQuery(window).height());
					if (void 0 != e.fullScreenOffsetContainer) {
						var c = e.fullScreenOffsetContainer.split(",");
						if (c) jQuery.each(c, function(e, i) {
							u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
						}), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
					}
					f = u
				} else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
				e.c.closest(".rev_slider_wrapper").css({
					height: f
				})
			} catch (d) {
				console.log("Failure at Presize of Slider:" + d)
			}
		};
	</script>
	<div class="main-page">
		<div class="brk-header-mobile">
			<div class="brk-header-mobile__open brk-header-mobile__open_white">
				<span></span>
			</div>
			<div class="brk-header-mobile__logo">
				<a href="{{url('/')}}">
					<img class="brk-header-mobile__logo-1 lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/logo/logo-white.svg')}}" alt="alt" width="130px">
					<img class="brk-header-mobile__logo-2 lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/logo/logo-color.svg')}}" alt="alt" width="130px">
				</a>
			</div>
		</div>
		<header class="brk-header d-lg-flex flex-column brk-header_style-1 brk-header_color-dark" style="display: none;" data-sticky-hide="0" data-brk-library="component__header" width="130">
			<div class="brk-header__top-bar brk-header_border-bottom order-lg-1 order-2 brk-bg-grad brk-header__top-bar_hide brk-header__top-bar_color-white" data-top="3" style="height: 46px;">
				
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-lg-9 align-self-lg-stretch text-left">
							<div class="brk-header__element brk-header__element_skin-1 brk-header__item">
								<a href="mailto:info@mayurdistributors.com" class="brk-header__element--wrap">
									<i class="far fa-envelope"></i>
									<span class="brk-header__element--label">info@mayurdistributors.com</span>
								</a>
							</div>
							<div class="brk-header__element brk-header__element_skin-1 brk-header__item">
								<div class="brk-header__element--wrap">
									<i class="far fa-clock"></i>
									<span class="brk-header__element--label">Office No.32-33, Santosh Heights, Opp. Apsara Theater , Gultekadi, Shankarshet Road, Pune-37.</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 align-self-lg-stretch text-left text-lg-right">
								<div class="social-icons">
									<ul class="d-flex float-right">
										<li> <a href="https://business.facebook.com/mayurdistributors/?business_id=2833294386752219"> <i class="fab fa-facebook" aria-hidden="true"></i></a> </li>
										<li> <a href="https://www.instagram.com/mayur_distributors/"> <i class="fab fa-instagram" aria-hidden="true"></i></a> </li>
										<li> <a href="https://twitter.com/MayurDistribut1">  <i class="fab fa-twitter" aria-hidden="true"></i></a> </li>
										<li> <a href="https://www.google.co.in/search?q=Mayur+Distributors&ludocid=10682126935951604583&lsig=AB86z5VF4Sn_EXvgolzQhi3zgSSY#fpstate=lie">  <i class="fab fa-google" aria-hidden="true"></i></a> </li>
										<li> <a href="https://api.whatsapp.com/send?phone=917774076072">  <i class="fab fa-whatsapp" aria-hidden="true"></i></a> </li>
									</ul>
								</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="brk-header__main-bar brk-header_border-bottom order-lg-2 order-1 bg-white" style="height: 72px;">
				<div class="container-fluid">
					<div class="row no-gutters align-items-center">
						<div class="col-lg-2 align-self-lg-center d-none d-lg-block">
							<div class="text-center">
								<a href="{{url('/')}}" class="brk-header__logo brk-header__item @@modifier">
									<div class="brk-header__logo-1 lazyload" id="lottie" style="width:200px;"></div>
									<div class="brk-header__logo-2 lazyload" id="lottie1" style="width:200px;"></div>
								</a>
							</div>
						</div>
						
						
						<div class="col-lg align-self-lg-stretch text-center">
							<nav class="brk-nav brk-header__item">
								<ul class="brk-nav__menu">
									<li class="brk-nav__full-width">
										<a href="{{url('/')}}" data-brk-library="component__tabs">
											<span>Home</span>
										</a>
									</li>
									<li class="brk-nav__full-width">
										<a href="{{url('/about-us')}}" data-brk-library="component__tabs">
											<span>About Us</span>
										</a>
									</li>
								
									<li class="brk-nav__children brk-nav__drop-down-effect display-mob">
										<a href="{{url('/products')}}">
											<span>Brands</span>
										</a>
										<ul class="brk-nav__sub-menu brk-nav-drop-down font__family-montserrat">
											<li class="dd-effect brk-nav__children brk-nav__drop-down-effect">
												<a href="#">Vivo <i class="fas fa-angle-right"></i></a>
												<ul class="brk-nav__sub-menu brk-nav-drop-down font__family-montserrat">
													<li class="dd-effect"><a href="{{url('/vivo-v17')}}">Vivo V17 (8GB+128GB)</a></li>
													<li class="dd-effect"><a href="{{url('/vivo-s1pro')}}">Vivo S1 Pro (8GB+128GB)</a></li>
													<li class="dd-effect"><a href="{{url('/vivo-s1')}}">Vivo S1 (4GB+128GB)</a></li>
													<li class="dd-effect"><a href="{{url('/vivo-y19')}}">Vivo Y19 (4GB+128GB)</a></li>
													<li class="dd-effect"><a href="{{url('/vivo-y91i')}}">Vivo Y91i (2GB+32GB)</a></li>
													<li class="dd-effect"><a href="{{url('/vivo-y91i3gb')}}">Vivo Y91i (3GB+32GB)</a></li>
													<li class="dd-effect"><a href="{{url('/vivo-y15')}}">Vivo Y15 (4GB+128GB)</a></li>
													<li class="dd-effect"><a href="{{url('/vivo-y12')}}">Vivo Y12 (3GB+64GB)</a></li>
													<li class="dd-effect"><a href="{{url('/vivo-y11')}}">Vivo Y11 (3GB+32GB)</a></li>
												</ul>
											</li>
											<li class="dd-effect brk-nav__children brk-nav__drop-down-effect">
												<a href="#">iFFALCON <i class="fas fa-angle-right"></i></a>
												<ul class="brk-nav__sub-menu brk-nav-drop-down font__family-montserrat">
													<li class="dd-effect"><a href="{{url('/iffalcon-32F2A')}}">32F2A</a></li>
													<li class="dd-effect"><a href="{{url('/iffalcon-43k31')}}">43K31</a></li>
													<li class="dd-effect"><a href="{{url('/iffalcon-50k31')}}">50K31</a></li>
													<li class="dd-effect"><a href="{{url('/iffalcon-40f2a')}}">40F2A</a></li>
													<li class="dd-effect"><a href="{{url('/iffalcon-55k31')}}">55K31</a></li>
													<li class="dd-effect"><a href="{{url('/iffalcon-65v2a')}}">65V2A</a></li>
													<li class="dd-effect"><a href="{{url('/iffalcon-49f2a')}}">49F2A</a></li>
												</ul>
											</li>

											<li class="dd-effect brk-nav__children brk-nav__drop-down-effect">
												<a href="#">Micromax <i class="fas fa-angle-right"></i></a>
												<ul class="brk-nav__sub-menu brk-nav-drop-down font__family-montserrat">
													<li class="dd-effect">
														<a href="{{url('/micromax-one-note')}}">One Note</a>
													</li>
													<li class="dd-effect">
													<a href="{{url('/micromax-Q4002N')}}">Q4002N Bharat 4</a>
													</li>
													<li class="dd-effect">
														<a href="{{url('/Q402bharat-2')}}">Q402+ Bharat 2</a>
													</li>
													<li class="dd-effect">
														<a href="{{url('/micromax-Qbharat-5')}}">Q4204 Bharat 5</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-Q204bharat-4')}}">Q4204 Bharat 4</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-N11')}}">N11</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-N12')}}">N12</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-X378')}}">X378</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-X412')}}">X412</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-X388')}}">X388</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-X412')}}">X421</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-X419')}}">X419</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-X744')}}">X744</a></li>
													<li class="dd-effect">
														<a href="{{url('/micromax-X809')}}">X809</a></li>
												</ul>
											</li>
											<li class="dd-effect brk-nav__children brk-nav__drop-down-effect">
												<a href="">Nikon <i class="fas fa-angle-right"></i></a>
												<ul class="brk-nav__sub-menu brk-nav-drop-down font__family-montserrat">
													<li class="dd-effect">
														<a href="{{url('/nikon-Dzoom70')}}">D5600 Kit (18-140)</a></li>
													<li class="dd-effect">
														<a href="{{url('/nikon-D3500-1815')}}">D5600 Kit (18-55)</a></li>
													<li class="dd-effect">
														<a href="{{url('/nikon-D5600-Dzoom')}}">D750</a></li>
													<li class="dd-effect">
														<a href="{{url('/nikon-D850')}}">D850</a></li>
													<li class="dd-effect">
														<a href="{{url('/nikon-Z62470')}}">
														D750Z6 with 24-70mm</a></li>
													<li class="dd-effect">
														<a href="{{url('/nikon-AF-Nikker70')}}">AF-S NIKKOR 70-200mm (Lens)</a></li>
													<li class="dd-effect">
														<a href="{{url('/nikon-AF-Nikker200')}}">AF-S NIKKOR 200-500mm (Lens)</a></li>
													<li class="dd-effect">
														<a href="{{url('/nikon-AF-50mm')}}">AF 50mm F/1.4D (Lens)</a></li>
													<li class="dd-effect">
														<a href="{{url('/nikon-AF-35mm')}}">AF-S DX 35mm F/1.8G (Lens)</a></li>
												</ul>
											</li>
											
										
											<li class="dd-effect brk-nav__children brk-nav__drop-down-effect">
												<a href="#">Tata Chemicals<i class="fas fa-angle-right"></i></a>
												<ul class="brk-nav__sub-menu brk-nav-drop-down font__family-montserrat">
													<li class="dd-effect"><a href="#">Tata Rock Salt</a></li>
													<li class="dd-effect"><a href="#">All Varients Pulses</a></li>
													<li class="dd-effect"><a href="#">Besan</a></li>
													<li class="dd-effect"><a href="#">Spices</a></li>
													<li class="dd-effect"><a href="#">Soda</a></li>
													<li class="dd-effect"><a href="#">Poha</a></li>
													<li class="dd-effect"><a href="#">Nutrimixes</a></li>
													<li class="dd-effect"><a href="#">Snacks</a></li>
												</ul>
											</li>
										</ul>
									</li>

									<li class="brk-nav__children brk-nav__full-width display-desk">
										<a href="{{url('/products')}}" data-brk-library="component__tabs">
											<span>Brands</span>
										</a>
										<div class="brk-nav__mega-menu brk-nav-drop-down">
											<div class="bg-white">
												<div class="brk-tabs brk-tabs-hovers" data-index="0">
													<ul class="brk-tabs-nav font__family-open-sans">
														<li class="brk-tab font__weight-bold">
															<img src="{{URL::to('public/img/brands/vivo.jpg')}}" class="mr-2" alt="" style="width:40px; border-radius:50%; vertical-align:middle;">
															Vivo
														</li>
														<li class="brk-tab font__weight-bold">
															<img src="{{URL::to('public/img/brands/iffalcon.jpg')}}" class="mr-2" alt="" style="width:40px; border-radius:50%; vertical-align:middle;">
															iFFALCON
														</li>
														<li class="brk-tab font__weight-bold">
															<img src="{{URL::to('public/img/brands/micromax.jpg')}}" class="mr-2" alt="" style="width:40px; border-radius:50%; vertical-align:middle;">
															Micromax
														</li>
														<li class="brk-tab font__weight-bold">
															<img src="{{URL::to('public/img/brands/nikon.jpg')}}" class="mr-2" alt="" style="width:40px; border-radius:50%; vertical-align:middle;">
															Nikon
														</li>
														<li class="brk-tab font__weight-bold">
															<img src="{{URL::to('public/img/brands/tata.jpg')}}" class="mr-2" alt="" style="width:40px; border-radius:50%; vertical-align:middle;">
															Tata Chemicals
														</li>
													</ul>
													<div class="brk-tabs-content">
														<div class="brk-tab-item">
															<div class="pl-4 pt-3">
																<div class="row">
																	<div class="col-lg-12">
																		<h4 class="font__family-montserrat font__size-28 font__weight-bold text-left mb-3">Vivo</h4>
																		<hr class="underline_white">
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/vivo-v17')}}" class="font__family-montserrat font__size-14 font__weight-bold">Vivo V17 (8GB+128GB)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/vivo-s1pro')}}" class="font__family-montserrat font__size-14 font__weight-bold">Vivo S1 Pro (8GB+128GB)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/vivo-s1')}}" class="font__family-montserrat font__size-14 font__weight-bold">Vivo S1 (4GB+128GB)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/vivo-y19')}}" class="font__family-montserrat font__size-14 font__weight-bold">Vivo Y19 (4GB+128GB)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/vivo-y91i')}}" class="font__family-montserrat font__size-14 font__weight-bold">Vivo Y91i (2GB+32GB)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/vivo-y91i3gb')}}" class="font__family-montserrat font__size-14 font__weight-bold">Vivo Y91i (3GB+32GB)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/vivo-y15')}}" class="font__family-montserrat font__size-14 font__weight-bold">Vivo Y15 (4GB+128GB)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/vivo-y12')}}" class="font__family-montserrat font__size-14 font__weight-bold">Vivo Y12 (3GB+64GB)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/vivo-y11')}}" class="font__family-montserrat font__size-14 font__weight-bold">Vivo Y11 (3GB+32GB)</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="brk-tab-item">
															<div class="pl-4 pt-3">
																<div class="row">
																	<div class="col-lg-12">
																		<h4 class="font__family-montserrat font__size-28 font__weight-bold text-left mb-3">iFFALCON</h4>
																		<hr class="underline_white">
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/iffalcon-32F2A')}}" class="font__family-montserrat font__size-14 font__weight-bold">32F2A</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/iffalcon-43k31')}}" class="font__family-montserrat font__size-14 font__weight-bold">43K31</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/iffalcon-50k31')}}" class="font__family-montserrat font__size-14 font__weight-bold">50K31</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/iffalcon-40f2a')}}" class="font__family-montserrat font__size-14 font__weight-bold">40F2A</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/iffalcon-55k31')}}" class="font__family-montserrat font__size-14 font__weight-bold">55K31</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/iffalcon-65v2a')}}" class="font__family-montserrat font__size-14 font__weight-bold">65V2A</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/iffalcon-49f2a')}}" class="font__family-montserrat font__size-14 font__weight-bold">49F2A</a>
																		</div>
																	</div>

																</div>
															</div>
														</div>
														<div class="brk-tab-item">
															<div class="pl-4 pt-3">
																<div class="row">
																	<div class="col-lg-12">
																		<h4 class="font__family-montserrat font__size-28 font__weight-bold text-left mb-3">Micromax</h4>
																		<hr class="underline_white">
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																		<a href="{{url('/micromax-onenote')}}" class="font__family-montserrat font__size-14 font__weight-bold">One Note</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-Q4002N')}}" class="font__family-montserrat font__size-14 font__weight-bold">Q4002N Bharat 4</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/Q402bharat-2')}}" class="font__family-montserrat font__size-14 font__weight-bold">Q402+ Bharat 2</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-Qbharat-5')}}" class="font__family-montserrat font__size-14 font__weight-bold">Q4204 Bharat 5</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-Q204bharat-4')}}" class="font__family-montserrat font__size-14 font__weight-bold">Q4204 Bharat 4</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-N11')}}" class="font__family-montserrat font__size-14 font__weight-bold">N11</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-N12')}}" class="font__family-montserrat font__size-14 font__weight-bold">N12</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-X378')}}" class="font__family-montserrat font__size-14 font__weight-bold">X378</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-X412')}}" class="font__family-montserrat font__size-14 font__weight-bold">X412</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-X388')}}" class="font__family-montserrat font__size-14 font__weight-bold">X388</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-X421')}}" class="font__family-montserrat font__size-14 font__weight-bold">X421</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-X419')}}" class="font__family-montserrat font__size-14 font__weight-bold">X419</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-X744')}}" class="font__family-montserrat font__size-14 font__weight-bold">X744</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/micromax-X809')}}" class="font__family-montserrat font__size-14 font__weight-bold">X809</a>
																		</div>
																	</div>

																</div>
															</div>
														</div>



														<div class="brk-tab-item">
															<div class="pl-4 pt-3">
																<div class="row">
																	<div class="col-lg-12">
																		<h4 class="font__family-montserrat font__size-28 font__weight-bold text-left mb-3">Nikon</h4>
																		<hr class="underline_white">
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-Dzoom70')}}" class="font__family-montserrat font__size-14 font__weight-bold">D3500 (Dzoom 70-300)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																		<a href="{{url('/nikon-D3500-1815')}}" class="font__family-montserrat font__size-14 font__weight-bold">
																		D3500 (18-55)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-D5600-Dzoom')}}" class="font__family-montserrat font__size-14 font__weight-bold">D5600 Dzoom</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-D5600kit-18140')}}" class="font__family-montserrat font__size-14 font__weight-bold">D5600 Kit (18-140)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-D5600kit-1855')}}" class="font__family-montserrat font__size-14 font__weight-bold">D5600 Kit (18-55)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-D750')}}" class="font__family-montserrat font__size-14 font__weight-bold">D750</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-D850')}}" class="font__family-montserrat font__size-14 font__weight-bold">D850</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-Z62470')}}" class="font__family-montserrat font__size-14 font__weight-bold">Z6 with 24-70mm</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-AF-Nikker70')}}" class="font__family-montserrat font__size-14 font__weight-bold">AF-S NIKKOR 70-200mm (Lens)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-AF-Nikker200')}}" class="font__family-montserrat font__size-14 font__weight-bold">AF-S NIKKOR 200-500mm (Lens)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-AF-50mm')}}" class="font__family-montserrat font__size-14 font__weight-bold">AF 50mm F/1.4D (Lens)</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="{{url('/nikon-AF-35mm')}}" class="font__family-montserrat font__size-14 font__weight-bold">AF-S DX 35mm F/1.8G (Lens)</a>
																		</div>
																	</div>

																</div>
															</div>
														</div>



														<div class="brk-tab-item">
															<div class="pl-4 pt-3">
																<div class="row">
																	<div class="col-lg-12">
																		<h4 class="font__family-montserrat font__size-28 font__weight-bold text-left mb-3">Tata Chemicals</h4>
																		<hr class="underline_white">
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Tata Salt</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Tata Salt Lite</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Tata iShakti</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Tata Rock Salt</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">All Varients Pulses</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Besan</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Spices</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Soda</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Poha</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Nutrimixes</a>
																		</div>
																	</div>
																	<div class="col-lg-4">
																		<div class="mb-4 mt-2">
																			<a href="#" class="font__family-montserrat font__size-14 font__weight-bold">Snacks</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>


													</div>
												</div>
											</div>
										</div>
									</li>


									<li class="brk-nav__drop-down-effect">
										<a href="{{url('/team')}}">
											<span>Team
											</span>
										</a>
										
									</li>
									<li class="brk-nav__drop-down-effect">
										<a href="{{url('/awards')}}">
											<span>Awards
											</span>
										</a>
										
									</li>
									<li class="brk-nav__drop-down-effect">
										<a href="{{url('/gallery')}}">
											<span>Gallery
											</span>
										</a>
										
									</li>
									<li class="brk-nav__drop-down-effect">
										<a href="{{url('/testimonials')}}">
											<span>Testimonials

											</span>
										</a>
										
									</li>
								
									<li class="brk-nav__drop-down-effect">
										<a href="{{url('/contact-us')}}">
											<span>Contact Us</span>
										</a>
									</li>
									
								</ul>
							</nav>
						</div>
					
						<div class="col-lg-2 align-self-lg-stretch text-lg-right">
							<div class="brk-call-us brk-header__item">
								<a href="tel:02026430632" class="brk-call-us__number"><i class="fa fa-phone" aria-hidden="true"></i>020-26430632</a>
								<a href="tel:02026430632" class="brk-call-us__link"><i class="fa fa-phone" aria-hidden="true"></i></a>
							</div>
				
						</div>

						<div class="col-lg-auto align-self-lg-stretch d-none d-lg-block">
							<div class="brk-open-top-bar brk-header__item">
								<div class="brk-open-top-bar__circle"></div>
								<div class="brk-open-top-bar__circle"></div>
								<div class="brk-open-top-bar__circle"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
	
        <div id="top"></div>
		
		

		@yield('content')

		<footer class="brk-footer brk-footer-1 position-relative" data-brk-library="component__footer,twitter_init">
			<div class=" brk-footer__wrapper_shadow">
						<div class="brk-base-bg-gradient-40-no-opacity">
							<div class="container">

							
							<div class="row">
								<div class="col-12 col-md-4">
									<div class="all-light text-center text-sm-left pt-20 pb-10">
										<a href="{{url('/')}}" class="d-inline-block">
											<img src="{{URL::to('public/img/logo/logo-white.svg')}}" alt="alt" width="200px">
										</a>
										<hr class="horiz-line" style="margin-top: 10px; margin-bottom: 47px;">
										<p class="font__family-open-sans font__size-14 line__height-21 brk-white-font-color text-center text-lg-left">
											Mayur distribution has its roots in the industry since 1975. We at Mayur Distribution started off in the niche of distribution with the intent to have the latest and trendy Global technology, soonest at the local level.
										</p>
										<div class="social-icons-f mt-4">
											<a href="https://business.facebook.com/mayurdistributors/?business_id=2833294386752219"><i class="fab fa-facebook" aria-hidden="true"></i></a>
											<a href="https://twitter.com/MayurDistribut1"><i class="fab fa-twitter" aria-hidden="true"></i></a>
											<a href="https://www.instagram.com/mayur_distributors/"><i class="fab fa-instagram" aria-hidden="true"></i></a>
											<a href="https://www.google.co.in/search?q=Mayur+Distributors&ludocid=10682126935951604583&lsig=AB86z5VF4Sn_EXvgolzQhi3zgSSY#fpstate=lie"><i class="fab fa-google" aria-hidden="true"></i></a>
											<a href="https://api.whatsapp.com/send?phone=917774076072"><i class="fab fa-whatsapp" aria-hidden="true"></i></a>
										</div>
										<hr class="horiz-line" style="margin-bottom: 12px;">
									</div>
								</div>
								 <div class="col-12 col-md-4">
									<div class="pt-30 pb-20">
										<h6 class="brk-white-font-color text-center text-sm-left font__family-montserrat font__size-26 font__weight-bold line__height-30">
											Quick Links
										</h6>
										<hr class="horiz-line mb-40" style="margin-top: 19px;">
										<div class="row text-white">
											<div class="col-lg-6 col-md-6 col-sm-6 col-6">

												<p class="font__family-open-sans font__weight-bold font__size-14 mb-3">
													<i class="brk-footer-icon text-middle fa fa-angle-double-right"></i>
													<a href="{{url('/')}}" class="show-inline-block">Home</a>
												</p>
												<p class="font__family-open-sans font__weight-bold font__size-14 mb-3">
													<i class="brk-footer-icon text-middle fa fa-angle-double-right"></i>
													<a href="{{url('/about-us')}}" class="show-inline-block">About Us</a>
												</p>
											
												<p class="font__family-open-sans font__weight-bold font__size-14 mb-3">
													<i class="brk-footer-icon text-middle fa fa-angle-double-right"></i>
													<a href="{{url('/products')}}" class="show-inline-block">Brands</a>
												</p>
											
											
												<p class="font__family-open-sans font__weight-bold font__size-14 mb-3">
													<i class="brk-footer-icon text-middle fa fa-angle-double-right"></i>
													<a href="{{url('/gallery')}}" class="show-inline-block">Gallery</a>
												</p>
												<p class="font__family-open-sans font__weight-bold font__size-14 mb-3">
													<i class="brk-footer-icon text-middle fa fa-angle-double-right"></i>
													<a href="{{url('/team')}}" class="show-inline-block">Our Team</a>
												</p>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-6">
											
											
												<p class="font__family-open-sans font__weight-bold font__size-14 mb-3">
													<i class="brk-footer-icon text-middle fa fa-angle-double-right"></i>
													<a href="{{url('/awards')}}" class="show-inline-block">Awards</a>
												</p>
												<p class="font__family-open-sans font__weight-bold font__size-14 mb-3">
													<i class="brk-footer-icon text-middle fa fa-angle-double-right"></i>
													<a href="{{url('/testimonials')}}" class="show-inline-block">Testimonials</a>
												</p>
												<p class="font__family-open-sans font__weight-bold font__size-14 mb-3">
													<i class="brk-footer-icon text-middle fa fa-angle-double-right"></i>
													<a href="{{url('/career')}}" class="show-inline-block">Career</a>
												</p>
												<p class="font__family-open-sans font__weight-bold font__size-14 mb-3">
													<i class="brk-footer-icon text-middle fa fa-angle-double-right"></i>
													<a href="{{url('/contact-us')}}" class="show-inline-block">Contact Us</a>
												</p>
											</div>
											<hr class="horiz-line" style="margin-top: 10px;">
		
										</div>
									</div>
								</div>
								<div class="col-12 col-md-4">
									<div class="pt-30 pb-20	">
										<h6 class="brk-white-font-color text-center text-sm-left font__family-montserrat font__size-26 font__weight-bold line__height-30">
											Reach Us
										</h6>
										<hr class="horiz-line mb-40" style="margin-top: 19px;">
										<div class="d-flex flex-wrap brk-footer__info-grid flex-sm-row flex-column  text-white">
											<p class="font__family-open-sans font__weight-bold font__size-14 mb-4">
												<i class="brk-footer-icon text-middle fa fa-map-marker line__height-24"></i>Office No.32-33, Santosh Heights, Opp. Apsara Theater, Gultekadi, Shankarshet Road, <br>Pune(MH) - 411037.
											</p>
											<p class="font__family-open-sans font__weight-bold font__size-14 mb-4">
												<i class="brk-footer-icon text-middle fa fa-envelope line__height-24"></i>
												<a href="info@mayurdistributors.com" class="show-inline-block">info@mayurdistributors.com</a>
											</p>
											<p class="font__family-open-sans font__weight-bold font__size-14 mb-4">
												<i class="brk-footer-icon text-middle fa fa-comments line__height-24"></i>Contact
											</p>
											<p class="font__family-open-sans font__weight-bold font__size-14 mb-4">
												<i class="brk-footer-icon text-middle fa fa-phone line__height-24"></i>
												<a href="tel:02026430632" class="show-inline-block">020-26430632</a>
											</p>
										</div>
										<hr class="horiz-line" style="margin-top:18px;">
									</div>

								</div>
							</div>
						</div>
				</div>
				<div class="brk-light-gradient-90deg-94 brk-shadow-light bottom-footer">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 col-md-6 pt-2 pb-2 brk-footer__rights text-left">
								<p class="font__family-open-sans font__size-14 font__weight-normal">© 2020 <a href="#">Mayur Distributors</a> All rights reserved.</p>
							</div>
							<div class="col-lg-6 col-md-6 pt-2 pb-2 text-right brk-social-links brk-white-font-color font__size-14">
								<p class="font__family-open-sans font__size-14 font__weight-normal">Designed & Developed by <a href="#">Accucia Softwares</a></p>
							
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</footer>

	
	</div>
	<a href="#top" id="toTop"></a>
	<script defer="defer" src="{{asset('public/js/scripts.js')}}"></script>
	<script defer="defer" src="{{asset('public/vendor/revslider/js/jquery.themepunch.tools.min.js')}}"></script>
	<script defer="defer" src="{{asset('public/vendor/revslider/js/jquery.themepunch.revolution.min.js')}}""></script>
	<script defer="defer" src="{{asset('public/vendor/revslider/js/extensions/revolution.extension.actions.min.js')}}"></script>
	<script defer="defer" src="{{asset('public/vendor/revslider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
	<script defer="defer" src="{{asset('public/vendor/revslider/js/extensions/revolution.extension.navigation.min.js')}}"></script>
	<script defer="defer" src="{{asset('public/vendor/revslider/js/extensions/revolution.extension.parallax.min.js')}}"></script>
	<script defer="defer" src="{{asset('public/vendor/revslider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
	<script defer="defer" src="{{asset('public/js/logo.js')}}"></script>
	<script defer="defer" src="{{asset('public/js/about-logo.js')}}"></script>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164613113-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-164613113-1');
	</script>

	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script src="{{asset('public/js/validate.js')}}"></script>
	<script src="{{asset('public/toaster/toaster.min.js')}}"></script>


		@yield('scripts')
	<script>
		var revapi7,
			tpj;
		(function() {
			if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
			else onLoad();

			function onLoad() {
				if (tpj === undefined) {
					tpj = jQuery;
					if ("on" == "on") tpj.noConflict();
				}
				if (tpj("#rev_slider_7_1").revolution == undefined) {
					revslider_showDoubleJqueryError("#rev_slider_7_1");
				} else {
					revapi7 = tpj("#rev_slider_7_1").show().revolution({
						startDelay: 100,
						sliderType: "standard",
						jsFileLocation: "vendor/revslider/js/",
						sliderLayout: "fullscreen",
						dottedOverlay: "none",
						delay: 6000,
						navigation: {
							keyboardNavigation: "off",
							keyboard_direction: "horizontal",
							mouseScrollNavigation: "off",
							mouseScrollReverse: "default",
							onHoverStop: "off",
							bullets: {
								enable: true,
								hide_onmobile: false,
								style: "brk-classic",
								hide_onleave: false,
								direction: "horizontal",
								h_align: "center",
								v_align: "bottom",
								h_offset: 0,
								v_offset: 20,
								space: 5,
								tmp: ''
							}
						},
						responsiveLevels: [1240, 1024, 778, 480],
						visibilityLevels: [1240, 1024, 778, 480],
						gridwidth: [1700, 1024, 768, 576],
						gridheight: [960, 768, 960, 720],
						lazyType: "none",
						minHeight: "768",
						parallax: {
							type: "mouse+scroll",
							origo: "enterpoint",
							speed: 400,
							speedbg: 0,
							speedls: 0,
							levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
							disable_onmobile: "on"
						},
						shadow: 0,
						spinner: "spinner2",
						stopLoop: "off",
						stopAfterLoops: -1,
						stopAtSlide: -1,
						shuffle: "off",
						autoHeight: "off",
						fullScreenAutoWidth: "on",
						fullScreenAlignForce: "off",
						fullScreenOffsetContainer: "",
						fullScreenOffset: "",
						disableProgressBar: "on",
						hideThumbsOnMobile: "off",
						hideSliderAtLimit: 0,
						hideCaptionAtLimit: 0,
						hideAllCaptionAtLilmit: 0,
						debugMode: false,
						fallbacks: {
							simplifyAll: "off",
							nextSlideOnWindowFocus: "off",
							disableFocusListener: false,
						}
					});
				}; /* END OF revapi call */
			}; /* END OF ON LOAD FUNCTION */
		}()); /* END OF WRAPPING FUNCTION */
	</script>
	
</body>

</html>