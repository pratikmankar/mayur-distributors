@extends('layouts.layout')

@section('title')
Mayur Distributors | VIVO
@endsection


@section('headers')
    <style>
        .product-name:hover{
            color:#fff;
            transition:.1s;
        }
    </style>
@endsection

@section('content')

<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-28 line__height-68 font__family-montserrat">
                            Vivo S1 Pro


                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Vivo S1 Pro

</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="main-wrapper">
    <main class="main-container pt-30">
        <div class="container">
            <div class="brk-sc-shop-item mb-30" data-brk-library="component__shop_item_page_sidebar">
                <div class="row">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <div class="brk-sc-shop-item__main">
                            <div class="row pb-20 justify-content-md-start justify-content-center">
                                <div class="col-lg-6 col-md-12 mb-4">
                                    
                                        <div class="slider-thumbnailed slick-loading fa-req">
                                            <div class="slider-thumbnailed-for arrows-modern" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "fade": true, "arrows": true, "autoplay": false, "autoplaySpeed": 4200}' data-brk-library="slider__slick" style="min-height: auto">
                                                <div>
                                                    <img src="{{('public/img/products/vivo/vivo-s1pro.jpeg')}}" alt="alt">
                                                </div>
                                                <div>
                                                    <img src="{{('public/img/products/vivo/vivo-s1pro-back.jpeg')}}" alt="alt">
                                                </div>
                                              
                                            </div>
                                            <div class="slider-thumbnailed-nav" data-slick='{"slidesToShow": 2, "slidesToScroll": 1}'>
                                               
                                            </div>
                                        </div>
                                        
                                </div>
                                <div class="col-lg-6 col-md-12 mt-4">
                                    <div class="brk-sc-shop-item__info">
                                       
                                        <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-light line__height-32">
                                            
                                            <span class="font__weight-bold">Vivo S1 Pro

</span>
                                        </h2>
                                        <div class="brk-sc-divider mt-10 mb-20"></div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3 class="brk-color-filter__title font__family-montserrat font__weight-bold font__size-16 text-uppercase text-left">
                                                    <span>Highlights</span>
                                                </h3>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>8 GB RAM | 128 GB ROM | Expandable Upto 256 GB</span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>16.21 cm (6.38 inch) Full HD+ Display</span>
                                                </h3>
                                             
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>48MP + 8MP + 2MP + 2MP | 32MP Front Camera
                                                    </span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>4500 mAh Lithium Polymer Battery
                                                    </span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>Qualcomm Snapdragon 665 Processor
                                                    </span>
                                                </h3>
                                            </div>
                                        
                                          
                                            <div class="col-lg-12">
                                                <hr class="underline-white">
                                                <div class="text-center text-lg-left mt-5">
                                                    <a href="{{url('/contact-us')}}" class="btn btn-inside-out btn-lg border-radius-25 btn-shadow ml-0 pl-50 pr-50 brk-library-rendered rendered" data-brk-library="component__button" tabindex="-1">
                                                        <span class="before">Enquire Now!</span><span class="text" style="min-width: 90.4375px;">Enquire Now!</span><span class="after">Enquire Now!</span>
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="brk-tabs brk-tabs_canted" data-brk-library="component__shop_item_page_sidebar">
                                <ul class="brk-tabs-nav font__family-montserrat font__size-14 font__weight-semibold line__height-14 text-uppercase">
                                    <li class="brk-tab">
                                        <span>Description</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Specifications</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Similar Products</span>
                                    </li>
                                </ul>
                                <div class="brk-tabs-content">
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    If you are looking for a phone that lets you take amazing pictures, the Vivo S1 Pro is for you. With a 32 MP front shooter that captures crystal-clear selfies. The phone also comes with a 48 MP AI quad-camera that lets you capture breathtaking pictures. It also packs in other features such an 8 MP super wide-angle camera, a 4 cm range for its macro camera with a frame-merging algorithm, a huge battery and an impressive 16.20 cm FHD+ Super AMOLED display. Its is powered by 8 GB of RAM and comes with a storage of 128 GB. The phone also comes with an in-display fingerprint scanner for a secure unlocking experience.
                                                </p>
                                                
                                            </div>
                                            <div class="col-md-12 col-lg-12">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">32 MP Front Shooter
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This smartphone’s rear camera comes with the Super Night mode which lets you click stunning pictures, even in the dark. You can utilise the Super Night Selfie feature to click clear selfies in the night.

                                                </p>
                                            </div>
                                            
                                            
                                            <div class="brk-sc-divider mt-10 mb-20"></div>

                                            
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">AI Quad Camera
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    With the Vivo S1 Pro comes with a powerful 48 MP AI quad-camera on the rear, expect nothing less than stunning photos that are breathtakingly beautiful. The remaining three cameras take care of all your wide-angle, macro and Bokeh needs. These cameras help you take pictures of almost professional levels irrespective of the scene you are capturing.

                                                </p>
                                            </div>
                                            <div class="brk-sc-divider mt-10 mb-20"></div>

                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">AI Super Wide-angle Camera
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Equipped with an 8 MP super wide-angle camera, it lets you expand your frame up to 120 degrees, so that you stop worrying about missing out on any details from your frame.


                                                </p>
                                            </div>
                                            
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Capture Intricate Details
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    The Vivo S1 Pro comes with a 4 cm range for its macro camera that also has a frame-merging algorithm. This unique feature of this phone’s camera lets you take pictures that have the minutest of details in the most picture-perfect manner.


                                                </p>
                                            </div>
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                          
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Powerful Performer
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Vivo S1 Pro is a powerful phone that comes with 8 GB of RAM. This helps the phone function in the most efficient manner. With a large storage capacity of 128 GB, you can be rest assured that you will never run out of space for all your favourite media files and pictures.

 
                                                </p>
                                            </div>
                                         
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                            
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Huge Battery
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    The phone comes with a large 4500 mAh battery. With the advanced 18 W Dual-Engine Fast Charging technology, the Vivo S1 Pro charges in no time.


                                                </p>
                                            </div>
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Impressive Display

                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    With a 90% screen-to-body ratio, this 16.20 cm FHD+ Super AMOLED display lets you enjoy all your multimedia content as well as the phone’s applications in the most vivid manner. Watching your favourite movies and TV series is an experience in itself.


                                                </p>
                                            </div>
                                           
                                            
                                         
                                        </div>
                                    </div>
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-16 font__weight-bold line__height-32">General</h2>
                                                <hr class="underline-white">
                                            </div>
                                        
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Model Number</h2>
                                                <p>Vivo 1920|PD1945F_EX

                                                </p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Model Name</h2>
                                                <p>
                                                    S1 Pro</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Color
                                                </h2>
                                                <p>Mystic Black
                                                </p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Browse Type</h2>
                                                <p>Smartphones</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">SIM Type</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Curve TV</h2>
                                                <p>Dual Sim</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Hybrid Sim Slot
                                                </h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Touchscreen</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">OTG Compatible</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Quick Charging
                                                </h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-3">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">SAR Value
                                                </h2>
                                                <p>Head - 1.191 W/kg, Body - 0.475 W/kg

                                                </p>
                                                <hr class="underline-white">
                                            </div>
                                           
                                            <div class="mb-3 col-md-6 col-lg-10">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">In The Box</h2>
                                                <p>
                                                    Handset, Headset, USB Cable, USB Power Adapter, SIM Ejector, Protective Case, Protective Film (Applied), Documentation
                                                </p>
                                                    <hr class="underline-white">
                                            </div>
                                            
                                        </div>
                                    </div>
                                      
                <div class="brk-grid__item Vivo" style="max-height:550px;">
                    <div class="container mt-50">
                        
                        <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": false, "responsive": [
                {"breakpoint": 1200, "settings": {"slidesToShow": 3}},
                {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/vivo-v17-vivo-1919-pd1948f-ex-original-imafmth6ezsgmfgj.jpg')}}" alt="iFFalcon tv by TCL">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo V17 (8GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/vivo-s1-pro-vivo-1920-original-imafnhwp7zhvmqkk.jpg')}}" alt="vivo mobiles distributors">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo S1 Pro (8GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/s1-64-d-1907-pd1913f-ex-vivo-6-original-imafgyfqfgswgq4w.jpg')}}" alt="tata chemicals distributors">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo S1 (4GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/vivo-y19-vivo-1915-pd1934f-ex-original-imafmcpd6cezg94d.jpg')}}" alt="telecom service distributors">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y19 (4GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/y91i-32-c-1820-vivo-2-original-imafegpdpjymsrwe.jpg')}}" alt="distributors in Pune">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y91i (2GB+32GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/y91i-32-d-1820-vivo-2-original-imafegpd6zyrmaqh.jpg')}}" alt="consumer goods company">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y91i (3GB+32GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/y15-64-a-1901-vivo-4-original-imafh432tfgwqatf.jpg')}}" alt="Electronic Products">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y15 (4GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/y12-64-b-1904-pd1901ef-ex-vivo-3-original-imafh4zjvmpfdy7v.jpg')}}" alt="consumer goods">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y12 (3GB+64GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/vivo-y11-vivo-1906-pd1930cf-ex-original-imafngxhtqbfdydg.jpg')}}" alt="consumer goods company in India">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y11 (3GB+32GB)
                                        </p>
                                    </div>
                                </div>
                            </div>
                         
                        </div>
                    </div>
                </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

    </main>
</div>

@endsection
              