@extends('layouts.layout')

@section('title')
2019 Best Distributor In Central Zone | Mayur Distributors
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="2019 Best Distributor In Central Zone | Mayur Distributors">
<meta name="description" content="In 2019 Mayur Distributors has been Awarded Best Distributor In Central Zone-Maharashtra By Vivo.First Price In Successful Launch Of Flagship Model V15pro | Entire S1-Series. etc">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors, distributors in Pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors, bbest distributor in maharashtra">
<link rel="canonical" href="{{url('/awards')}}">
<meta property="og:title" content="2019 Best Distributor In Central Zone | Mayur Distributors">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/awards">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Best Distributor Maharashtra">
<meta property="og:description"content="In 2019 Mayur Distributors has been Awarded Best Distributor In Central Zone-Maharashtra By Vivo.First Price In Successful Launch Of Flagship Model V15pro | Entire S1-Series. etc">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-48 line__height-68 font__family-montserrat">
                            Awards
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Awards</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <main class="main-container">
        <section id="awards" style="scroll-margin:100px 0px 0px 0px;">
            <div class="container">
                
                    <div class="text-center">
                        <h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30" data-brk-library="component__title">AWARDS</h2>
                        <div class="divider-cross wow zoomIn" data-brk-library="component__title">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
              
            </div>
            <div class="mb-50">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
							
							<div class="mb-70">
								<ul class="list-inline-5" data-brk-library="component__list">
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										
										Awarded ‘Best Distributor In Central Zone-Maharashtra’ By Vivo In 2019.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="iFFalcon Televisions by TCL" class="list-shape">
                                        <span class="after"></span>
										
									</li>
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										First Price In Successful Launch Of Flagship Model V15pro In 2019.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="Vivo Mobiles" class="list-shape">
                                        <span class="after"></span>
										
									</li>
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										First Price In Successful Launch Of Flagship Models Of Entire S1-Series In 2019.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="mayur distributors" class="list-shape">
                                        <span class="after"></span>
										
									</li>
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										Received Special Personalized Gift From ‘Vivo-India CEO Jerome Chen’ In 2019 On Completing 5 Years Of Committed Partnership.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="consumer goods" class="list-shape">
                                        <span class="after"></span>
										
                                    </li>
                                    <li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										
										Received ‘Best Distributor West-1’ From Tata Chemicals & ‘Best Sales Team (UPC) Award 2016’ In Honk Kong.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="consumer goods company in India" class="list-shape">
                                        <span class="after"></span>
										
									</li>
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										Received ‘Best Debut Award 2016’ From Panasonic In Thailand.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="consumer goods company" class="list-shape">
                                        <span class="after"></span>
										
									</li>
								</ul>
							</div>
						</div>
                        <div class="col-md-12 col-lg-6">
							
							<div class="mb-70">
								<ul class="list-inline-5" data-brk-library="component__list">
								
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										Received ‘Best Performing RDS Award – Lumia Growth’ 2014 By Microsoft.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="smart led tv" class="list-shape">
                                        <span class="after"></span>
										
									</li>
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										Received ‘Best Distributors In West 2012’ From Tata Chemicals.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="led tv" class="list-shape">
                                        <span class="after"></span>
										
									</li>
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										Received ‘National Award 2011’ From Canon In<br> New Delhi.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="Vivo Mobiles" class="list-shape">
                                        <span class="after"></span>
										
                                    </li>
                                    <li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										Winner Of ‘Best RDS On Sizzling Saturday’ In Entire West Region For Oct’18 By Nokia India Pvt. Ltd For 3 Crores In One Day.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="Tata Chemicals" class="list-shape">
                                        <span class="after"></span>
										
									</li>
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
										Received ‘Best Debut  Award 2010’ From Canon In <br>Macau.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="iFFalcon Televisions by TCL" class="list-shape">
                                        <span class="after"></span>
										
									</li>
									
									<li class="font__family-montserrat font__size-16 font__weight-bold list-counter text-left">
                                        Awarded ‘Best Innovative Performer’ For 2007 By HTC In Thailand.
                                        <img src="{{URL::to('public/img/shape-1.png')}}" alt="consumer goods" class="list-shape">
                                        <span class="after"></span>
										
									</li>
								</ul>
							</div>
						</div>
                    </div>
                </div>


                <div class="triple-slider slick-loading arrows-classic" data-brk-library="slider__slick,component__image_caption">
                    <div>
                        <div class="brk-ic-left-slide brk-ic-left-slide__pointer" data-brk-library="component__image_caption_css">
                            <a href="#" class="brk-ic-left-slide__link"></a>
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/shop-home-page/slider-1.jpg')}}" alt="consumer goods company" class="brk-ic-left-slide__img lazyload">
                            <div class="brk-ic-left-slide__overlay brk-base-bg-gradient-6-black"></div>
                            <div class="brk-ic-left-slide__wrapper brk-ic-left-slide__wrapper_gradient brk-base-bg-gradient-50deg-a">
                                <h3 class="brk-ic-left-slide__title font__size-21 line__height-28">
                                    <span class="font__family-montserrat font__weight-light">
                                        Network device
                                    </span>
                                    <br>
                                    <span class="font__family-montserrat font__weight-bold letter-spacing--20">
                                        & Personal computer
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brk-ic-left-slide brk-ic-left-slide__pointer" data-brk-library="component__image_caption_css">
                            <a href="#" class="brk-ic-left-slide__link"></a>
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/shop-home-page/slider-2.jpg')}}" alt="distributors in Pune" class="brk-ic-left-slide__img lazyload">
                            <div class="brk-ic-left-slide__overlay brk-base-bg-gradient-6-black"></div>
                            <div class="brk-ic-left-slide__wrapper brk-ic-left-slide__wrapper_gradient brk-base-bg-gradient-50deg-a">
                                <h3 class="brk-ic-left-slide__title font__size-21 line__height-28">
                                    <span class="font__family-montserrat font__weight-light">
                                        Wireless device
                                    </span>
                                    <br>
                                    <span class="font__family-montserrat font__weight-bold letter-spacing--20">
                                        & Portable media player
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brk-ic-left-slide brk-ic-left-slide__pointer" data-brk-library="component__image_caption_css">
                            <a href="#" class="brk-ic-left-slide__link"></a>
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/shop-home-page/slider-3.jpg')}}" alt="nikon distributors" class="brk-ic-left-slide__img lazyload">
                            <div class="brk-ic-left-slide__overlay brk-base-bg-gradient-6-black"></div>
                            <div class="brk-ic-left-slide__wrapper brk-ic-left-slide__wrapper_gradient brk-base-bg-gradient-50deg-a">
                                <h3 class="brk-ic-left-slide__title font__size-21 line__height-28">
                                    <span class="font__family-montserrat font__weight-light">
                                        Car audio
                                    </span>
                                    <br>
                                    <span class="font__family-montserrat font__weight-bold letter-spacing--20">
                                        & Display device
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="brk-ic-left-slide brk-ic-left-slide__pointer" data-brk-library="component__image_caption_css">
                            <a href="#" class="brk-ic-left-slide__link"></a>
                            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/shop-home-page/slider-3.jpg')}}" alt="iFFalcon Televisions by TCL" class="brk-ic-left-slide__img lazyload">
                            <div class="brk-ic-left-slide__overlay brk-base-bg-gradient-6-black"></div>
                            <div class="brk-ic-left-slide__wrapper brk-ic-left-slide__wrapper_gradient brk-base-bg-gradient-50deg-a">
                                <h3 class="brk-ic-left-slide__title font__size-21 line__height-28">
                                    <span class="font__family-montserrat font__weight-light">
                                        Network device
                                    </span>
                                    <br>
                                    <span class="font__family-montserrat font__weight-bold letter-spacing--20">
                                        & Personal computer
                                    </span>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
</div>
@endsection