@extends('layouts.layout')

@section('title')
Mayur Distributors | A Distributors company of consumer goods 
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="Mayur Distributors | A Distributors company of consumer goods">
<meta name="description" content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors, distributors in Pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors">
<link rel="canonical" href="{{url('/contact-us')}}">
<meta property="og:title" content="Mayur Distributors | A Distributors company of consumer goods ">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/contact-us">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Mayur Distributors">
<meta property="og:description"content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-48 line__height-68 font__family-montserrat">
                            Contact Us
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Contact Us</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <main class="main-container">
   
        <section class="pt-50 pb-25">
            <div class="container">
                <div class="brk-form brk-form-round" data-brk-library="component__form">
                <div class="row">   
                    <div class="col-lg-6">
                        <form id="contact-form">
                            <div class="brk-form-wrap mb-10 mt-0 p-0">
                                
                                <input id="brk-email-form-1" class="c_name brk-form-input-transparent" name="c_name" type="text" required="" placeholder="* Enter your name">
                            </div>
                            <div class="brk-form-wrap mb-10 mt-0 p-0">
                                
                                <input id="brk-pass-form-1" class="c_email brk-form-input-transparent" name="c_email" type="email" required="" placeholder="* Enter your E-Mail">
                            </div>
                            <div class="brk-form-wrap mb-10 mt-0 p-0">
                                
                                <input id="brk-pass-form-1" class="c_phone brk-form-input-transparent" name="c_phone" type="text" required="" pattern="[789][0-9]{9}" maxlength="10" placeholder="* Enter your Mobile Number">
                            </div>
                            <div class="brk-form-wrap mb-10 mt-0 p-0">
                                
                                <textarea id="brk-textarea-form-1" class="c_message brk-form-textarea-transparent" name="c_message" placeholder="Enter your Message"></textarea>
                            </div>
                            <div class="text-center">
                                <button type="submit" id="send_email" class="btn btn-inside-out btn-lg btn-icon border-radius-30 mt-25 btn-shadow" data-brk-library="component__button">
                                    <i class="fas fa-envelope icon-inside"></i>
                                    <span class="before">send message</span><span class="text">send message</span><span class="after">send message</span>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-5">
                        <div class="wow fadeInRight">
                            <h1 class="font__family-montserrat font__size-56 line__height-60 font__weight-thin mb-30">
                                Need Help? <br>
                                <span class="font__weight-bold">Contact Us</span>
                            </h1>
                            <p class="brk-dark-font-color font__size-14 line__height-26 mb-35">We would love to hear you from. Our experts are available to help you in the best way possible.                            </p>
                            <p class="font__family-open-sans font__weight-bold font__size-14 mb-15">
                                <i class="brk-footer-icon text-middle fa fa-envelope line__height-24 brk-base-font-color"></i>
                                <a href="mailto:info@mayurdistributors.com" class="show-inline-block">info@mayurdistributors.com</a>
                            </p>
                            <p class="font__family-open-sans font__weight-bold font__size-14 mb-15">
                                <i class="brk-footer-icon text-middle fas fa-map-marker-alt line__height-24 brk-base-font-color"></i>
                                <span>Pune (MH), India</span>
                            </p>
                            <p class="font__family-open-sans font__weight-bold font__size-14 mb-15">
                                <i class="brk-footer-icon text-middle fa fa-phone line__height-24 brk-base-font-color"></i>
                                <a href="tel:02026430632" class="show-inline-block">020-26430632</a>
                            </p>
                        </div>
                        <div class="text-left">
							<a href="https://bit.ly/3dgO73K" target="_blank" class="btn btn-prime btn-sm border-radius-10 font__family-open-sans font__weight-bold btn-min-width-200" data-brk-library="component__button">
								<i class="fa fa-location-arrow icon-inside" aria-hidden="true"></i>
								<span class="before"></span><span class="after"></span><span class="border-btn"></span>Get Directions
							</a>
						</div>
                    </div>
                </div>
            </div>
            </div>
        </section>
       
        <section>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3783.608005912979!2d73.86628481489251!3d18.501406887420178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x943e89da1706fb67!2sMayur%20Distributors!5e0!3m2!1sen!2sin!4v1584710219574!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </section>
    </main>
</div>
@endsection

@section('scripts')

<script>
    $('#contact-form').on("click", "#send_email", function(event) {
        
   var isvalidate = $("#contact-form")[0].checkValidity();
       if (isvalidate)
       {
           event.preventDefault();
                   var obj = new Object();
                   obj.c_name = $('.c_name').val();
                   obj.c_email = $('.c_email').val();
                   obj.c_phone = $('.c_phone').val();
                   obj.c_message = $('.c_message').val();
                   obj._token = "{{ csrf_token() }}";

                   $('#send_email').prop('disabled',true);

                   $.ajax({
                       type: "POST",
                       contentType:"application/json; charset=utf-8'",
                       dataType: "JSON",
                       data: JSON.stringify(obj),
                       url:"{{url('/send_email')}}",
                       success: function (responce) {
                           $('#send_email').prop('disabled', false);
                           toastr[responce.text](responce.message, responce.title);
                           $('#contact-form')[0].reset();
                   }
               });
           }
   });
</script>

@endsection