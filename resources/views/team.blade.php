@extends('layouts.layout')

@section('title')
2019 Best Distributor In Central Zone | Mayur Distributors
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="2019 Best Distributor In Central Zone | Mayur Distributors">
<meta name="description" content="In 2019 Mayur Distributors has been Awarded Best Distributor In Central Zone-Maharashtra By Vivo.First Price In Successful Launch Of Flagship Model V15pro | Entire S1-Series. etc">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors, distributors in Pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors, bbest distributor in maharashtra">
<link rel="canonical" href="{{url('/awards')}}">
<meta property="og:title" content="2019 Best Distributor In Central Zone | Mayur Distributors">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/awards">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Best Distributor Maharashtra">
<meta property="og:description"content="In 2019 Mayur Distributors has been Awarded Best Distributor In Central Zone-Maharashtra By Vivo.First Price In Successful Launch Of Flagship Model V15pro | Entire S1-Series. etc">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-48 line__height-68 font__family-montserrat">
                            Our Team
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Our Team</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <main class="main-container">


        <section class="pt-40 bg-white">
			<div class="text-center mb-50">
				<h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30 brk-library-rendered" data-brk-library="component__title">ABOUT THE DIRECTOR</h2>
				<div class="divider-cross wow zoomIn brk-library-rendered" data-brk-library="component__title" style="visibility: visible; animation-name: zoomIn;">
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="container">
				<div class="row no-gutters-center">
                    <div class="col-12 col-md-7">
                        <div class="align-items-center">
                            <p class="brk-dark-font-color text-justify font__size-16 font__weight-normal line__height-26 align-items-center pt-4">
                                An MBA In Marketing & Finance, Mr. Sharad Shah Carries A Rich Business Experience Of More Than 3 Decades In Distribution Realm.
                           <br>
                           <br>
                                A Results-oriented Director Driven To Manage Costs And Establish Strategic Mutually Beneficial Partnerships And Relationships With Users Vendors And Clients.
                           <br>
                           <br>
                                Skillful At Creating Strategic Alliances With Organization Leaders To Effectively Align With And Support Key Business Initiatives. Excel At Building And Retaining High Performance Teams By Motivating Skilled Professionals.
                           </p>
                           <div class="text-right">
                               <h4 class="mt-5">Mr. Sharad Shah</h4>
                               <h6>Director<br>Mayur Distributors</h6>
                           </div>
                        </div>
                    </div>
					<div class="col-12 col-md-5 text-center">

                        <div class="pl-15 pr-15">
                            <article class="brk-team-persone-circle text-center" data-brk-library="component__team">
                                
                                <div class="brk-team-persone-circle__bg lazyload" data-bg="{{URL::to('public/img/about_us/director.png')}}">
                                    <span class="brk-team-persone-circle__bg-overlay">
                                        <span class="before brk-base-bg-gradient-90deg"></span>
                                        <svg viewBox="0 0 270 37">
                                            <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                        </svg>
                                    </span>
                                    <ul class="brk-team-persone-circle__contacts">
                                    
                                        <li class="text-left">
                                            <a href="javascript:void(0);" style="font-size: 18px; font-weight:700;">Mr. Sharad Shah</a>
                                        </li>
                                        <li class="text-left">
                                            <a href="javascript:void(0);" style="font-size: 16px; font-weight:700;">Director</a>
                                        </li>
                                        <br>
                                        <li>
                                            <i class="fas fa-phone" aria-hidden="true"></i>
                                            <a href="tel:+919822014774">+91 9822014774</a>
                                        </li>
                                        <li>
                                            <i class="far fa-envelope" aria-hidden="true"></i>
                                            <a href="mailto:sharad@mayurdistributors.com" style="font-size:12px;">sharad@mayurdistributors.com</a>
                                        </li>
                                       
                                    </ul>
                                </div>
                                <div class="brk-team-persone-circle__social-links">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </article>
                        </div>      
                    {{-- <img src="{{URL::to('public/img/about_us/director.png')}}" class="img-fluid" alt="Mayur Distributors"> --}}
					</div>
				
				</div>
			</div>
        </section>


        <section class="mb-50" id="team" style="scroll-margin-top:100px !important;">
            <div class="container mb-50">
                <div class="text-center">
                    <h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30" data-brk-library="component__title">OUR TEAM</h2>
                    <div class="divider-cross wow zoomIn" data-brk-library="component__title">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="default-slider slick-loading arrows-classic-dark fa-req" data-slick='{"slidesToShow": 4, "slidesToScroll": 1,
"responsive": [
{"breakpoint": 992, "settings": {"slidesToShow": 3}},
{"breakpoint": 768, "settings": {"slidesToShow": 2}},
{"breakpoint": 480, "settings": {"slidesToShow": 1}}
], "autoplay": true, "autoplaySpeed": 2000}' data-brk-library="slider__slick">
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Mahesh Manchale</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Finance - Head</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                    <li>
                                        <a href="javascript:void(0);">Department - Finance</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Brand - All</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Firm - All</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mrs. Deepali Shinde</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Accounts Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                    <li>
                                        <a href="javascript:void(0);">Department - Finance</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Brand - All</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Firm - All</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Kunal Shah</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Director</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - IFFALCON</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Mayur Telecom Pvt. Ltd</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Manoj Patil</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - Sales</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Vivo</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Harsh Communication</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Chirag Shah</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Dev. Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - Sales</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Vivo</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Harsh Communication</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Shivaji Desai</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Dev. Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - Sales</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Vivo</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Harsh Communication</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Imran Shaikh</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">MIS Head</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - MIS</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - All</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Firm - All</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Shaheen Shingde</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Sales & Collection</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Tata Chemicals</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Mayur Distributors</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Vikas Konde</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Sales & Collection</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Nikon</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Mayur Distributors</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Chetan Deshpande</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - Sales</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Micromax</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Mayur Telecom Pvt Ltd</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                   
                </div>
            </div>
        </section>
    

    </main>
</div>
@endsection