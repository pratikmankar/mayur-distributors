@extends('layouts.layout')

@section('title')
TVs and Electronics distributor in Pune -Mayur Distributors
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="TVs and Electronics distributor in Pune -Mayur Distributors">
<meta name="description" content="Mayur Distributors -a reputed distribution company in Pune. Our core service is distributorship, iFFalcon Smart TVs, Electronics, Tata Chemicals, Vivo, and Micromax Mobiles distributor in Pune. ✓Get a Free Quote Today 020-26430632">
<meta name="keywords" content="vivo mobiles distributors, Vivo, iFFalcon by TCL, led tv, smart led  tv, micromax mobiles distributors, Micromax, Nikon, nikon India, nikon distributors, Tata Chemicals, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors">
<link rel="canonical" href="{{url('/products')}}">
<meta property="og:title" content="TVs and Electronics distributor in Pune -Mayur Distributors">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/products">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Electronics distributor in Pune">
<meta property="og:description"content="Mayur Distributors -Ass reputed distribution company in Pune. Our core service is distributorship, iFFalcon Smart TVs, Electronics, Tata Chemicals, Vivo, and Micromax Mobiles distributor in Pune. ✓Get a Free Quote Today 020-26430632">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-44 line__height-68 font__family-montserrat">
                            Products
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-14 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Products</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <main class="main-container">
        <div class="container mt-50 mb-50" style="overflow:hidden">
            <ul class="brk-filters brk-filters_style-3 justify-content-center d-flex flex-wrap" data-brk-library="component__gallery,component__shop_grid_filter">
                <li class="brk-filters__item active" data-filter="*">
                    <i class="fal fa-th"></i>
                    <span>All</span>
                </li>
                <li class="brk-filters__item" data-filter=".Vivo">
                    <span>Vivo</span>
                </li>
                <li class="brk-filters__item" data-filter=".iFFALCON">
                    <span>iFFALCON</span>
                </li>
                <li class="brk-filters__item" data-filter=".Micromax">
                    <span>Micromax</span>
                </li>
                <li class="brk-filters__item" data-filter=".Nikon">
                    <span>Nikon</span>
                </li>
                <li class="brk-filters__item" data-filter=".Tata-Chemicals">
                    <span>Tata Chemicals</span>
                </li>
            </ul>
            <div class="brk-gallery brk-grid" data-grid-cols="1" data-brk-library="component__portfolio_isotope">
                <div class="brk-grid__sizer"></div>
                
                <div class="brk-grid__item Vivo" style="max-height:550px;">
                    <div class="container mt-50">
                        <div class="mb-50">
                            <div class="text-left">
                                <h3 class="font__family-montserrat font__size-24 font__weight-bold">
                                    <span class="heading__number">01</span> Vivo
                                </h3>
                            </div>
                        </div>
                        <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": false, "responsive": [
                {"breakpoint": 1200, "settings": {"slidesToShow": 3}},
                {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/vivo-v17-vivo-1919-pd1948f-ex-original-imafmth6ezsgmfgj.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo V17 (8GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/vivo-s1-pro-vivo-1920-original-imafnhwp7zhvmqkk.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo S1 Pro (8GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/s1-64-d-1907-pd1913f-ex-vivo-6-original-imafgyfqfgswgq4w.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo S1 (4GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/vivo-y19-vivo-1915-pd1934f-ex-original-imafmcpd6cezg94d.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y19 (4GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/y91i-32-c-1820-vivo-2-original-imafegpdpjymsrwe.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y91i (2GB+32GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/y91i-32-d-1820-vivo-2-original-imafegpd6zyrmaqh.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y91i (3GB+32GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/y15-64-a-1901-vivo-4-original-imafh432tfgwqatf.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y15 (4GB+128GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/y12-64-b-1904-pd1901ef-ex-vivo-3-original-imafh4zjvmpfdy7v.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y12 (3GB+64GB)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/vivo-y11-vivo-1906-pd1930cf-ex-original-imafngxhtqbfdydg.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Vivo Y11 (3GB+32GB)
                                        </p>
                                    </div>
                                </div>
                            </div>
                         
                        </div>
                    </div>
                </div>



                <div class="brk-grid__item iFFALCON" style="max-height:550px;">
                    <div class="container mb-50">
                        <div class="mb-50 mt-50">
                            <div class="text-left">
                                <h3 class="font__family-montserrat font__size-24 font__weight-bold">
                                    <span class="heading__number">02</span> iFFALCON
                                </h3>
                            </div>
                        </div>
                        <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": false, "responsive": [
                        {"breakpoint": 1200, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                        {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                        ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/iffalcon-1080p-wide-angle-12mp-waterproof-under-water-portable-original-imafq2fncukhwrc4.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            iFFALCON 32F2A
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/iffalcon-43k31-original-imafm59fudczqhuy.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            iFFALCON 43K31
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/iffalcon-50k31-original-imafm59fmsdhh8uj.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            iFFALCON 50K31
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/iffalcon-1080p-wide-angle-12mp-waterproof-under-water-portable-original-imafq2fncukhwrc43.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            iFFALCON 40F2A
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/iffalcon-55k31-original-imafm59fa7ftgpkm.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            iFFALCON 55K31
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/iffalcon-65v2a-65v2a-original-imafm59fpm5wnxbb.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            iFFALCON 65V2A
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/iffalcon-1080p-wide-angle-12mp-waterproof-under-water-portable-original-imafq2fncukhwrc44.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            iFFALCON 49F2A
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/iffalcon-led-32e3-1.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            iFFALCON 32E3
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                          
                         
                        </div>
                    </div>
                </div>



                <div class="brk-grid__item Micromax" style="max-height:550px;">
                    <div class="container mb-50">
                        <div class="mb-50 mt-50">
                            <div class="text-left">
                                <h3 class="font__family-montserrat font__size-24 font__weight-bold">
                                    <span class="heading__number">03</span> Micromax
                                </h3>
                            </div>
                        </div>
                        <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": false, "responsive": [
                        {"breakpoint": 1200, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                        {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                        ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/136079-v2-micromax-ione-note-mobile-phone-hres-1.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            One Note
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/Q4002N.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Q4002N Bharat 4
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/Q402+.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Q402+ Bharat 2
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-bharat-5-infinity-edition-q4204-original-imafbebtvbd5zsuj.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Q4204 Bharat 5
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-bharat-4-q440-original-imaeyt6cwmyzmdc4.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Q4204 Bharat 4
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-infinity-n11-n8216-pd6201m-original-imafmvfrhvrfmrtz.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            N11
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-infinity-n12-n8301-pd6202m-original-imafmvfrshsg2pze.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            N12
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-x378-x378-original-imafh6urnqdwkrwn.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            X378
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-x412-x412-original-imafgwgsn9kcywzg.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            X412
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-x388-x388-original-imafnbhewz9zccg4.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            X388
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-x421-x421-original-imafz6mzdg8qz8ke.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            X421
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/Micromax-X419.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            X419
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-x744-x744-original-imafjxywkvsjdymz.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            X744
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/micromax-x809-x809-original-imafjsfjgynt3u9z.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            X809
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                          
                         
                        </div>
                    </div>
                </div>


                <div class="brk-grid__item Nikon" style="max-height:550px;">
                    <div class="container mb-50">
                        <div class="mb-50 mt-50">
                            <div class="text-left">
                                <h3 class="font__family-montserrat font__size-24 font__weight-bold">
                                    <span class="heading__number">04</span> Nikon
                                </h3>
                            </div>
                        </div>
                        <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": false, "responsive": [
                        {"breakpoint": 1200, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                        {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                        ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/nikon-d3500-d3500-nikon-original-imafhr8tdrzvavse.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            D3500 (Dzoom 70-300)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/na-d3500-nikon-original-imaf9fgtzqxgtk5s.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            D3500 (18-55)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/71huW72qomL._SX450_.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            D5600 Dzoom
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/71mKvbRR5BL._SL1280_.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            D5600 Kit (18-140)
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/d5600-kit.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            D5600 Kit (18-55)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/nikon-d750-original-imaefpvj8np8u3d5.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            D750
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/na-d850-nikon-original-imaf6v2t2zp6mekb.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            D850
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/na-z-6-nikon-original-imafbk4gpzfbgafs.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Z6 with 24-70mm
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/nikon-af-p-dx-nikkor-70-300-mm-f-4-5-6-3g-ed-vr-original-imaf9v2sf7kqjmm6.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            AF-S NIKKOR 70-200mm (Lens)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/nikon-af-s-dx-nikkor-18-200mm-f-3-5-5-6g-ed-vr-ii-original-imafyerykbg39tvs.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            AF-S NIKKOR 200-500mm (Lens)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/sigma-100-400mm-f5-6-3-dg-os-hsm-contemporary-lens-for-canon-original-imaetneswvxwvden.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            AF 50mm F/1.4D (Lens)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/nikon-af-s-dx-nikkor-35-mm-f-1-8g-lens-original-imaffvfvmynktjg8.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            AF-S DX 35mm F/1.8G (Lens)
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                        
                         
                        </div>
                    </div>
                </div>

                <div class="brk-grid__item Tata-Chemicals" style="max-height:550px;">
                    <div class="container mb-50">
                        <div class="mb-50 mt-50">
                            <div class="text-left">
                                <h3 class="font__family-montserrat font__size-24 font__weight-bold">
                                    <span class="heading__number">05</span> Tata Chemicals
                                </h3>
                            </div>
                        </div>
                        <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": false, "responsive": [
                        {"breakpoint": 1200, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                        {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                        {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                        ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/1-na-iodized-salt-tata-original-imafc7agxkbypvgr.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Tata Salt
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/freedomcart-tata-salt-lite.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Tata Salt Lite
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/ishakti.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Tata iShakti
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/1-rock-salt-1kg-rock-salt-tata-original-imafmkbxt7xrfqz7.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Tata Rock Salt
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/91vJ4AtUg1L._SL1500_.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            All Varients Pulses
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/fd1513e9-f477-42e9-9246-cc41efd4e271.__CR0,0,300,300_PT0_SX300_V1___.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Besan
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/tata_spices.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Spices
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/61PdBA68uKL._SX425_.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Soda
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/500-white-thik-poha-pouch-tata-sampann-original-imafkdfushstpu6g.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Poha
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/nutrimixes.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Nutrimixes
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="swiper-slide">
                                    <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                        <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                        <img original src="{{('public/img/products/Tata+Sampann_Packaging_Elephant+Design+8.jpg')}}" alt="">
                                        <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                            Snacks
                                        </p>
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </main>
</div>
@endsection