@extends('layouts.layout')

@section('title')
Mayur Distributors | A Distributors company of consumer goods 
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="Mayur Distributors | A Distributors company of consumer goods ">
<meta name="description" content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors, distributors in Pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors">
<link rel="canonical" href="{{url('/')}}">
<meta property="og:title" content="Mayur Distributors | A Distributors company of consumer goods ">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Mayur Distributors">
<meta property="og:description"content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')

<div id="rev_slider_7_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="demo_creative_agency_full"
    data-source="gallery"
    style="background:linear-gradient(50deg,rgba(15,90,224,1)0%,rgba(15,90,224,1)0%,rgba(116,0,186,1)100%,rgba(116,0,186,1)100%);padding:0px;">
    <div id="rev_slider_7_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8.1">
        <ul>


            <li data-index="rs-16" data-transition="slideright" data-slotamount="default" data-hideafterloop="0"
                data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default"
                data-thumb="{{URL::to('public/img/demo_creative_agency_full/images/100x50_4549a-brk_slide-1.jpg')}}"
                data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2=""
                data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                data-param10="" data-description="">
                <img src="{{URL::to('public/img/demo_creative_agency_full/images/4549a-brk_slide-1.jpg')}}" alt=""
                    data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off"
                    class="rev-slidebg" data-no-retina>
                <div class="tp-caption rev_group" id="slide-16-layer-27" data-x="['left','top','top','bottom']"
                    data-hoffset="['50','0','5','0']" data-y="['top','top','top','bottom']"
                    data-voffset="['200','503','605','100']" data-width="['430','430','430','300']"
                    data-height="['240','240','240','400']" data-whitespace="nowrap" data-type="group"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','center']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 7; min-width: 430px; max-width: 430px; max-width: 240px; max-width: 240px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    
                    <div class="tp-caption" id="slide-16-layer-4" data-x="['left','center','center','center']"
                        data-hoffset="['0','31','31','26']" data-y="['top','center','center','center']"
                        data-voffset="['50','50','50','0']" data-fontsize="['44','44','44','42']"
                        data-lineheight="['64','64','64','40']" data-width="['388','388','0','0']"
                        data-height="['350','400','400','200']" data-whitespace="normal" data-type="text"
                        data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['left','center','center','center']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 9; min-width: 388px; max-width: 388px; max-width: 129px; max-width: 129px; white-space: normal; font-size: 72px; line-height: 64px; font-weight: 700; color: #272727; letter-spacing: 0px;font-family:Montserrat;">
                        <span style="color:#000; font-size:30px !important;"></span>
                        <div style="">Been in the Distribution sector since <span style="color:#305dde;">1975.</span></div>
                    </div>
                </div>
                <div class="tp-caption rev_group" id="slide-16-layer-30" data-x="['right','right','right','right']"
                    data-hoffset="['1009','830','830','830']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['130','90','90','90']" data-width="175" data-height="170" data-whitespace="nowrap"
                    data-visibility="['on','off','off','off']" data-type="group" data-basealign="slide"
                    data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 11; min-width: 175px; max-width: 175px; max-width: 170px; max-width: 170px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    <div class="tp-caption" id="slide-16-layer-23" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                        data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                        data-type="image" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]" style="z-index: 12;"><img
                            src="{{URL::to('public/img/demo_creative_agency_full/images/18d65-brk_slide_icon-1.png')}}"
                            alt="" data-ww="" data-hh="" data-no-retina> </div>
                    <div class="tp-caption" id="slide-16-layer-15" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                        data-voffset="['100','100','100','100']" data-width="none" data-height="none"
                        data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 13; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 300; color: #272727; letter-spacing: 0px;font-family:Montserrat;text-transform:uppercase;">
                        Effective <div style="font-weight: 700">Promotion</div>
                    </div>
                </div>
                <div class="tp-caption rev_group" id="slide-16-layer-36" data-x="['right','right','right','right']"
                    data-hoffset="['742','593','594','594']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['130','90','90','90']" data-width="175" data-height="170" data-whitespace="nowrap"
                    data-visibility="['on','off','off','off']" data-type="group" data-basealign="slide"
                    data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 14; min-width: 175px; max-width: 175px; max-width: 170px; max-width: 170px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    <div class="tp-caption" id="slide-16-layer-37" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                        data-voffset="['100','100','100','100']" data-width="none" data-height="none"
                        data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 15; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 300; color: #272727; letter-spacing: 0px;font-family:Montserrat;text-transform:uppercase;">
                        Advanced <div style="font-weight: 700">Research</div>
                    </div>
                    <div class="tp-caption" id="slide-16-layer-24" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                        data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                        data-type="image" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]" style="z-index: 16;"><img
                            src="{{URL::to('public/img/demo_creative_agency_full/images/0d0c8-brk_slide_icon-2.png')}}"
                            alt="" data-ww="" data-hh="" data-no-retina> </div>
                </div>
                <div class="tp-caption rev_group" id="slide-16-layer-39" data-x="['right','right','right','right']"
                    data-hoffset="['473','357','357','357']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['130','90','90','90']" data-width="175" data-height="170" data-whitespace="nowrap"
                    data-visibility="['on','off','off','off']" data-type="group" data-basealign="slide"
                    data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 17; min-width: 175px; max-width: 175px; max-width: 170px; max-width: 170px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    <div class="tp-caption" id="slide-16-layer-41" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                        data-voffset="['100','100','100','100']" data-width="none" data-height="none"
                        data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 18; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 300; color: #272727; letter-spacing: 0px;font-family:Montserrat;text-transform:uppercase;">
                        Effective <div style="font-weight: 700">Promotion</div>
                    </div>
                    <div class="tp-caption" id="slide-16-layer-25" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                        data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                        data-type="image" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]" style="z-index: 19;"><img
                            src="{{URL::to('public/img/demo_creative_agency_full/images/31ba2-brk_slide_icon-3.png')}}"
                            alt="" data-ww="" data-hh="" data-no-retina> </div>
                </div>
                <div class="tp-caption rev_group" id="slide-16-layer-43" data-x="['right','right','right','right']"
                    data-hoffset="['184','98','98','98']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['130','90','90','90']" data-width="204" data-height="170" data-whitespace="nowrap"
                    data-visibility="['on','off','off','off']" data-type="group" data-basealign="slide"
                    data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 20; min-width: 204px; max-width: 204px; max-width: 170px; max-width: 170px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    <div class="tp-caption" id="slide-16-layer-46" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                        data-voffset="['100','100','100','100']" data-width="none" data-height="none"
                        data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 21; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 300; color: #272727; letter-spacing: 0px;font-family:Montserrat;text-transform:uppercase;">
                        Amazing <div style="font-weight: 700">Development</div>
                    </div>
                    <div class="tp-caption" id="slide-16-layer-26" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                        data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                        data-type="image" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]" style="z-index: 22;"><img
                            src="{{URL::to('public/img/demo_creative_agency_full/images/8d850-brk_slide_icon-4.png')}}"
                            alt="" data-ww="" data-hh="" data-no-retina> </div>
                </div>
                <div class="tp-caption rev_group" id="slide-16-layer-48" data-x="['right','center','center','center']"
                    data-hoffset="['165','0','1','0']" data-y="['top','top','top','middle']"
                    data-voffset="['132','106','109','-140']" data-width="['630','629','480','320']"
                    data-height="['390','374','373','394']" data-whitespace="nowrap" data-type="group"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 24; min-width: 630px; max-width: 630px; max-width: 390px; max-width: 390px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    <div class="tp-caption tp-resizeme" id="slide-16-layer-11"
                        data-x="['left','center','center','center']" data-hoffset="['-50','0','0','0']"
                        data-y="['top','top','top','top']" data-voffset="['100','194','0','132']"
                        data-fontsize="['46','46','46','38']" data-width="none" data-height="none"
                        data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","split":"chars","splitdelay":0.05,"speed":600,"split_direction":"forward","frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 26; white-space: nowrap; font-size: 46px; line-height: 46px; font-weight: 300; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">
                        Welcome To </div>
                    <div class="tp-caption tp-resizeme" id="slide-16-layer-10"
                        data-x="['left','center','center','middle']" data-hoffset="['-50','0','0','0']"
                        data-y="['top','top','top','middle']" data-voffset="['100','0','50','0']"
                        data-fontsize="['80','80','50','40']" data-lineheight="['210','186','145','145']"
                        data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                        data-responsive_offset="on"
                        data-frames='[{"delay":"+0","split":"chars","splitdelay":0.1,"speed":1500,"split_direction":"forward","frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 25; white-space: nowrap; font-size: 80px; line-height: 100px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">
                        Mayur Distributors </div>
                    <div class="tp-caption tp-resizeme" id="slide-16-layer-12"
                        data-x="['left','center','center','center']" data-hoffset="['-50','0','0','0']"
                        data-y="['top','top','top','center']" data-voffset="['280','280','203','203']"
                        data-fontsize="['16','20','22','20']" data-lineheight="['26','26','30','30']"
                        data-width="['626','626','457','320']" data-height="['79','79','none','none']"
                        data-whitespace="normal" data-type="text" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','center','center','center']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 27; min-width: 626px; max-width: 626px; max-width: 79px; max-width: 79px; white-space: normal; font-size: 16px; line-height: 26px; font-weight: 400; color: rgba(255,255,255,0.5); letter-spacing: 0px;font-family:Open Sans;">
                       Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores voluptate officia iste nulla laborum eius, amet corporis et accusantium nesciunt deleniti quisquam excepturi repudiandae, saepe pariatur corrupti, nihil perspiciatis alias?</div>
                </div>
                <div class="tp-caption" id="slide-16-layer-7" data-x="['right','right','right','right']"
                    data-hoffset="['-140','-140','-140','-140']" data-y="['top','top','top','top']"
                    data-voffset="['73','73','73','73']" data-width="none" data-height="none" data-whitespace="nowrap"
                    data-visibility="['on','on','off','off']" data-type="image" data-basealign="slide"
                    data-responsive_offset="on" data-responsive="off"
                    data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:left;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 5;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/0bb69-brk_slide_element-3.png')}}"
                        alt="" data-ww="['220px','220px','220px','220px']" data-hh="['207px','207px','207px','207px']"
                        data-no-retina> </div>
                <div class="tp-caption" id="slide-16-layer-9" data-x="['right','right','right','right']"
                    data-hoffset="['-54','-54','-54','-54']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['100','100','100','100']" data-width="none" data-height="none"
                    data-whitespace="nowrap" data-visibility="['on','on','off','off']" data-type="image"
                    data-basealign="slide" data-responsive_offset="off" data-responsive="off"
                    data-frames='[{"delay":10,"speed":1000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:left;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 6;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/835ab-brk_slide_element-4.png')}}"
                        alt="" data-ww="['119px','119px','119px','119px']" data-hh="['97px','97px','97px','97px']"
                        data-no-retina> </div>
                <div class="tp-caption tp-shape tp-shapewrapper" id="slide-16-layer-50"
                    data-x="['right','right','right','right']" data-hoffset="['81','80','80','80']"
                    data-y="['bottom','bottom','bottom','bottom']" data-voffset="['41','40','40','40']"
                    data-width="['1205','1000','1000','1000']" data-height="['300','270','270','270']"
                    data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="shape"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 10;background-color:rgb(255,255,255);box-shadow:0 6px 30px rgba(0,0,0,.12);margin:30px;">
                </div>
                <div class="tp-caption" id="slide-16-layer-1" data-x="['left','left','left','left']"
                    data-hoffset="['200','200','200','200']" data-y="['middle','middle','middle','middle']"
                    data-voffset="['-60','-60','-60','-60']" data-width="none" data-height="none"
                    data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="image"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:[-100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 23;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/eb646-1122x1237_1.png')}}" alt=""
                        data-ww="['550px','550px','550px','550px']" data-hh="['617px','617px','617px','617px']"
                        data-no-retina> </div>
            </li>
            <li data-index="rs-17" data-transition="slideleft" data-slotamount="default" data-hideafterloop="0"
                data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1800"
                data-thumb="{{URL::to('public/img/demo_creative_agency_full/images/100x50_32ebd-brk_slide-2.jpg')}}"
                data-delay="5000" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1=""
                data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8=""
                data-param9="" data-param10="" data-description="">
                <img src="{{URL::to('public/img/demo_creative_agency_full/images/32ebd-brk_slide-2.jpg')}}" alt=""
                    data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off"
                    class="rev-slidebg" data-no-retina>
                <div class="tp-caption rev_group" id="slide-17-layer-27" data-x="['left','left','center','center']"
                    data-hoffset="['125','125','0','0']" data-y="['middle','middle','top','middle']"
                    data-voffset="['0','0','140','-200']" data-width="['774','774','504','504']"
                    data-height="['203','203','228','228']" data-whitespace="nowrap" data-type="group"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','center','center']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 15; min-width: 774px; max-width: 774px; max-width: 203px; max-width: 203px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    <div class="tp-caption tp-resizeme" id="slide-17-layer-11"
                        data-x="['left','left','center','center']" data-hoffset="['0','0','0','0']"
                        data-y="['top','top','top','top']" data-voffset="['0','0','0','0']"
                        data-fontsize="['100','100','72','72']" data-lineheight="['96','96','72','72']"
                        data-width="['763','763','491','491']" data-height="none" data-whitespace="normal"
                        data-type="text" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","split":"chars","splitdelay":0.05,"speed":1000,"split_direction":"forward","frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"nothing"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','center','center']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 16; min-width: 763px; max-width: 763px; white-space: normal; font-size: 86px; line-height: 96px; font-weight: 300; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">
                        Hello for you & your business! </div>
                </div>
                <div class="tp-caption rev_group" id="slide-17-layer-28" data-x="['left','left','center','center']"
                    data-hoffset="['125','125','0','0']" data-y="['middle','middle','middle','middle']"
                    data-voffset="['135','135','200','329']" data-width="['380','380','380','311']"
                    data-height="['40','40','40','39']" data-whitespace="nowrap" data-type="group"
                    data-basealign="slide" data-responsive_offset="on" data-responsive="off"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 17; min-width: 380px; max-width: 380px; max-width: 40px; max-width: 40px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    <div class="tp-caption" id="slide-17-layer-14" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']" data-fontsize="['15','15','15','13']" data-width="none"
                        data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="off"
                        data-responsive="off"
                        data-frames='[{"delay":"+0","speed":500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 18; white-space: nowrap; font-size: 15px; line-height: 20px; font-weight: 400; color: rgba(255,255,255,0.3); letter-spacing: 0px;font-family:Open Sans;">
                        Find now: </div>
                    <div class="tp-caption" id="slide-17-layer-16" data-x="['left','left','left','left']"
                        data-hoffset="['86','86','86','72']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']" data-fontsize="['15','15','15','13']" data-width="none"
                        data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="off"
                        data-responsive="off"
                        data-frames='[{"delay":"+0","speed":500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 19; white-space: nowrap; font-size: 15px; line-height: 20px; font-weight: 400; color: rgba(255,255,255,0.3); letter-spacing: 0px;font-family:Open Sans;">
                        #blog </div>
                    <div class="tp-caption" id="slide-17-layer-17" data-x="['left','left','left','left']"
                        data-hoffset="['147','147','147','121']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']" data-fontsize="['15','15','15','13']" data-width="none"
                        data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="off"
                        data-responsive="off"
                        data-frames='[{"delay":"+0","speed":500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 20; white-space: nowrap; font-size: 15px; line-height: 20px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;text-decoration:none;">
                        #business </div>
                    <div class="tp-caption" id="slide-17-layer-18" data-x="['left','left','left','left']"
                        data-hoffset="['240','240','240','198']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']" data-fontsize="['15','15','15','13']" data-width="none"
                        data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="off"
                        data-responsive="off"
                        data-frames='[{"delay":"+0","speed":500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 21; white-space: nowrap; font-size: 15px; line-height: 20px; font-weight: 400; color: rgba(255,255,255,0.3); letter-spacing: 0px;font-family:Open Sans;">
                        #portfolio </div>
                    <div class="tp-caption" id="slide-17-layer-19" data-x="['left','left','left','left']"
                        data-hoffset="['331','331','331','273']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']" data-fontsize="['15','15','15','13']" data-width="none"
                        data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="off"
                        data-responsive="off"
                        data-frames='[{"delay":"+0","speed":500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 22; white-space: nowrap; font-size: 15px; line-height: 20px; font-weight: 400; color: rgba(255,255,255,0.3); letter-spacing: 0px;font-family:Open Sans;">
                        #art </div>
                </div>
                <div class="tp-caption rev_group" id="slide-17-layer-29" data-x="['left','left','center','center']"
                    data-hoffset="['125','125','0','0']" data-y="['middle','middle','top','middle']"
                    data-voffset="['295','295','384','100']" data-width="352" data-height="170" data-whitespace="nowrap"
                    data-type="group" data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 23; min-width: 352px; max-width: 352px; max-width: 170px; max-width: 170px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    <a class="tp-caption rev-btn tp-resizeme"
                        href="https://www.youtube.com/watch?v=cZM8OoYu4So&feature=youtu.be" target="_blank"
                        id="slide-17-layer-20" data-x="['left','left','left','left']"
                        data-hoffset="['40','40','40','40']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                        data-type="button" data-actions="" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,0.83);bc:rgba(255,255,255,0.67);"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[22,22,22,22]" data-paddingright="[41,41,41,41]"
                        data-paddingbottom="[22,22,22,22]" data-paddingleft="[104,104,104,104]"
                        style="z-index: 24; white-space: nowrap; font-size: 15px; line-height: 24px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: ;font-family:Montserrat;text-transform:uppercase;background-color:rgba(255,255,255,0);border-color:rgba(255,255,255,0.14);border-style:solid;border-width:2px 2px 2px 2px;border-radius:40px 40px 40px 40px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Watch
                        the video </a>
                    <div class="tp-caption tp-resizeme" id="slide-17-layer-21" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                        data-type="image" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]" style="z-index: 25;"><img
                            src="{{URL::to('public/img/demo_creative_agency_full/images/4c2d9-brk_button-1.png')}}"
                            alt="" data-ww="" data-hh="" data-no-retina> </div>
                </div>
                <div class="tp-caption rev_group" id="slide-17-layer-30" data-x="['left','left','center','center']"
                    data-hoffset="['464','464','0','0']" data-y="['middle','middle','top','top']"
                    data-voffset="['297','297','557','557']" data-width="274" data-height="110" data-whitespace="nowrap"
                    data-type="group" data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                    data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                    data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                    data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                    data-paddingleft="[0,0,0,0]"
                    style="z-index: 26; min-width: 274px; max-width: 274px; max-width: 110px; max-width: 110px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                    <a class="tp-caption rev-btn tp-resizeme" href="https://1.envato.market/Bd5V9" target="_blank"
                        id="slide-17-layer-23" data-x="['left','left','left','left']"
                        data-hoffset="['11','11','11','11']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['-2','-2','-2','-2']" data-width="none" data-height="none"
                        data-whitespace="nowrap" data-type="button" data-actions="" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,0.83);bc:rgba(255,255,255,0.67);"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[22,22,22,22]" data-paddingright="[41,41,41,41]"
                        data-paddingbottom="[22,22,22,22]" data-paddingleft="[104,104,104,104]"
                        style="z-index: 27; white-space: nowrap; font-size: 15px; line-height: 24px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: ;font-family:Montserrat;text-transform:uppercase;background-color:rgba(255,255,255,0);border-color:rgba(255,255,255,0.14);border-style:solid;border-width:2px 2px 2px 2px;border-radius:40px 40px 40px 40px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Buy
                        it now </a>
                    <div class="tp-caption tp-resizeme" id="slide-17-layer-24" data-x="['left','left','left','left']"
                        data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                        data-type="image" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]" style="z-index: 28;"><img
                            src="{{URL::to('public/img/demo_creative_agency_full/images/4aad8-11.png')}}" alt=""
                            data-ww="" data-hh="" data-no-retina> </div>
                    <div class="tp-caption tp-resizeme" id="slide-17-layer-25" data-x="['left','left','left','left']"
                        data-hoffset="['39','39','39','39']" data-y="['middle','middle','middle','middle']"
                        data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                        data-type="text" data-responsive_offset="on"
                        data-frames='[{"delay":"+0","speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textalign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 29; white-space: nowrap; font-size: 26px; line-height: 26px; font-weight: 700; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">
                        17 </div>
                </div>
                <div class="tp-caption tp-resizeme" id="slide-17-layer-1" data-x="['left','left','left','left']"
                    data-hoffset="['-62','-62','-62','-62']" data-y="['top','top','top','top']"
                    data-voffset="['73','73','73','73']" data-width="none" data-height="none" data-whitespace="nowrap"
                    data-visibility="['on','off','off','off']" data-type="image" data-basealign="slide"
                    data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"x:left;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 5;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/0bb69-brk_slide_element-3.png')}}"
                        alt="" data-ww="" data-hh="" data-no-retina> </div>
                <div class="tp-caption tp-resizeme" id="slide-17-layer-3" data-x="['left','left','left','left']"
                    data-hoffset="['-60','-60','-60','-60']" data-y="['top','top','top','top']"
                    data-voffset="['788','788','788','788']" data-width="none" data-height="none"
                    data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="image"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 6;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/835ab-brk_slide_element-4.png')}}"
                        alt="" data-ww="" data-hh="" data-no-retina> </div>
                <div class="tp-caption tp-resizeme" id="slide-17-layer-5" data-x="['right','right','right','right']"
                    data-hoffset="['-322','-322','-322','-322']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['0','0','0','0']" data-width="none" data-height="none" data-whitespace="nowrap"
                    data-visibility="['on','off','off','off']" data-type="image" data-basealign="slide"
                    data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 7;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/bd38d-brk_slide_thumb-6.png')}}"
                        alt="" data-ww="['368px','368px','368px','368px']" data-hh="['182px','182px','182px','182px']"
                        data-no-retina> </div>
                <div class="tp-caption tp-resizeme" id="slide-17-layer-6" data-x="['right','right','right','right']"
                    data-hoffset="['-322','-322','-322','-322']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['188','188','188','188']" data-width="none" data-height="none"
                    data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="image"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"x:left;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 8;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/c0ff5-brk_slide_thumb-5.png')}}"
                        alt="" data-ww="['368px','368px','368px','368px']" data-hh="['260px','260px','260px','260px']"
                        data-no-retina> </div>
                <div class="tp-caption tp-resizeme" id="slide-17-layer-7" data-x="['right','right','right','right']"
                    data-hoffset="['-322','-322','-322','-322']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['449','449','449','449']" data-width="none" data-height="none"
                    data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="image"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"x:left;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 9;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/d1b07-brk_slide_thumb-4.png')}}"
                        alt="" data-ww="['368px','368px','368px','368px']" data-hh="['264px','264px','264px','264px']"
                        data-no-retina> </div>
                <div class="tp-caption tp-resizeme" id="slide-17-layer-8" data-x="['right','right','right','right']"
                    data-hoffset="['43','43','43','43']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['34','34','34','34']" data-width="none" data-height="none" data-whitespace="nowrap"
                    data-visibility="['on','off','off','off']" data-type="image" data-basealign="slide"
                    data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"x:left;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 10;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/4d6c0-brk_slide_thumb-3.png')}}"
                        alt="" data-ww="['368px','368px','368px','368px']" data-hh="['264px','264px','264px','264px']"
                        data-no-retina> </div>
                <div class="tp-caption tp-resizeme" id="slide-17-layer-9" data-x="['right','right','right','right']"
                    data-hoffset="['43','43','43','43']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['300','300','300','300']" data-width="none" data-height="none"
                    data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="image"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"x:left;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 11;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/db3cf-brk_slide_thumb-2.png')}}"
                        alt="" data-ww="['368px','368px','368px','368px']" data-hh="['264px','264px','264px','264px']"
                        data-no-retina> </div>
                <div class="tp-caption tp-resizeme" id="slide-17-layer-10" data-x="['right','right','right','right']"
                    data-hoffset="['43','44','44','44']" data-y="['bottom','bottom','bottom','bottom']"
                    data-voffset="['564','565','565','565']" data-width="none" data-height="none"
                    data-whitespace="nowrap" data-visibility="['on','off','off','off']" data-type="image"
                    data-basealign="slide" data-responsive_offset="on"
                    data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"x:left;","ease":"Power3.easeInOut"}]'
                    data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]"
                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"
                    style="z-index: 12;"><img
                        src="{{URL::to('public/img/demo_creative_agency_full/images/451e1-brk_slide_thumb-1.png')}}"
                        alt="" data-ww="['368px','368px','368px','368px']" data-hh="['264px','264px','264px','264px']"
                        data-no-retina> </div>
            </li>
        </ul>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
</div>
<section class="container position-relative brk-z-index-10 mt-100 mb-lg-0">
    <div class="text-center mb-40 mb-lg-70">
        <h4 class="font__family-montserrat font__weight-light font__size-28 line__height-30 mb-10 brk-base-font-color">
            Our
        </h4>
        <h3 class="font__family-montserrat font__size-42 font__weight-bold line__height-46">
            Group Companies
        </h3>
    </div>
    <div class="icon__wrapper-main icon__wrapper-linear active__effect-main text-center"
        data-brk-library="component__sequence">
        <div class="row no-gutters">
            <div class="col-lg-4">
                <div class="icon__wrapper active__effect pt-30 pb-40 pl-30 pr-30">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60">
                        <g>
                            <defs>
                                <clipPath id="livicon_magic_fl">
                                    <rect x="6" y="-6" width="60" height="60" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#livicon_magic_fl)">
                                <g>
                                    <path fill="none" stroke="#F39C12" stroke-width="2" stroke-linecap="square"
                                        stroke-miterlimit="10"
                                        d="M20.15,34.19L29.34,25c0,0,1.41,0,3.54,2.12S35,30.66,35,30.66l-9.19,9.19" />
                                    <path fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                        stroke-miterlimit="10"
                                        d="M-1.06,66.72l-5.66-5.66l26.87-26.87c0,0,1.41,0,3.54,2.12s2.12,3.54,2.12,3.54L-1.06,66.72z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <defs>
                                <clipPath id="livicon_magic_s">
                                    <rect x="6" y="-6" width="60" height="60" />
                                </clipPath>
                            </defs>
                            <g clip-path="url(#livicon_magic_s)">
                                <g>
                                    <path fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                        stroke-miterlimit="10"
                                        d="M20.15,34.19L29.34,25c0,0,1.41,0,3.54,2.12S35,30.66,35,30.66l-9.19,9.19" />
                                    <path fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                        stroke-miterlimit="10"
                                        d="M-1.06,66.72l-5.66-5.66l26.87-26.87c0,0,1.41,0,3.54,2.12s2.12,3.54,2.12,3.54L-1.06,66.72z" />
                                </g>
                            </g>
                        </g>
                        <g>
                            <path fill="none" stroke="#F9BF3B" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10"
                                d="M16,22c0,0-0.4-1.2-1.6-2.4C13.2,18.4,12,18,12,18s1.2-0.4,2.4-1.6C15.6,15.2,16,14,16,14s0.4,1.2,1.6,2.4C18.8,17.6,20,18,20,18s-1.2,0.4-2.4,1.6C16.4,20.8,16,22,16,22z" />
                            <path fill="none" stroke="#F9BF3B" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10"
                                d="M30,9c0,0-0.1-0.3-0.4-0.6C29.3,8.1,29,8,29,8s0.3-0.1,0.6-0.4C29.9,7.3,30,7,30,7s0.1,0.3,0.4,0.6C30.7,7.9,31,8,31,8s-0.3,0.1-0.6,0.4C30.1,8.7,30,9,30,9z" />
                            <path fill="none" stroke="#F9BF3B" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10"
                                d="M44,22c0,0-0.6-1.8-2.4-3.6C39.8,16.6,38,16,38,16s1.8-0.6,3.6-2.4C43.4,11.8,44,10,44,10s0.6,1.8,2.4,3.6C48.2,15.4,50,16,50,16s-1.8,0.6-3.6,2.4C44.6,20.2,44,22,44,22z" />
                            <path fill="none" stroke="#F9BF3B" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10"
                                d="M52,31c0,0-0.1-0.3-0.4-0.6C51.3,30.1,51,30,51,30s0.3-0.1,0.6-0.4C51.9,29.3,52,29,52,29s0.1,0.3,0.4,0.6C52.7,29.9,53,30,53,30s-0.3,0.1-0.6,0.4C52.1,30.7,52,31,52,31z" />
                            <path fill="none" stroke="#F9BF3B" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10"
                                d="M42,48c0,0-0.4-1.2-1.6-2.4C39.2,44.4,38,44,38,44s1.2-0.4,2.4-1.6C41.6,41.2,42,40,42,40s0.4,1.2,1.6,2.4C44.8,43.6,46,44,46,44s-1.2,0.4-2.4,1.6C42.4,46.8,42,48,42,48z" />
                        </g>
                    </svg>
                    <h4 class="font__family-montserrat font__size-28 font__weight-bold">
                        Mayur Distributors
                    </h4>
                    <p class="font__family-open-sans font__size-14 line__height-21 pl-10 pr-10">
                        <ul>
                            <li> &bullet; Tata Chemicals Ltd</li>
                            <li> &bullet; Nikon India Pvt. Ltd</li>
                        </ul>
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="icon__wrapper active__effect active current pt-30 pb-40 pl-30 pr-30">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60">
                        <path fill="none" stroke="#9CAAB9" stroke-width="2" stroke-linecap="square"
                            stroke-miterlimit="10"
                            d="M42,31h1.43l0.35,2.94l2.65,1.1L49,33l2,2l-2.04,2.56l1.1,2.65L53,40.57v2.87l-2.94,0.35l-1.1,2.65L51,49l-2,2l-2.56-2.04l-2.65,1.1L43.43,53h-2.87l-0.35-2.94l-2.65-1.1L35,51l-2-2l2.04-2.56l-1.1-2.65L31,43.43v-2.87l2.94-0.35l1.1-2.65L33,35l2-2l2.56,2.04l2.65-1.1L40.57,31H42zM42,39c-1.66,0-3,1.34-3,3c0,1.66,1.34,3,3,3c1.66,0,3-1.34,3-3C45,40.34,43.66,39,42,39z" />
                        <path fill="none" stroke="#95A5A6" stroke-width="2" stroke-linecap="square"
                            stroke-miterlimit="10"
                            d="M23,7h1.04l1.01,4.31l2.02,0.54l3.03-3.23l1.81,1.04l-1.28,4.23l1.48,1.48l4.23-1.28l1.05,1.81l-3.22,3.03l0.54,2.02L39,21.96l0,2.08l-4.31,1.01l-0.54,2.02l3.22,3.03l-1.04,1.81l-4.23-1.28l-1.48,1.48l1.28,4.23l-1.81,1.05l-3.03-3.23l-2.02,0.54L24.04,39l-2.09,0l-1.01-4.31l-2.02-0.54l-3.02,3.23l-1.81-1.04l1.28-4.23l-1.48-1.48L9.66,31.9L8.62,30.1l3.23-3.03l-0.54-2.02L7,24.04l0-2.09l4.31-1.01l0.54-2.02L8.62,15.9l1.04-1.81l4.24,1.28l1.48-1.48L14.1,9.66l1.81-1.04l3.03,3.23l2.02-0.54L21.96,7H23zM23,17c-3.31,0-6,2.68-6,6c0,3.31,2.68,6,6,6c3.31,0,6-2.69,6-6C29,19.68,26.31,17,23,17z" />
                    </svg>
                    <h4 class="font__family-montserrat font__size-28 font__weight-bold">
                        Mayur Telecom Pvt. Ltd
                    </h4>
                    <p class="font__family-open-sans font__size-14 line__height-21 pl-10 pr-10">
                        <ul>
                            <li> &bullet; Micromax Mobiles</li>
                            <li> &bullet; IFFALCON By TCL</li>
                        </ul>
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="icon__wrapper active__effect pt-30 pb-40 pl-30 pr-30">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60">
                        <g>
                            <path fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10" d="M52.97,28.91C52.99,29.27,53,29.63,53,30" />
                            <path fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10" d="M51.55,21.96c0.26,0.7,0.48,1.4,0.67,2.11" />
                            <path fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10" d="M46.84,14.33c0.78,0.83,1.49,1.73,2.14,2.67" />
                            <path fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10"
                                d="M49.8,42.19C45.75,48.75,38.27,53,30,53C17.3,53,7,42.7,7,30C7,17.3,17.3,7,30,7c4.17,0,8.22,1.11,11.78,3.24" />
                            <polyline fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10" points="43,41 51,41 51,49" />
                        </g>
                        <g>
                            <line fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10" x1="30" y1="31" x2="41" y2="24" />
                            <line fill="none" stroke="#34495E" stroke-width="2" stroke-linecap="square"
                                stroke-miterlimit="10" x1="30" y1="31" x2="18" y2="19" />
                        </g>
                        <g>
                            <path fill="none" stroke="#34495E" stroke-width="3" stroke-linecap="square"
                                stroke-miterlimit="10" d="M52.97,28.91C52.99,29.27,53,29.63,53,30" />
                            <path fill="none" stroke="#34495E" stroke-width="3" stroke-linecap="square"
                                stroke-miterlimit="10" d="M51.55,21.96c0.26,0.7,0.48,1.4,0.67,2.11" />
                            <path fill="none" stroke="#34495E" stroke-width="3" stroke-linecap="square"
                                stroke-miterlimit="10" d="M46.84,14.33c0.78,0.83,1.49,1.73,2.14,2.67" />
                            <path fill="none" stroke="#34495E" stroke-width="3" stroke-linecap="square"
                                stroke-miterlimit="10"
                                d="M49.8,42.19C45.75,48.75,38.27,53,30,53C17.3,53,7,42.7,7,30C7,17.3,17.3,7,30,7c4.17,0,8.22,1.11,11.78,3.24" />
                            <polyline fill="none" stroke="#34495E" stroke-width="3" stroke-linecap="square"
                                stroke-miterlimit="10" points="43,41 51,41 51,49" />
                        </g>
                        <g>
                            <line fill="none" stroke="#34495E" stroke-width="3" stroke-linecap="square"
                                stroke-miterlimit="10" x1="30" y1="31" x2="41" y2="24" />
                            <line fill="none" stroke="#34495E" stroke-width="3" stroke-linecap="square"
                                stroke-miterlimit="10" x1="30" y1="31" x2="18" y2="19" />
                        </g>
                    </svg>
                    <h4 class="font__family-montserrat font__size-28 font__weight-bold">
                        Harsh Communication
                    </h4>
                    <p class="font__family-open-sans font__size-14 line__height-21 pl-10 pr-10">
                        <ul>
                            <li>&bullet; Vivo Mobiles</li>
                        </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="brk-left-overflow-image">
    <div class="container-fluid">
        <div class="row justify-content-center no-gutters">
            <div class="col-xl-5 position-relative">
                <div
                    class="brk-z-index-10 position-static position-xl-absolute image-map-creative_agency pt-xl-20 pt-60 pb-40">
                    <div class="brk-image-map"
                        data-brk-image-map='{"id":2025,"editor":{"selected_shape":"rect-4738","tool":"poly","zoom":8},"general":{"name":"brk-image-map-app-2","width":1596,"height":1414,"naturalWidth":1596,"naturalHeight":1414,"center_image_map":1},"image":{"url":"{{URL::to('public/img/brk-image-map-5-1.png')}}"},"tooltips":{"sticky_tooltips":1,"tooltip_animation":"grow"},"spots":[{"id":"rect-7736","title":"rect-7736","type":"poly","x":16.739,"y":21.379,"width":30.079,"height":39.398,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":50.854913726967865,"y":0},{"x":100,"y":24.412725242680587},{"x":49.48970932402295,"y":100},{"x":0,"y":75.44037717096171}]},{"id":"rect-8358","title":"rect-7737","type":"poly","x":29.931,"y":48.03,"width":30.028,"height":39.224,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":50.77144498446338,"y":0},{"x":100,"y":24.5210215457661},{"x":49.573763379422076,"y":100},{"x":0,"y":75.62716652512617}]},{"id":"rect-4170","title":"rect-7738","type":"poly","x":41.633,"y":1.412,"width":15.668,"height":15.628,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"position":"bottom","auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":0.8727733978674437,"y":1.8471852921554122},{"x":2.401026175872763,"y":0.5235779021890471},{"x":4.577427483787329,"y":0},{"x":6.879035357757206,"y":0.9054155224118445},{"x":47.04293478826334,"y":22.929945523923497},{"x":51.27273176699539,"y":14.373435460124231},{"x":52.786368909662244,"y":10.00564944269796},{"x":57.104932091200276,"y":11.55851687179889},{"x":58.65414546579031,"y":9.339107694537141},{"x":59.89314790111951,"y":8.35067721367934},{"x":61.223470003704236,"y":7.899360268958051},{"x":61.59718644745049,"y":9.171389738041652},{"x":61.503760632436475,"y":11.364583997242297},{"x":60.977021384084274,"y":13.922029652735779},{"x":64.84778394007596,"y":17.074174467080944},{"x":61.50658958132718,"y":22.256329400392755},{"x":57.98354527875368,"y":29.56779399196945},{"x":98.17446176695822,"y":54.81416305725566},{"x":99.60393359563562,"y":56.714504904485686},{"x":100,"y":58.7137278709419},{"x":99.64368047747357,"y":60.98648504629253},{"x":98.46091005360553,"y":62.74011974953305},{"x":96.6487811447235,"y":63.698044516993704},{"x":94.3968359454279,"y":64.06968103002534},{"x":51.19915970311102,"y":55.067425727397264},{"x":48.91660782337288,"y":55.1851509849248},{"x":46.85191411247151,"y":55.68492078193681},{"x":45.128530626728555,"y":56.78554420636671},{"x":43.45012943951688,"y":58.29073758391073},{"x":28.104523217124434,"y":83.57394132853959},{"x":37.091101224536715,"y":92.60677567918977},{"x":37.87049062644151,"y":95.18299512477051},{"x":37.17439132828505,"y":97.33157706456628},{"x":36.16652865182013,"y":98.9896464691364},{"x":34.215348553109855,"y":100},{"x":24.91262358740608,"y":98.7721280597119},{"x":23.46167335960269,"y":97.01518372813835},{"x":23.44660747373296,"y":92.04003651458855},{"x":19.957170825443715,"y":96.57616641149382},{"x":21.39650479060814,"y":90.70606310096363},{"x":17.71643930974979,"y":94.06411459550989},{"x":16.10377283050708,"y":94.10625287357564},{"x":9.021319050196038,"y":85.27938954545786},{"x":8.809614513379783,"y":83.31831945758113},{"x":9.274803910927986,"y":81.11063871610618},{"x":10.788851218419172,"y":79.30556305376157},{"x":12.997306650121418,"y":78.56793076115589},{"x":24.58640148339415,"y":81.42094683076147},{"x":34.729856037485106,"y":52.67147365900274},{"x":34.807949908133146,"y":50.17220346409908},{"x":34.433408430272955,"y":47.96412444666384},{"x":33.714836175210685,"y":45.526704203669475},{"x":32.19265418080161,"y":43.57181870364323},{"x":1.618807728774492,"y":9.125589666299266},{"x":0.3327562682962219,"y":6.573572548585202},{"x":0,"y":4.0887887955441435}]},{"id":"rect-9828","title":"rect-7739","type":"poly","x":75.975,"y":13.876,"width":3.332,"height":3.695,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":37.77601112599038,"y":0},{"x":57.747715722776064,"y":5.523864637841851},{"x":77.4215892594326,"y":21.983708739730602},{"x":92.29517835535972,"y":46.891266195256215},{"x":100,"y":68.26777114635053},{"x":93.96293096666246,"y":86.24751827520284},{"x":83.03244528007603,"y":95.06067226357831},{"x":69.65919031776149,"y":100},{"x":49.287998340712896,"y":96.92390969939592},{"x":30.214314401776804,"y":87.19165155395378},{"x":15.814638675100928,"y":76.88870775874656},{"x":4.489264600880868,"y":61.371109908871844},{"x":0,"y":40.782490356485354},{"x":5.4075605680955245,"y":22.76180451465419},{"x":17.20832216342487,"y":7.095444923508822}]},{"id":"rect-3347","title":"rect-7740","type":"poly","x":61.625,"y":11.516,"width":15.512,"height":15.486,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":51.58490524303526},{"x":76.19161793461757,"y":100},{"x":0,"y":54.62267367369675},{"x":26.78609436752502,"y":0}]},{"id":"rect-4254","title":"rect-7741","type":"poly","x":58.75,"y":22.002,"width":6.067,"height":6.361,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":39.51350163228871},{"x":81.57270461078787,"y":85.06411076539611},{"x":69.61653313455282,"y":99.35887786516797},{"x":61.544253800396746,"y":100},{"x":0,"y":64.21718275019485},{"x":34.647011954203755,"y":0}]},{"id":"rect-6525","title":"rect-7742","type":"poly","x":55.85,"y":27.941,"width":5.631,"height":6.465,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":35.28820597556567},{"x":91.43073393931469,"y":51.15074022603464},{"x":73.98401482687004,"y":75.40257532433377},{"x":62.025554229144106,"y":100},{"x":53.09743323188763,"y":89.64916189663256},{"x":0,"y":61.17132089405871},{"x":34.82567311148246,"y":0}]},{"id":"rect-4274","title":"rect-7743","type":"poly","x":64.769,"y":25.856,"width":7.402,"height":7.056,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":52.245430591990086},{"x":77.84164523630022,"y":100},{"x":0,"y":51.93977707849927},{"x":24.06197693493862,"y":0}]},{"id":"rect-9553","title":"rect-7744","type":"poly","x":61.24,"y":31.78,"width":8.519,"height":7.983,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":52.34169434623895},{"x":78.3288771778307,"y":100},{"x":0,"y":47.17370357047527},{"x":20.30847924915763,"y":0},{"x":59.908984563082,"y":23.171719802664363}]},{"id":"rect-4738","title":"rect-7745","type":"poly","x":56.223,"y":35.372,"width":8.201,"height":6.227,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":87.30148344554574,"y":52.457262815904535},{"x":92.00476897939785,"y":59.30405893069962},{"x":95.89826676940906,"y":70.06443495755896},{"x":99.61529102538347,"y":76.38245917398501},{"x":100,"y":82.88639475919075},{"x":97.7101996094477,"y":90.38377673122939},{"x":94.14565353177272,"y":96.29548149032652},{"x":89.22072535776786,"y":100},{"x":82.31127293005822,"y":98.8942592848406},{"x":5.428204234998655,"y":36.51471925075552},{"x":1.1889637198103906,"y":27.73758167889286},{"x":0,"y":19.30207918422468},{"x":0.10256621287384803,"y":10.821123520783274},{"x":2.419806589866757,"y":4.780705869721582},{"x":6.854837794348533,"y":0},{"x":16.449238966005748,"y":2.9937710084001945},{"x":51.12789764237152,"y":24.134130922402043}]}]}'
                        data-brk-library="component__image_map"></div>
                </div>
            </div>
            <div class="col-xl-7">
                <div class="d-flex flex-column pb-md-200 pb-sm-140 pb-20 pl-xl-30 image-map-desc-creative_agency">
                    <div class="text-center text-xl-left">
                        <h2 class="font__family-montserrat font__weight-bold font__size-56 line__height-60 mt-10"
                            data-brk-library="component__title">
                            <span class="highlight-massive font__weight-bold">
                                About Us <span class="after wow zoomIn"></span>
                            </span>
                        </h2>
                    </div>
                    <h4 class="mt-5">Mayur Distributors is been in the Distribution sector since 1975.
                    </h4>
                    <p class="brk-dark-font-color font__size-16 line__height-26 mt-40 pr-xl-15 text-center text-lg-left">
                        We Have Handled The Top Most Brands In Various Categories Consumer Goods, Electronic Products And In Telecom Services  
                    </p>
                    
                    <p class="brk-dark-font-color font__size-16 line__height-26 mt-40 pr-xl-15 text-center text-lg-left">
                        Our Current Distribution Brands  Comprise Of Vivo Mobiles, NIKON Camera, Tata Chemicals, Micromax Mobiles & Iffalcon Televisions By TCL
                    </p>
                    <p class="brk-dark-font-color font__size-16 line__height-26 mt-40 pr-xl-15 text-center text-lg-left">
                        In Past Successful Associations With Nokia/Microsoft, Reliance Jio, Britannia, Cadbury, Novartis India, Canon, Panasonic, Airtel, Sony Ericson, O2, Motorola, HTC, Apple, Idea Cellular And Many More.
                    </p>
                    <p class="brk-dark-font-color font__size-16 line__height-26 mt-40 pr-xl-15 text-center text-lg-left">
                        Serviced approximately 5000 Retailers and Wholesalers in entire Maharashtra.
                    </p>
                    <div class="text-center text-lg-left mt-5">
                        <a href="{{url('/about-us')}}" class="btn btn-inside-out btn-lg border-radius-25 btn-shadow ml-0 pl-50 pr-50 brk-library-rendered rendered" data-brk-library="component__button" tabindex="-1">
                            <span class="before">Know More</span><span class="text" style="min-width: 90.4375px;">Go to About Us</span><span class="after">Know More</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

 <section class="pt-80 pb-80 pb-lg-0">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="mb-40 mt-30">
                    <h3 class="font__family-montserrat font__weight-bold font__size-38 line__height-60 mt-10 text-center text-lg-left" data-brk-library="component__title">Our Core Services <span class="after wow zoomIn"></span>
                         
                    </h3>
                    <div class="overflow-hid position-static position-lg-relative">
                        <svg width="1920" height="1" version="1.1" xmlns="http://www.w3.org/2000/svg" class="mb-45">
                            <line stroke-dasharray="5, 5" x1="0" y1="1" x2="1920" y2="1" stroke="var(--brand-primary)" stroke-width="1"></line>
                        </svg>
                        <h2 class="font__family-montserrat font__size-24 line__height-32 text-uppercase mb-35 text-center text-lg-left"><span class="font__weight-light">Make business</span> <span class="font__weight-bold">with us</span></h2>
                        <p class="brk-dark-font-color font__size-16 font__weight-normal line__height-26 mb-20 text-center text-lg-left">
                            Our core service is distributorship. We are directly carrying the manufactured units to the market. We are renowned distributors across Maharashtra. Addressing and connecting the large group of dealers, we are handling our distribution eco-system smoothly throughout all the stages until it reaches the market right. 
                        </p>
                        <div class="text-center text-lg-left mt-5">
                            <a href="{{url('/about-us')}}" class="btn btn-inside-out btn-lg border-radius-25 btn-shadow ml-0 pl-50 pr-50 brk-library-rendered rendered" data-brk-library="component__button" tabindex="-1">
                                <span class="before">Know More</span><span class="text" style="min-width: 90.4375px;">Go to About Us</span><span class="after">Know More</span>
                            </a>
                        </div>
                       
                    </div>
                </div>
            
            </div>
            <div class="col-12 col-lg-6">
                <div class="frame-image img-double-bigger" data-brk-library="component__image_frames">
                    <div class="img">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x540_1.jpg')}}" alt="alt" class="lazyload">
                    </div>
                    <div class="img">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/540x370_1.jpg')}}" alt="alt" class="lazyload">
                    </div>
                </div>
            </div>
        </div>
   
    </div>
</section>

<section class="pt-lg-120 pt-0 pb-50 brk-trapezoid-left bg-white" id="">
    <div class="container">
        <div class="row align-items-end justify-content-center" data-brk-library="component__image_frames">
            <div class="col-lg-6 text-left mt-140 mt-lg-40 mb-50">
                <div class="extra__heading-3" data-brk-library="component__title_section">
                    <div class="heading">
                        <h2 class="font__family-montserrat font__weight-bold font__size-30 text-uppercase"> <span class="font__weight-ultralight">HOW</span>  MAYUR Distributors
                            <br>
                            <span class="font__weight-ultralight">STAND</span> Unique
                        </h2>
                    </div>
                    <p class="font__family-open-sans font__size-16">Mayur distributors have their roots in the industry since 1975. We at Mayur Distributors started off in the niche of distribution with the intent to have the latest and trendy Global technology, soonest at the local level.</p>
                    <a href="{{url('/')}}" class="font__family-montserrat font__weight-medium font__size-13 text-uppercase letter-spacing-100 text-blue link-icon mt-20">Learn more
                        <i class="icon fa fa-long-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 flex-lg-last">
                <div class="text-center text-lg-left">
                    <ul class="list-inline-4 mb-35" data-brk-library="component__list">
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span><a href="#">We follow our ethics as our protocol.</a><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Our relation management is good enough to hold the clients for long. <span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>We manage all the supplies to be quicker and smoother. <span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Our team is an expert in all the distribution services that we extend.<span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Our experience and expertise made us grow reliable.<span class="after"></span>
                        </li>
                    </ul>
                   
                </div>
            </div>
        </div>
    </div>
</section>




<section class="pt-80 pb-20">
    <div class="text-center mb-40 mb-lg-70">
        <h4 class="font__family-montserrat font__weight-light font__size-28 line__height-30 mb-10 brk-base-font-color">
            Statisctics
        </h4>
        <h3 class="font__family-montserrat font__size-42 font__weight-bold line__height-46">
            Experience at a Glance
        </h3>
    </div>
    <div class="container">
       
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="text-center text-lg-left">
                    <div class="text-center text-lg-left">
                        <h5 class="font__family-montserrat font__weight-light text-uppercase font__size-18 text-blue" data-brk-library="component__title">Previously Managed Businesses</h5>
                        <hr class="divider wow zoomIn ml-md-0" data-brk-library="component__title">
                    </div>
                    <ul class="list-inline-4 mb-35" data-brk-library="component__list">
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span><a href="#">Microsoft/Nokia distributorship for around 7 years.</i></a><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Britannia distributorship for around 12 years.</i><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Cadbury’s  distributorship for around 5 years.</i><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Distributorship of Novartis India limited for 5 years.</i><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Motorola / HTC</i><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Canon India Pvt. Ltd./Panasonic India Pvt. Ltd
                            </i><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Idea Cellular Ltd distributorship For 16 Years
                            </i><span class="after"></span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="text-center text-lg-left">
                    <div class="text-center text-lg-left">
                        <h5 class="font__family-montserrat font__weight-light text-uppercase font__size-18 text-blue" data-brk-library="component__title">Currently Managed Businesses</h5>
                        <hr class="divider wow zoomIn ml-md-0" data-brk-library="component__title">
                    </div>
                    <ul class="list-inline-4 mb-35" data-brk-library="component__list">
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span><a href="#">Nikon India Pvt. Ltd.</i></a><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Vivo Mobiles </i><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Micromax Mobiles </i><span class="after"></span>
                        </li>
                        <li class="font__family-montserrat font__size-16">
                            <span class="before"></span>Tata Chemicals </i><span class="after"></span>
                        </li>
                        
                        <li class="font__family-montserrat font__size-16">
                           <span class="before"></span> <a target="_blank" href="https://www.iffalcon.com/">IFFALCON Televisions by TCL</a>  </i><span class="after"></span>
                        </li>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</section>


<section>
    <div class="container mt-50">
        <div class="text-center mb-lg-20">
            <h4 class="font__family-montserrat font__weight-light font__size-28 line__height-30 mb-10 brk-base-font-color">
                Brands
            </h4>
            <h3 class="font__family-montserrat font__size-42 font__weight-bold line__height-46">
                Brands We Worked With
            </h3>
        </div>
    </div>
    <div class="container mb-50">
        <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 6, "slidesToScroll": 1, "arrows": false, "responsive": [
{"breakpoint": 1200, "settings": {"slidesToShow": 5}},
{"breakpoint": 992, "settings": {"slidesToShow": 4}},
{"breakpoint": 768, "settings": {"slidesToShow": 3}},
{"breakpoint": 576, "settings": {"slidesToShow": 2}},
{"breakpoint": 375, "settings": {"slidesToShow": 1}}
], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/iffalcon.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/nikon.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/tata.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/micromax.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/vivo.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/motorola.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/britannia.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/cadbury.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/canon.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/idea.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
            <div>
                <div class="shop-window" data-brk-library="component__content_slider">
                    <a href="#">
                        <div class="shop-window__thumb" style="background-image: url({{URL::to('public/img/brands/microsoft-lumia.jpg')}}); background-size:100%;"></div>
                        
                    </a>
                    
                </div>
            </div>
           
        </div>
    </div>
</section>




<section class="pb-2">
    <div class="container mb-80 pt-0">
        <div class="text-left mb-40 mb-lg-70">
            <h4 class="font__family-montserrat font__weight-light font__size-28 line__height-30 mb-10 brk-base-font-color text-center text-lg-left">
                Testimonials
            </h4>
            <h3 class="font__family-montserrat font__size-42 font__weight-bold line__height-46 text-center text-lg-left">
                What Our Clients Say
            </h3>
        </div>
        <div class="brk-testimonials-dash-two" data-brk-library="component__testimonials,slider__swiper">
            <div class="dash-two-pagination"></div>
            <div class="swiper-container dash-two-slider">
                <div class="swiper-wrapper">
                   
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/1.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            I have been associated with Harsh Communication, Vivo since 2015. I have received enormous support from Harsh Communication in growing my business. Their sales staff has been very friendly and has adjusted with us well in terms of timely stock support and other services. 
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Zubeir Shaikh,</p>
                        <p class="font__family-open-sans text-center text-lg-left">New Cell 4 You<br> Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/2.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            Its been over 4 years we are associated with Harsh Communication. Never we had any loose or bad experience regarding any services. With their professional and holistic approach, they have been very supportive and helpful.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">New Sai Mobile</p>
                        <p class="font__family-open-sans text-center text-lg-left">Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/3.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            For last many years I am into mobile business and have dealt with many mobile brand distributors. In all those distributors, Harsh Communication is undoubtedly one of the best distributors in Mobile Industry. Their services and policies are extremely good. Keep up the good work.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Baljinder Singh</p>
                        <p class="font__family-open-sans text-center text-lg-left">Sona Mobile<br> Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/2.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            We are among the leading multi-mobile retailer in Pune City. We are dealing with many mobile brands distributors. I am associated with Mayur group for more than 15 years and It has been great experience in working with this group. We have received great support from this team and from Sharad Shah in particular. He is indeed an extra ordinary person.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Manik Mobile</p>
                        <p class="font__family-open-sans text-center text-lg-left">Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/testimonials/1.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            We are associated with Mayur Distributors for last 15 Years. They have supported us in all respects. We are thankful to entire team to Mayur Distributors.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Goa Beverages</p>
                        <p class="font__family-open-sans text-center text-lg-left">Sachin Pai<br> North Goa</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/people/testimonials/2.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            Mayur Distributors has very co-operative and systematic working style. It’s a pleasure to work with Mayur Distributors.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">Awdita Entreprises</p>
                        <p class="font__family-open-sans text-center text-lg-left"> Pune</p>
                   
                        </div>  
                    </div>
                    <div class="swiper-slide">
                        <div class="brk-testimonials-dash-two__text-reviews brk-base-box-shadow" data-img="{{URL::to('public/img/people/testimonials/5.jpg')}}">
                      
                            <div class="font__size-28 pull-left" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                            </div>
                            <div class="font__size-28 pull-right" style="color:#cfcfcf!important;">
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </div>
                        
                        
                        <div class="brk-testimonials-dash-two__description font__family-open-sans mt-30 pt-30" style="font-size: 18px !important;">
                            Very supportive Firm & all Firm staff. Proud to be associated with & looking forward to work with them.
                        </div>
                        <p class="font__family-open-sans font__weight-bold text-center text-lg-left" style="font-size: 20px !important; margin-top:40px !important;">M/s. Lakade</p>
                        <p class="font__family-open-sans text-center text-lg-left">Mungurwadi and Co.<br> Kolhapur</p>
                   
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
