@extends('layouts.layout')

@section('title')
Procurement | Supply Chain | A distribution partner of consumer products
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="Procurement | Supply Chain | A distribution partner of consumer products">
<meta name="description" content="Mayur Distributors services are distributorship. Serviced approximately 5000 Retailers and Wholesalers in the entire Maharashtra.✓Get a Free Quote Today 020-26430632">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in india, consumer goods company, Electronic Products, telecom service distributors, distributors in pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors">
<link rel="canonical" href="{{url('/about-us')}}">
<meta property="og:title" content="Procurement | Supply Chain | A distribution partner of consumer products">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/about-us">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Mayur Distributors">
<meta property="og:description"content="Mayur Distributors services are distributorship. Serviced approximately 5000 Retailers and Wholesalers in the entire Maharashtra.✓Get a Free Quote Today 020-26430632">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('headers')
<style>
    .bg-strengths{
        background: url("{{URL::to('public/img/bg-patterns/bg-strength.jpg')}}");
        background-size:100%;
        padding:40px 0px;
        background-attachment: fixed;
    }
</style>
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-48 line__height-68 font__family-montserrat">
                            About Us
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">About Us</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <main class="main-container">
        <section class="brk-left-overflow-image">
            <div class="container-fluid">
                <div class="row justify-content-center no-gutters">
                    <div class="col-xl-5 position-relative">
                        <div
                            class="brk-z-index-10 position-static position-xl-absolute image-map-creative_agency pt-xl-20 pt-60 pb-40">
                            <div class="brk-image-map"
                                data-brk-image-map='{"id":2025,"editor":{"selected_shape":"rect-4738","tool":"poly","zoom":8},"general":{"name":"brk-image-map-app-2","width":1596,"height":1414,"naturalWidth":1596,"naturalHeight":1414,"center_image_map":1},"image":{"url":"{{URL::to('public/img/brk-image-map-5-1.png')}}"},"tooltips":{"sticky_tooltips":1,"tooltip_animation":"grow"},"spots":[{"id":"rect-7736","title":"rect-7736","type":"poly","x":16.739,"y":21.379,"width":30.079,"height":39.398,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":50.854913726967865,"y":0},{"x":100,"y":24.412725242680587},{"x":49.48970932402295,"y":100},{"x":0,"y":75.44037717096171}]},{"id":"rect-8358","title":"rect-7737","type":"poly","x":29.931,"y":48.03,"width":30.028,"height":39.224,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":50.77144498446338,"y":0},{"x":100,"y":24.5210215457661},{"x":49.573763379422076,"y":100},{"x":0,"y":75.62716652512617}]},{"id":"rect-4170","title":"rect-7738","type":"poly","x":41.633,"y":1.412,"width":15.668,"height":15.628,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"position":"bottom","auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":0.8727733978674437,"y":1.8471852921554122},{"x":2.401026175872763,"y":0.5235779021890471},{"x":4.577427483787329,"y":0},{"x":6.879035357757206,"y":0.9054155224118445},{"x":47.04293478826334,"y":22.929945523923497},{"x":51.27273176699539,"y":14.373435460124231},{"x":52.786368909662244,"y":10.00564944269796},{"x":57.104932091200276,"y":11.55851687179889},{"x":58.65414546579031,"y":9.339107694537141},{"x":59.89314790111951,"y":8.35067721367934},{"x":61.223470003704236,"y":7.899360268958051},{"x":61.59718644745049,"y":9.171389738041652},{"x":61.503760632436475,"y":11.364583997242297},{"x":60.977021384084274,"y":13.922029652735779},{"x":64.84778394007596,"y":17.074174467080944},{"x":61.50658958132718,"y":22.256329400392755},{"x":57.98354527875368,"y":29.56779399196945},{"x":98.17446176695822,"y":54.81416305725566},{"x":99.60393359563562,"y":56.714504904485686},{"x":100,"y":58.7137278709419},{"x":99.64368047747357,"y":60.98648504629253},{"x":98.46091005360553,"y":62.74011974953305},{"x":96.6487811447235,"y":63.698044516993704},{"x":94.3968359454279,"y":64.06968103002534},{"x":51.19915970311102,"y":55.067425727397264},{"x":48.91660782337288,"y":55.1851509849248},{"x":46.85191411247151,"y":55.68492078193681},{"x":45.128530626728555,"y":56.78554420636671},{"x":43.45012943951688,"y":58.29073758391073},{"x":28.104523217124434,"y":83.57394132853959},{"x":37.091101224536715,"y":92.60677567918977},{"x":37.87049062644151,"y":95.18299512477051},{"x":37.17439132828505,"y":97.33157706456628},{"x":36.16652865182013,"y":98.9896464691364},{"x":34.215348553109855,"y":100},{"x":24.91262358740608,"y":98.7721280597119},{"x":23.46167335960269,"y":97.01518372813835},{"x":23.44660747373296,"y":92.04003651458855},{"x":19.957170825443715,"y":96.57616641149382},{"x":21.39650479060814,"y":90.70606310096363},{"x":17.71643930974979,"y":94.06411459550989},{"x":16.10377283050708,"y":94.10625287357564},{"x":9.021319050196038,"y":85.27938954545786},{"x":8.809614513379783,"y":83.31831945758113},{"x":9.274803910927986,"y":81.11063871610618},{"x":10.788851218419172,"y":79.30556305376157},{"x":12.997306650121418,"y":78.56793076115589},{"x":24.58640148339415,"y":81.42094683076147},{"x":34.729856037485106,"y":52.67147365900274},{"x":34.807949908133146,"y":50.17220346409908},{"x":34.433408430272955,"y":47.96412444666384},{"x":33.714836175210685,"y":45.526704203669475},{"x":32.19265418080161,"y":43.57181870364323},{"x":1.618807728774492,"y":9.125589666299266},{"x":0.3327562682962219,"y":6.573572548585202},{"x":0,"y":4.0887887955441435}]},{"id":"rect-9828","title":"rect-7739","type":"poly","x":75.975,"y":13.876,"width":3.332,"height":3.695,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":37.77601112599038,"y":0},{"x":57.747715722776064,"y":5.523864637841851},{"x":77.4215892594326,"y":21.983708739730602},{"x":92.29517835535972,"y":46.891266195256215},{"x":100,"y":68.26777114635053},{"x":93.96293096666246,"y":86.24751827520284},{"x":83.03244528007603,"y":95.06067226357831},{"x":69.65919031776149,"y":100},{"x":49.287998340712896,"y":96.92390969939592},{"x":30.214314401776804,"y":87.19165155395378},{"x":15.814638675100928,"y":76.88870775874656},{"x":4.489264600880868,"y":61.371109908871844},{"x":0,"y":40.782490356485354},{"x":5.4075605680955245,"y":22.76180451465419},{"x":17.20832216342487,"y":7.095444923508822}]},{"id":"rect-3347","title":"rect-7740","type":"poly","x":61.625,"y":11.516,"width":15.512,"height":15.486,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":51.58490524303526},{"x":76.19161793461757,"y":100},{"x":0,"y":54.62267367369675},{"x":26.78609436752502,"y":0}]},{"id":"rect-4254","title":"rect-7741","type":"poly","x":58.75,"y":22.002,"width":6.067,"height":6.361,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":39.51350163228871},{"x":81.57270461078787,"y":85.06411076539611},{"x":69.61653313455282,"y":99.35887786516797},{"x":61.544253800396746,"y":100},{"x":0,"y":64.21718275019485},{"x":34.647011954203755,"y":0}]},{"id":"rect-6525","title":"rect-7742","type":"poly","x":55.85,"y":27.941,"width":5.631,"height":6.465,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":35.28820597556567},{"x":91.43073393931469,"y":51.15074022603464},{"x":73.98401482687004,"y":75.40257532433377},{"x":62.025554229144106,"y":100},{"x":53.09743323188763,"y":89.64916189663256},{"x":0,"y":61.17132089405871},{"x":34.82567311148246,"y":0}]},{"id":"rect-4274","title":"rect-7743","type":"poly","x":64.769,"y":25.856,"width":7.402,"height":7.056,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":52.245430591990086},{"x":77.84164523630022,"y":100},{"x":0,"y":51.93977707849927},{"x":24.06197693493862,"y":0}]},{"id":"rect-9553","title":"rect-7744","type":"poly","x":61.24,"y":31.78,"width":8.519,"height":7.983,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":100,"y":52.34169434623895},{"x":78.3288771778307,"y":100},{"x":0,"y":47.17370357047527},{"x":20.30847924915763,"y":0},{"x":59.908984563082,"y":23.171719802664363}]},{"id":"rect-4738","title":"rect-7745","type":"poly","x":56.223,"y":35.372,"width":8.201,"height":6.227,"default_style":{"fill":"#2774ff","fill_opacity":0,"stroke_opacity":0},"mouseover_style":{"fill":"#2774ff","fill_opacity":0.5917293003627232,"stroke_opacity":0},"tooltip_style":{"border_radius":0,"background_color":"#ffffff","background_opacity":1,"auto_width":1},"tooltip_content":{"plain_text":"<div style=\"text-align: center\">\n\t<div style=\"font-weight: 700;color: #272727;font-size: 18px;line-height: 24px;padding-bottom: 10px;\">\n\t\tTooltip title\n\t</div>\n\t<div style=\"font-size: 14px;line-height: 20px;color: #9f9f9f;\">\n\t\tAenean vulputate eleifend tellus.<br>Aenean leo ligula, porttitor.\n\t</div>\n</div>","squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":87.30148344554574,"y":52.457262815904535},{"x":92.00476897939785,"y":59.30405893069962},{"x":95.89826676940906,"y":70.06443495755896},{"x":99.61529102538347,"y":76.38245917398501},{"x":100,"y":82.88639475919075},{"x":97.7101996094477,"y":90.38377673122939},{"x":94.14565353177272,"y":96.29548149032652},{"x":89.22072535776786,"y":100},{"x":82.31127293005822,"y":98.8942592848406},{"x":5.428204234998655,"y":36.51471925075552},{"x":1.1889637198103906,"y":27.73758167889286},{"x":0,"y":19.30207918422468},{"x":0.10256621287384803,"y":10.821123520783274},{"x":2.419806589866757,"y":4.780705869721582},{"x":6.854837794348533,"y":0},{"x":16.449238966005748,"y":2.9937710084001945},{"x":51.12789764237152,"y":24.134130922402043}]}]}'
                                data-brk-library="component__image_map"></div>
                        </div>
                    </div>
                    <div class="col-xl-7">
                        <div class="d-flex flex-column pb-md-200 pb-sm-140 pb-20 pl-xl-30 image-map-desc-creative_agency">
                            <div class="text-center text-xl-left">
                                <h2 class="font__family-montserrat font__weight-bold font__size-56 line__height-60 mt-10"
                                    data-brk-library="component__title">
                                    <span class="highlight-massive font__weight-bold">
                                        About Us <span class="after wow zoomIn"></span>
                                    </span>
                                </h2>
                            </div>
                            <h4 class="mt-5">Mayur Distributors is been in the Distribution sector since 1975.
                            </h4>
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-40 pr-xl-15 text-center text-lg-left">
                                Mayur distribution has its roots in the industry since 1975. We at Mayur Distribution started off in the niche of distribution with the intent to have the latest and trendy Global technology, soonest at the local level. 
                            </p>
                            
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-40 pr-xl-15 text-center text-lg-left">
                                In our initial stages, we started with FMCG for renowned brands namely - Britannia, Nestle, Cadbury’s. Working with such brands, our expertise and ethics in the distributorship business got stronger by each passing day. 

                            </p>
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-40 pr-xl-15 text-center text-lg-left">
                                It was early 2001 when we actually stepped into tech product distributorship. Back then, idea cellular SIM card was our distribution product which was quite in a demand. Thus started our journey of Mobiles and laptops distribution thereon. With our previous hands-on experience in distributorship, we earned good remarks from brands like HTC, Motorola, Gionee, Nokia and so. We worked as a Nokia mobile distributor for 9 years. 
                            </p>
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-40 pr-xl-15 text-center text-lg-left">
                                With so much of reputation and compliments, we are now associated as a distribution partner of Vivo Mobiles, Micromax, Panasonic & Canon camera. Since 2017, we also started with the Nikon Cameras distribution and Iffalcon by TCL in the Division of entire Maharashtra. 
                            </p>
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-40 pr-xl-15 text-center text-lg-left">
                            Apart from this, we are also distribution partners of Tata chemicals, for their products - Tata Salt, Pulses and other spices across Maharashtra & Goa.
                            </p>
                          
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="pt-50 pb-50 bg-white">
			<div class="text-center mb-50">
				<h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30 brk-library-rendered" data-brk-library="component__title">ABOUT THE DIRECTOR</h2>
				<div class="divider-cross wow zoomIn brk-library-rendered" data-brk-library="component__title" style="visibility: visible; animation-name: zoomIn;">
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="container">
				<div class="row no-gutters-center">
                    <div class="col-12 col-md-7">
                        <div class="align-items-center">
                            <p class="brk-dark-font-color text-justify font__size-16 font__weight-normal line__height-26 align-items-center pt-4">
                                An MBA In Marketing & Finance, Mr. Sharad Shah Carries A Rich Business Experience Of More Than 3 Decades In Distribution Realm.
                           <br>
                           <br>
                                A Results-oriented Director Driven To Manage Costs And Establish Strategic Mutually Beneficial Partnerships And Relationships With Users Vendors And Clients.
                           <br>
                           <br>
                                Skillful At Creating Strategic Alliances With Organization Leaders To Effectively Align With And Support Key Business Initiatives. Excel At Building And Retaining High Performance Teams By Motivating Skilled Professionals.
                           </p>
                           <div class="text-right">
                               <h4 class="mt-5">Mr. Sharad Shah</h4>
                               <h6>Director<br>Mayur Distributors</h6>
                           </div>
                        </div>
                    </div>
					<div class="col-12 col-md-5 text-center">

                        <div class="pl-15 pr-15">
                            <article class="brk-team-persone-circle text-center" data-brk-library="component__team">
                                
                                <div class="brk-team-persone-circle__bg lazyload" data-bg="{{URL::to('public/img/about_us/director.png')}}">
                                    <span class="brk-team-persone-circle__bg-overlay">
                                        <span class="before brk-base-bg-gradient-90deg"></span>
                                        <svg viewBox="0 0 270 37">
                                            <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                        </svg>
                                    </span>
                                    <ul class="brk-team-persone-circle__contacts">
                                    
                                        <li class="text-left">
                                            <a href="javascript:void(0);" style="font-size: 18px; font-weight:700;">Mr. Sharad Shah</a>
                                        </li>
                                        <li class="text-left">
                                            <a href="javascript:void(0);" style="font-size: 16px; font-weight:700;">Director</a>
                                        </li>
                                        <br>
                                        <li>
                                            <i class="fas fa-phone" aria-hidden="true"></i>
                                            <a href="tel:+919822014774">+91 9822014774</a>
                                        </li>
                                        <li>
                                            <i class="far fa-envelope" aria-hidden="true"></i>
                                            <a href="mailto:sharad@mayurdistributors.com" style="font-size:12px;">sharad@mayurdistributors.com</a>
                                        </li>
                                       
                                    </ul>
                                </div>
                                <div class="brk-team-persone-circle__social-links">
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </article>
                        </div>      
                    {{-- <img src="{{URL::to('public/img/about_us/director.png')}}" class="img-fluid" alt="Mayur Distributors Director"> --}}
					</div>
				
				</div>
			</div>
        </section>
        

        <div class="pt-90 text-center position-relative" data-brk-library="component__social_block,component__button">
            <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/bg-1920_1.jpg" alt="alt" class="brk-abs-img lazyload">
            <span class="brk-abs-overlay brk-light-gradient-90deg-94"></span>
            <div class="container all-light">
                <div class="text-center">
                    <h3 class="brk-white-font-color font__family-montserrat font__size-56 font__weight-bold lh-60">Our Socials</h3>
                </div>
                <div class="row no-gutters mt-70">
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://business.facebook.com/mayurdistributors/?business_id=2833294386752219"><i class="brk-icon slide-bg-wrap fab fa-facebook-f"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">Facebook</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://www.instagram.com/mayur_distributors/"><i class="brk-icon slide-bg-wrap fab fa-instagram"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">Instagram</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://twitter.com/MayurDistribut1"><i class="brk-icon slide-bg-wrap fab fa-twitter"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">Twitter</h4>
                        </div>
                    </div>
                    
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="#"><i class="brk-icon slide-bg-wrap fab fa-linkedin"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">LinkedIn</h4>
                        </div>
                    </div>
                    
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://www.google.co.in/search?q=Mayur+Distributors&ludocid=10682126935951604583&lsig=AB86z5VF4Sn_EXvgolzQhi3zgSSY#fpstate=lie"><i class="brk-icon slide-bg-wrap fab fa-google"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">Google</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://api.whatsapp.com/send?phone=917774076072"><i class="brk-icon slide-bg-wrap fab fa-whatsapp"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">WhatsApp</h4>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>  

        <section class="mb-50 mt-100" id="team" style="scroll-margin-top:100px !important;">
            <div class="container mb-50">
                <div class="text-center">
                    <h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30" data-brk-library="component__title">OUR TEAM</h2>
                    <div class="divider-cross wow zoomIn" data-brk-library="component__title">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="default-slider slick-loading arrows-classic-dark fa-req" data-slick='{"slidesToShow": 4, "slidesToScroll": 1,
"responsive": [
{"breakpoint": 992, "settings": {"slidesToShow": 3}},
{"breakpoint": 768, "settings": {"slidesToShow": 2}},
{"breakpoint": 480, "settings": {"slidesToShow": 1}}
], "autoplay": true, "autoplaySpeed": 2000}' data-brk-library="slider__slick">
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Mahesh Manchale</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Finance - Head</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                    <li>
                                        <a href="javascript:void(0);">Department - Finance</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Brand - All</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Firm - All</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mrs. Deepali Shinde</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Accounts Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                    <li>
                                        <a href="javascript:void(0);">Department - Finance</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Brand - All</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Firm - All</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Kunal Shah</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Director</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - IFFALCON</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Mayur Telecom Pvt. Ltd</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Manoj Patil</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - Sales</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Vivo</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Harsh Communication</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Chirag Shah</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Dev. Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - Sales</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Vivo</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Harsh Communication</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Shivaji Desai</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Dev. Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - Sales</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Vivo</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Harsh Communication</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Imran Shaikh</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">MIS Head</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - MIS</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - All</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Firm - All</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Shaheen Shingde</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Sales & Collection</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Tata Chemicals</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Mayur Distributors</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Vikas Konde</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Sales & Collection</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Nikon</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Mayur Distributors</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="pl-15 pr-15">
                        <article class="brk-team-persone-circle brk-base-box-shadow text-center" data-brk-library="component__team">
                            <div class="brk-team-persone-circle__name-position">
                                <a href="#">
                                    <h4 class="font__family-montserrat font__weight-bold font__size-18">Mr. Chetan Deshpande</h4>
                                </a>
                                <span class="font__family-montserrat font__weight-normal font__size-16 line__height-24">Business Manager</span>
                            </div>
                            <div class="brk-team-persone-circle__bg lazyload" data-bg="img/270x414_1.jpg">
                                <span class="brk-team-persone-circle__bg-overlay">
                                    <span class="before brk-base-bg-gradient-90deg"></span>
                                    <svg viewBox="0 0 270 37">
                                        <path d="M270,37H0V0A267.6,267.6,0,0,0,135.53,36.5,267.52,267.52,0,0,0,270,0Z" fill="rgb(255, 255, 255)" />
                                    </svg>
                                </span>
                                <ul class="brk-team-persone-circle__contacts text-center">
                                  
                                    <li>
                                        <a href="javascript:void(0);">Department - Sales</a>
                                    </li>
                                  
                                    <li>
                                        <a href="javascript:void(0);">Brand - Micromax</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Mayur Telecom Pvt Ltd</a>
                                    </li>
                                    <li>
                                        <a href="mailto:gmail@gmail.com">gmail@gmail.com</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="brk-team-persone-circle__social-links">
                                <ul>
                                    <li><a href="#"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </article>
                    </div>
                   
                </div>
            </div>
        </section>
    
        

        <section class="mb-5 mt-5">
            <div class="brk-tabs brk-tabs_tabbed-intro" data-index="2" data-brk-library="component__tabbed_contents">
                <ul class="brk-tabs-nav">
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/nikon.jpg')}}" alt="" style="width:120px;"> </li>
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/tata.jpg')}}" alt="" style="width:120px;"> </li>
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/vivo.jpg')}}" alt="" style="width:120px;"> </li>
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/micromax.jpg')}}" alt="" style="width:120px;"> </li>
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/iffalcon.jpg')}}" alt="" style="width:120px;"> </li>
                </ul>
                <div class="brk-tabs-content position-relative">
                    <div class="brk-abs-overlay" data-bg="{{URL::to('public/img/bg-patterns/3.png')}}">
                        <span class="brk-abs-overlay brk-bg-grad opacity-95"></span>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-xl-left text-center brk-white-font-color mb-20 mt-10">Mayur Distributors</h4>
                                    <p class="font__size-16 line__height-26 text-xl-left text-justify">Nikon is a Japanese multinational corporation headquartered in Tokyo, Japan, specializing in optics and imaging products.
                                        <br>Nikon is the name trusted by millions of photographers around the world. With our cutting-edge innovations and relentless pursuit of quality, Nikon products are one of the global leader in digital imaging and optics. The company is the eighth-largest chip equipment maker as reported in 2017.
                                        <br>Nikon's products include cameras, camera lenses, binoculars, microscopes, ophthalmic lenses, measurement instruments, rifle scopes, spotting scopes, and the steppers used in the photolithography steps of semiconductor fabrication, of which it is the world's second largest manufacturer.
                                        <br>Mayur Distributors is associated with Nikon since 2017 as authorized Super Distributor for South & Central Maharashtra.
                                        </p>
                                </div>
                                
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/nikon.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/nikon.jpg')}}" class="image-border-1 lazyload" alt="alt">
                                     
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-xl-left text-center brk-white-font-color mb-20 mt-10">Mayur Distributors</h4>
                                    <p class="font__size-16 line__height-26 text-xl-left text-justify">
                                        
                                        Tata Chemicals Limited is an Indian global company with interests in chemicals, crop nutrition and consumer products. The company is one of the largest chemical companies in India with significant operations in India and Africa. Tata Chemicals is a subsidiary of Tata Group conglomerate.
                                        <br> Tata Chemicals have the largest salt works in Asia, and are the 3rd largest soda ash manufacturer and the 6th largest sodium bicarbonate manufacturer in the world.
                                        <br> Tata Chemicals has huge range of products in various ( ) such as Spices, Chemicals, Fertilizers, Consumer Products, Biofuels, Cement, Pulses, Nutrition solutions, water purifiers etc.
                                        <br> Mayur Distributors is associated with Tata Chemicals since 2004 as Authorized Super Distributor for 70% area of Maharashtra state & entire Goa State. And takes care of products like Tata Salt, Tata Lite, I-shakti, Soda, Besan & all Pulses for above said area.

                                    </p>
                                </div>
                                
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/tata.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/tata.jpg')}}" class="image-border-1 lazyload" alt="alt">
                                     
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-lg-left text-center brk-white-font-color mb-20 mt-10">Harsh Communication</h4>
                                    <p class="font__size-16 line__height-26 text-lg-left text-center">Vivo Communication Technology Co. Ltd. is a Chinese technology company owned by BBK Electronics that designs and manufactures smartphones and smartphone accessories in China, software and online services.

                                        <br>
                                        Vivo products are one of the preferred brands of young people around the world.
                                        <br>Vivo launched its products in India in 2015 and since then Harsh Communication is associated with Vivo.
                                        </p>
                                </div>
                               
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/vivo.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/vivo.jpg')}}" class="image-border-1 lazyload" alt="alt">
                                     
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8 col-lg-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-lg-left text-center brk-white-font-color mb-20 mt-10">Mayur Telecom Pvt. Ltd.</h4>
                                  <p class="text-white">
                                    Micromax Informatics Limited is one of the leading consumer electronics company in India, and the 10th largest mobile phone player in the world. The company has many firsts to its credit when it comes to the mobile handset market; including the 30-day battery backup, Dual SIM Dual Standby phones, QWERTY keypads, universal remote control mobile phones & first quad-core budget smart phone. The brand's product portfolio embraces more than 60 models today, ranging from feature rich, dual-SIM phones, 3G Android smartphones, tablets, LED televisions and data cards.
                                    <br>Since 2018 Mayur Telecom Pvt. Ltd is an authorized Super Distributor of Micromax Mobile for Rest of Maharashtra.
                                    
                                  </p>
                                </div>
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/micromax.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/micromax.jpg')}}" class="image-border-1 lazyload" alt="alt">
                                     
                                    </a>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8 col-lg-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-lg-left text-center brk-white-font-color mb-20 mt-10">Mayur Telecom Pvt. Ltd.</h4>
                                  <p class="text-white">
                                    iFFalcon Technology is responsible for the operations of global smart TV platforms of TCL Multimedia and its subsidiary companies. It is also engaged in the design, production, manufacture and sales of smart TVs with iFFalcon or other new brands. 
                                    <br>Mayur Telecom Pvt Ltd is an authorized Super Distributor of Micromax Mobile for Rest of Maharashtra since 2019.

                                </div>
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/iffalcon.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/iffalcon.jpg')}}" class="image-border-1 lazyload" alt="alt">
                                     
                                    </a>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
      

        <section>
            <div class="container mb-80 mt-100">
                <div class="text-center">
                    <h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30" data-brk-library="component__title">Our Stregths</h2>
                    <div class="divider-cross wow zoomIn" data-brk-library="component__title">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="bg-strengths">

            <div class="container">
                <div class="row">
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">01.</h4>
                                <p class="text-white">Professional Management With The Involvement Of Three Family Members.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">02.</h4>
                                <p class="text-white">Strong Financial Background With Capacity To Invest Our Own Funds.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">03.</h4>
                                <p class="text-white">Excellent Personal Touch With All Major Retailers Of Pune.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">04.</h4>
                                <p class="text-white">Highly Trained Sales Team With Complete Knowledge Of Products And Market.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">05.</h4>
                                <p class="text-white">Efficient Backend Team Which Can  Manage  Daily Reporting, Claim Processing, Accounting & Analytical Works.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">06.</h4>
                                <p class="text-white">Excellent Long Lasting Relationship With Bankers Since Year 1956.</p>
                           
                            </div>
                        </article>
                    </div>
               
                    
                </div>
            </div>
        </div>

        </section>


        <section>
            <div class="container mb-100">
                <div class="text-center">
                    <h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30" data-brk-library="component__title">Vision & Mission</h2>
                    <div class="divider-cross wow zoomIn" data-brk-library="component__title">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="container mb-50">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="flip-box" data-brk-library="component__flip_box">
                            <div class="flip flip_horizontal flip-box__multiply">
                                <div class="flip__front">
                                    <div class="flip-box__multiply-title font__family-montserrat font__weight-bold font__size-58 text-uppercase mt-35 pl-xl-50 pr-xl-50 pl-20 pr-20 lazyload" data-bg="{{URL::to('public/img/about-us/vision.jpg')}}">Our Vision
                                    </div>
                                    <div class="flip-box__multiply-decoration lazyload border-radius-25 ml-xl-50 ml-20 mt-90 mb-40" data-bg="{{URL::to('public/img/about-us/vision.jpg')}}">
                                        <i class="fal fa-angle-right font__size-28 font__weight-thin" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="flip__back flip-box__bg flip-box__bg_overlay lazyload" data-bg="{{URL::to('public/img/about-us/vision.jpg')}}">
                                    <div class="flip-box__position flip-box__position_35 text-center">
                                        <h4 class="flip-box__multiply-h4 font__family-montserrat font__weight-bold font__size-58">01</h4>
                                        <p class="font__family-open-sans font__size-16 line__height-31 text-white mt-40 pl-30 pr-30">&bullet; To have a Global and Transparent relationship from businesses and brands to the local vendors and resellers.
                                            <br>&bullet; To have an Eco-system in the forte of Distribution of the latest technologies and products.
                                            </p>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="flip-box" data-brk-library="component__flip_box">
                            <div class="flip flip_horizontal flip-box__multiply">
                                <div class="flip__front">
                                    <div class="flip-box__multiply-title font__family-montserrat font__weight-bold font__size-58 text-uppercase mt-35 pl-xl-50 pr-xl-50 pl-20 pr-20 lazyload" data-bg="{{URL::to('public/img/about-us/mission.jpg')}}">Our Mission
                                    </div>
                                    <div class="flip-box__multiply-decoration lazyload border-radius-25 ml-xl-50 ml-20 mt-90 mb-40" data-bg="{{URL::to('public/img/about-us/mission.jpg')}}">
                                        <i class="fal fa-angle-right font__size-28 font__weight-thin" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="flip__back flip-box__bg flip-box__bg_overlay lazyload" data-bg="{{URL::to('public/img/about-us/mission.jpg')}}">
                                    <div class="flip-box__position flip-box__position_35 text-center">
                                        <h4 class="flip-box__multiply-h4 font__family-montserrat font__weight-bold font__size-58">02</h4>
                                        <p class="font__family-open-sans font__size-16 line__height-31 text-white mt-40 pl-30 pr-30">To Establish a Fine Distribution Platform by Implementing Breakthrough Technologies, Process Improvisation & People Development.</p>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="flip-box" data-brk-library="component__flip_box">
                            <div class="flip flip_horizontal flip-box__multiply">
                                <div class="flip__front">
                                    <div class="flip-box__multiply-title font__family-montserrat font__weight-bold font__size-58 text-uppercase mt-35 pl-xl-50 pr-xl-50 pl-20 pr-20 lazyload" data-bg="{{URL::to('public/img/about-us/values.jpg')}}">Our Values
                                    </div>
                                    <div class="flip-box__multiply-decoration lazyload border-radius-25 ml-xl-50 ml-20 mt-90 mb-40" data-bg="{{URL::to('public/img/about-us/values.jpg')}}">
                                        <i class="fal fa-angle-right font__size-28 font__weight-thin" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="flip__back flip-box__bg flip-box__bg_overlay lazyload" data-bg="{{URL::to('public/img/about-us/values.jpg')}}">
                                    <div class="flip-box__position flip-box__position_35 text-center">
                                        <h4 class="flip-box__multiply-h4 font__family-montserrat font__weight-bold font__size-58">03</h4>
                                        <p class="font__family-open-sans font__size-16 line__height-31 text-white mt-40 pl-30 pr-30">
                                            We encourage <b>learning</b> at every step,<br> 
                                            We remain <b>passionate</b> and <b>disciplined</b> through our process, <br>
                                            Our distribution process is <br>innovating</b> and <b>excelling</b> at every stage,<br>
                                            <b>Customers’ ease</b> is what we put above all.
                                        </p>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </section>

       
    </main>
</div>
@endsection