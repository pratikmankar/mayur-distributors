@extends('layouts.layout')

@section('title')
Mayur Distributors | A Distributors company of consumer goods
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="Mayur Distributors | A Distributors company of consumer goods">
<meta name="description" content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors, distributors in Pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors">
<link rel="canonical" href="{{url('/gallery')}}">
<meta property="og:title" content="Mayur Distributors | A Distributors company of consumer goods">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/gallery">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="A Distributors company of consumer goods">
<meta property="og:description"content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-48 line__height-68 font__family-montserrat">
                            Gallery
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Gallery</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <main class="main-container">
        <div class="container mt-50 mb-50">
            <ul class="brk-filters brk-filters_style-3 justify-content-center d-flex flex-wrap mb-70" data-brk-library="component__gallery,component__shop_grid_filter">
                <li class="brk-filters__item active" data-filter="*">
                    <i class="fal fa-th"></i>
                    <span>All</span>
                </li>
                <li class="brk-filters__item" data-filter=".Bags">
                    <span>Team</span>
                </li>
                <li class="brk-filters__item" data-filter=".Shoes">
                    <span>Events</span>
                </li>
                <li class="brk-filters__item" data-filter=".Dresses">
                    <span>Interior</span>
                </li>
                <li class="brk-filters__item" data-filter=".Shirts">
                    <span>Exterior</span>
                </li>
            </ul>
            <div class="brk-gallery brk-grid" data-grid-cols="3" data-grid-cols-tablet="3" data-brk-library="component__portfolio_isotope">
                <div class="brk-grid__sizer"></div>
                <div class="brk-grid__item bags">
                    <div class="brk-gallery-card brk-gallery-card_grayscale brk-gallery-card_overflow" data-brk-library="component__gallery">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/gallary/gallery-1.png')}}" alt="alt" class="brk-gallery-card__img lazyload">
                        <span class="brk-gallery-card__add-1"></span>
                        <span class="brk-gallery-card__add-2"></span>
                        <span class="brk-gallery-card__add-3"></span>
                        <span class="brk-gallery-card__add-4"></span>
                        <span class="brk-gallery-card__add-5"></span>
                        <span class="brk-gallery-card__add-6"></span>
                        <span class="brk-gallery-card__overlay brk-bg-gradient-40deg-60"></span>
                        <a href="img/gallary/gallery-1.png" data-fancybox="gallery" class="brk-gallery-card__fancy fancybox brk-white-font-color d-flex align-items-center justify-content-center">
                            <i class="fas fa-plus font__size-36"></i>
                        </a>
                    </div>
                </div>
                <div class="brk-grid__item bags">
                    <div class="brk-gallery-card brk-gallery-card_grayscale brk-gallery-card_overflow" data-brk-library="component__gallery">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/gallary/gallery-2.PNG')}}" alt="alt" class="brk-gallery-card__img lazyload">
                        <span class="brk-gallery-card__add-1"></span>
                        <span class="brk-gallery-card__add-2"></span>
                        <span class="brk-gallery-card__add-3"></span>
                        <span class="brk-gallery-card__add-4"></span>
                        <span class="brk-gallery-card__add-5"></span>
                        <span class="brk-gallery-card__add-6"></span>
                        <span class="brk-gallery-card__overlay brk-bg-gradient-40deg-60"></span>
                        <a href="img/gallary/gallery-2.PNG" data-fancybox="gallery" class="brk-gallery-card__fancy fancybox brk-white-font-color d-flex align-items-center justify-content-center">
                            <i class="fas fa-plus font__size-36"></i>
                        </a>
                    </div>
                </div>
                <div class="brk-grid__item shoes">
                    <div class="brk-gallery-card brk-gallery-card_grayscale brk-gallery-card_overflow" data-brk-library="component__gallery">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/gallary/gallery-3.png')}}" alt="alt" class="brk-gallery-card__img lazyload">
                        <span class="brk-gallery-card__add-1"></span>
                        <span class="brk-gallery-card__add-2"></span>
                        <span class="brk-gallery-card__add-3"></span>
                        <span class="brk-gallery-card__add-4"></span>
                        <span class="brk-gallery-card__add-5"></span>
                        <span class="brk-gallery-card__add-6"></span>
                        <span class="brk-gallery-card__overlay brk-bg-gradient-40deg-60"></span>
                        <a href="img/gallary/gallery-3.png" data-fancybox="gallery" class="brk-gallery-card__fancy fancybox brk-white-font-color d-flex align-items-center justify-content-center">
                            <i class="fas fa-plus font__size-36"></i>
                        </a>
                    </div>
                </div>
                <div class="brk-grid__item shoes">
                    <div class="brk-gallery-card brk-gallery-card_grayscale brk-gallery-card_overflow" data-brk-library="component__gallery">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/gallary/gallery-4.png')}}" alt="alt" class="brk-gallery-card__img lazyload">
                        <span class="brk-gallery-card__add-1"></span>
                        <span class="brk-gallery-card__add-2"></span>
                        <span class="brk-gallery-card__add-3"></span>
                        <span class="brk-gallery-card__add-4"></span>
                        <span class="brk-gallery-card__add-5"></span>
                        <span class="brk-gallery-card__add-6"></span>
                        <span class="brk-gallery-card__overlay brk-bg-gradient-40deg-60"></span>
                        <a href="img/gallary/gallery-4.png" data-fancybox="gallery" class="brk-gallery-card__fancy fancybox brk-white-font-color d-flex align-items-center justify-content-center">
                            <i class="fas fa-plus font__size-36"></i>
                        </a>
                    </div>
                </div>
                <div class="brk-grid__item dresses">
                    <div class="brk-gallery-card brk-gallery-card_grayscale brk-gallery-card_overflow" data-brk-library="component__gallery">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/gallary/gallery-5.png')}}" alt="alt" class="brk-gallery-card__img lazyload">
                        <span class="brk-gallery-card__add-1"></span>
                        <span class="brk-gallery-card__add-2"></span>
                        <span class="brk-gallery-card__add-3"></span>
                        <span class="brk-gallery-card__add-4"></span>
                        <span class="brk-gallery-card__add-5"></span>
                        <span class="brk-gallery-card__add-6"></span>
                        <span class="brk-gallery-card__overlay brk-bg-gradient-40deg-60"></span>
                        <a href="img/gallary/gallery-5.png" data-fancybox="gallery" class="brk-gallery-card__fancy fancybox brk-white-font-color d-flex align-items-center justify-content-center">
                            <i class="fas fa-plus font__size-36"></i>
                        </a>
                    </div>
                </div>
                <div class="brk-grid__item dresses">
                    <div class="brk-gallery-card brk-gallery-card_grayscale brk-gallery-card_overflow" data-brk-library="component__gallery">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/gallary/gallery-6.png')}}" alt="alt" class="brk-gallery-card__img lazyload">
                        <span class="brk-gallery-card__add-1"></span>
                        <span class="brk-gallery-card__add-2"></span>
                        <span class="brk-gallery-card__add-3"></span>
                        <span class="brk-gallery-card__add-4"></span>
                        <span class="brk-gallery-card__add-5"></span>
                        <span class="brk-gallery-card__add-6"></span>
                        <span class="brk-gallery-card__overlay brk-bg-gradient-40deg-60"></span>
                        <a href="img/gallary/gallery-6.png" data-fancybox="gallery" class="brk-gallery-card__fancy fancybox brk-white-font-color d-flex align-items-center justify-content-center">
                            <i class="fas fa-plus font__size-36"></i>
                        </a>
                    </div>
                </div>
                <div class="brk-grid__item shirts">
                    <div class="brk-gallery-card brk-gallery-card_grayscale brk-gallery-card_overflow" data-brk-library="component__gallery">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/gallary/gallery-7.png')}}" alt="alt" class="brk-gallery-card__img lazyload">
                        <span class="brk-gallery-card__add-1"></span>
                        <span class="brk-gallery-card__add-2"></span>
                        <span class="brk-gallery-card__add-3"></span>
                        <span class="brk-gallery-card__add-4"></span>
                        <span class="brk-gallery-card__add-5"></span>
                        <span class="brk-gallery-card__add-6"></span>
                        <span class="brk-gallery-card__overlay brk-bg-gradient-40deg-60"></span>
                        <a href="img/gallary/gallery-7.png" data-fancybox="gallery" class="brk-gallery-card__fancy fancybox brk-white-font-color d-flex align-items-center justify-content-center">
                            <i class="fas fa-plus font__size-36"></i>
                        </a>
                    </div>
                </div>
                <div class="brk-grid__item shirts">
                    <div class="brk-gallery-card brk-gallery-card_grayscale brk-gallery-card_overflow" data-brk-library="component__gallery">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/gallary/gallery-8.png')}}" alt="alt" class="brk-gallery-card__img lazyload">
                        <span class="brk-gallery-card__add-1"></span>
                        <span class="brk-gallery-card__add-2"></span>
                        <span class="brk-gallery-card__add-3"></span>
                        <span class="brk-gallery-card__add-4"></span>
                        <span class="brk-gallery-card__add-5"></span>
                        <span class="brk-gallery-card__add-6"></span>
                        <span class="brk-gallery-card__overlay brk-bg-gradient-40deg-60"></span>
                        <a href="img/gallary/gallery-8.png" data-fancybox="gallery" class="brk-gallery-card__fancy fancybox brk-white-font-color d-flex align-items-center justify-content-center">
                            <i class="fas fa-plus font__size-36"></i>
                        </a>
                    </div>
                </div>
                <div class="brk-grid__item shirts">
                    <div class="brk-gallery-card brk-gallery-card_grayscale brk-gallery-card_overflow" data-brk-library="component__gallery">
                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/gallary/gallery-9.png')}}" alt="alt" class="brk-gallery-card__img lazyload">
                        <span class="brk-gallery-card__add-1"></span>
                        <span class="brk-gallery-card__add-2"></span>
                        <span class="brk-gallery-card__add-3"></span>
                        <span class="brk-gallery-card__add-4"></span>
                        <span class="brk-gallery-card__add-5"></span>
                        <span class="brk-gallery-card__add-6"></span>
                        <span class="brk-gallery-card__overlay brk-bg-gradient-40deg-60"></span>
                        <a href="img/gallary/gallery-9.png" data-fancybox="gallery" class="brk-gallery-card__fancy fancybox brk-white-font-color d-flex align-items-center justify-content-center">
                            <i class="fas fa-plus font__size-36"></i>
                        </a>
                    </div>
                </div>
            </div>
            {{-- <div class="col-12 text-center mt-80 mb-80">
                <a href="#" class="icon__btn icon__btn-anim icon__btn-md icon__btn-invert" data-brk-library="component__button">
                    <span class="before"></span>
                    <i class="fal fa-sync" aria-hidden="true"></i>
                    <span class="after"></span>
                    <span class="bg"></span>
                </a>
            </div> --}}
        </div>
    </main>
</div>
@endsection