@extends('layouts.layout')

@section('title')
Mayur Distributors | A Distributors company of consumer goods 
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="Mayur Distributors | A Distributors company of consumer goods ">
<meta name="description" content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in India, consumer goods company, Electronic Products, telecom service distributors, distributors in Pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors">
<link rel="canonical" href="{{url('/')}}">
<meta property="og:title" content="Mayur Distributors | A Distributors company of consumer goods ">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Mayur Distributors">
<meta property="og:description"content="Mayur Distributors is a Consumer Goods Electronic Products and in Telecom Service Distributors in Pune Vivo Mobiles, Tata Chemicals, iFFalcon Tv by TCL. ✓Get a Free Quote Today 020-26430632">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('content')

<div class="main-wrapper">
    <main class="main-container">
        <section class="brk-backgrounds brk-base-bg-gradient-2 full-screen d-flex flex-column align-items-center justify-content-center" data-brk-library="component__backgrounds_css,assets_fss">
            <div id="fss" class="brk-backgrounds__canvas"></div>
            <div class="brk-backgrounds__content">
                <div class="container">
                    <div class="maxw-770 pt-80 pb-80">
                        <h2 class="font__family-montserrat font__weight-bold text-white brk-error-page-title">404</h2>
                        <hr class="brk-dashed-border-transparent mt-25 mb-50">
                        <h3 class="font__family-montserrat font__size-48 line__height-52 text-white text-center mb-10">
                            <span class="font__weight-bold">Oops! </span>
                            <span class="font__weight-light">That page can’t be found.</span>
                        </h3>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
@endsection