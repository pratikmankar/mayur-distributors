@extends('layouts.layout')
@section('title')
Mayur Distributors | IFFALCON
@endsection
@section('headers')
<style>
.product-name:hover{
color:#fff;
transition:.1s;
}
</style>
@endsection
@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-28 line__height-68 font__family-montserrat">
                        Micromax Bharat 2 Plus (Black, 8 GB)  (1 GB RAM)
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                        
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Micromax Bharat 2 Plus (Black, 8 GB)  (1 GB RAM)</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="main-wrapper">
    <main class="main-container pt-30">
        <div class="container">
            <div class="brk-sc-shop-item mb-30" data-brk-library="component__shop_item_page_sidebar">
                <div class="row">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <div class="brk-sc-shop-item__main">
                            <div class="row pb-20 justify-content-md-start justify-content-center">
                                 <div class="col-lg-6 col-md-12 mb-4">
                                    
                                    <div class="slider-thumbnailed slick-loading fa-req">
                                        <div class="slider-thumbnailed-for arrows-modern" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "fade": true, "arrows": true, "autoplay": false, "autoplaySpeed": 4200}' data-brk-library="slider__slick" style="min-height: auto">
                                            <div>
                                                <img src="{{URL::to('public/img/products/micromax/micromax-bharat-2.jpg')}}" alt="alt">
                                            </div>
                                            <div>
                                                <img src="{{URL::to('public/img/products/micromax/micromax-bharat-21.jpg')}}" alt="alt">
                                            </div>
                                          
                                        </div>
                                        <div class="slider-thumbnailed-nav" data-slick='{"slidesToShow": 2, "slidesToScroll": 1}'>
                                           
                                        </div>
                                    </div>
                                    
                            </div>
                                <div class="col-lg-6 col-md-12 mt-4">
                                    <div class="brk-sc-shop-item__info">
                                        <div class="brk-sc-category d-flex align-items-baseline mb-15" data-brk-library="component__elements">
                                            <span class="brk-sc-category__title font__family-montserrat font__size-14 font__weight-bold mr-10 text-uppercase">
                                                Category:
                                            </span>
                                            <div class="brk-sc-category__items d-flex align-items-center flex-wrap">
                                                <a href="javascript:void();" class="brk-dark-font-color font__family-open-sans font__size-14 mr-1">Smartphones</a>
                                            </div>
                                        </div>
                                        <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-light line__height-32">Micromax Bharat 2 Plus
                                        <br>
                                        <span class="font__weight-bold">(Black, 8 GB)  (1 GB RAM)</span>
                                        </h2>
                                        <div class="brk-sc-divider mt-10 mb-20"></div>
                                        <div class="row no-gutters">
                                            <div class="col-lg-12">
                                                <h3 class="brk-color-filter__title font__family-montserrat font__weight-bold font__size-16 text-uppercase text-left">
                                                <span>Highlights</span>
                                                </h3>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                <span>Display</span>
                                                </h3>
                                                <p>10.16 cm (4 inch) Display</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                <span>Memory</span>
                                                </h3>
                                                <p>1 GB RAM | 8 GB ROM</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                <span>Camera</span>
                                                </h3>
                                                <p>5MP Rear Camera</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                <span>OS</span>
                                                </h3>
                                                <p>Android Nougat 7.0</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                <span>Processor</span>
                                                </h3>
                                                <p> Octa core</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                <span>Battery</span>
                                                </h3>
                                                <p>1600 mAh Battery </p>
                                            </div>
                                            <div class="col-lg-12">
                                                <hr class="underline-white">
                                                <div class="text-center text-lg-left mt-5">
                                                    <a href="{{url('/contact-us')}}" class="btn btn-inside-out btn-lg border-radius-25 btn-shadow ml-0 pl-50 pr-50 brk-library-rendered rendered" data-brk-library="component__button" tabindex="-1">
                                                        <span class="before">Enquire Now!</span><span class="text" style="min-width: 90.4375px;">Enquire Now!</span><span class="after">Enquire Now!</span>
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="brk-tabs brk-tabs_canted" data-brk-library="component__shop_item_page_sidebar">
                                <ul class="brk-tabs-nav font__family-montserrat font__size-14 font__weight-semibold line__height-14 text-uppercase">
                                    <li class="brk-tab">
                                        <span>Description</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Specifications</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Similar Products</span>
                                    </li>
                                </ul>
                                <div class="brk-tabs-content">
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Powered by the SC9832 processor and 1 GB of RAM, the Micromax Bharat 2 Plus comes with a 10.16-cm (4) WVGA display and a 1600 mAh Li-ion battery. You can also click pictures and selfies with the 5 MP rear camera and the 2 MP front camera.
                                                </p>
                                                
                                            </div>
                                            <!-- <div class="col-md-12 col-lg-7">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                HD Screen</h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Featuring a 15.44-cm HD display that has a resolution of 720x1560 pixels, this phone helps you watch videos or view photos with great clarity.
                                                </p>
                                            </div> -->
                                            <!-- <div class="col-md-12 col-lg-5 text-right">
                                                <img src="{{('public/img/products/micromax/hdscreen.jpg')}}" class="img-fluid" width="200px">
                                            </div>
                                            
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
                                            <div class="col-md-12 col-lg-5">
                                                <img src="{{('public/img/products/micromax/quality.jpg')}}" class="img-fluid mb-4" width="400px">
                                            </div>
                                            <div class="col-md-12 col-lg-7 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Excellent Picture Quality</h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    While its 13+2 MP rear camera helps you capture beautiful moments on the go, the 5 MP front camera lets you take amazing selfies. To top it off, its multi-shot feature helps you choose the best shot and the night mode helps you click great pictures even in low-light situations.
                                                </p>
                                            </div> -->
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-lg-12">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Take calls, click beautiful pictures and have uninterrupted entertainment by using this Micromax iOne phone. While its 15.44-cm display provides you with sufficient space to read news or watch videos, its 3950 mAh battery lets you listen to music or play games for hours on end.
                                                </p>
                                                
                                            </div>
                                            <div class="col-md-12 col-lg-7">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                Long-lasting Battery</h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Thanks to its 3950 mAh battery, this phone offers long hours of entertainment with up to 8 hours of browsing time, 18 hours of talk time and 500 hours of standby time.
                                                </p>
                                            </div>
                                            <div class="col-md-12 col-lg-5 text-right">
                                                <img src="{{('public/img/products/micromax/longlosting.jpg')}}" class="img-fluid" width="200px">
                                            </div>
                                            
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
                                            <div class="col-md-12 col-lg-5">
                                                <img src="{{('public/img/products/micromax/memory.jpg')}}" class="img-fluid mb-4" width="300px">
                                            </div>
                                            <div class="col-md-12 col-lg-7 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Expandable Memory</h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    With 64 GB of expandable memory, this phone helps you store movies, photos, music and other files in it conveniently.
                                                    With 3 GB of RAM, 32 GB of ROM and the Android Pie 9.0 operating system, this phone ensures smooth and seamless performance.
                                                </p>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-16 font__weight-bold line__height-32">General</h2>
                                                <hr class="underline-white">
                                            </div>
                                            
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Model Name</h2>
                                                <p>
                                                Bharat 2 Plus</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Model Number</h2>
                                                <p>
                                                Q402+</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Display Size</h2>
                                                <p>15.44 cm (6.08 inch)</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Screen Type</h2>
                                                <p>HD</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Resolution</h2>
                                                <p>480 x 800Pixels</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Camera Features</h2>
                                                <p>5MP Rear |Rear Flash</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Smartphone</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">SIM Type</h2>
                                                <p>Dual Sim</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Touchscreen</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Color</h2>
                                                <p>Black</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">OTG Compatible</h2>
                                                <p>No</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Network Type</h2>
                                                <p>3G, 4G VOLTE, 2G</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Launch Year</h2>
                                                <p>9 April 2019</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-6 col-lg-10">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">In The Box</h2>
                                                <p>
                                                Earphone, Charger, Handset, User manual, Battery</p>
                                                <hr class="underline-white">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "arrows": false, "responsive": [
                                                        {"breakpoint": 1200, "settings": {"slidesToShow": 4}},
                                                        {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                                                        {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                                                        {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                                                        {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                                                        ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/micromax/micromax-ione-note.jpg')}}" class="img2" alt="">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                        <a class="product-name" href="{{url('/micromax-onenote')}}"> One Note</a>
                                                                    </p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/micromax/micromax-bharat-4-diwali.jpg')}}" class="img2" alt="mayur distributors">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                        <a class="product-name" href="{{url('/micromax-Q4002N')}}">
                                                                        Q4002N Bharat 4</a>
                                                                    </p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/micromax/micromax-bharat-2.jpg')}}" class="img2" alt="Tata Chemicals distributors">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                        <a class="product-name" href="{{url('/Q402bharat-2')}}"> Micromax Bharat 2</a>
                                                                    </p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/micromax/micromax-bharat-5.jpg')}}" class="img2" alt="iFFalcon Televisions by TCL">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                        <a class="product-name" href="{{url('/micromax-Qbharat-5')}}"> Micromax Bharat 5</a>
                                                                    </p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/micromax/micromax-bharat-4-diwali.jpg')}}" class="img2" alt="Vivo Mobiles">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                        <a class="product-name" href="{{url('/micromax-Q204bharat-4')}}"> Micromax Bharat 4 </a>
                                                                    </p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/micromax/micromax-infinity-n11.jpg')}}" class="img2" alt="Micromax Mobiles">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                        <a class="product-name" href="{{url('/micromax-N11')}}"> Micromax Infinity N11</a>
                                                                    </p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/micromax/micromax-infinity-n12.jpg')}}" class="img2" alt="distribution partner">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                        <a class="product-name" href="{{url('/micromax-N12')}}">Micromax Infinity N12</a>
                                                                    </p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/micromax/micromax-x378-x378.jpg')}}" class="img2" alt="Nikon India">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                        <a class="product-name" href="{{url('/micromax-X378')}}"> Micromax-X378</a>
                                                                    </p>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            
                                            
                                            </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </main>
</div>
@endsection