@extends('layouts.layout')

@section('title')
Mayur Distributors | VIVO
@endsection


@section('headers')
    <style>
        .product-name:hover{
            color:#fff;
            transition:.1s;
        }
    </style>
@endsection

@section('content')

<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-28 line__height-68 font__family-montserrat">
                            Vivo Y91i (Fusion Black, 32 GB)  (2 GB RAM)



                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">Vivo Y91i (Fusion Black, 32 GB)  (2 GB RAM)


</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="main-wrapper">
    <main class="main-container pt-30">
        <div class="container">
            <div class="brk-sc-shop-item mb-30" data-brk-library="component__shop_item_page_sidebar">
                <div class="row">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <div class="brk-sc-shop-item__main">
                            <div class="row pb-20 justify-content-md-start justify-content-center">
                                <div class="col-lg-6 col-md-12 mb-4">
                                    
                                    <div class="slider-thumbnailed slick-loading fa-req">
                                        <div class="slider-thumbnailed-for arrows-modern" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "fade": true, "arrows": true, "autoplay": false, "autoplaySpeed": 4200}' data-brk-library="slider__slick" style="min-height: auto">
                                            <div>
                                                <img src="{{('public/img/products/vivo/vivo-y91i.jpeg')}}" alt="alt">
                                            </div>
                                            <div>
                                                <img src="{{('public/img/products/vivo/vivo-y91i-back.jpeg')}}" alt="alt">
                                            </div>
                                          
                                        </div>
                                        <div class="slider-thumbnailed-nav" data-slick='{"slidesToShow": 2, "slidesToScroll": 1}'>
                                           
                                        </div>
                                    </div>
                                    
                            </div>
                                <div class="col-lg-6 col-md-12 mt-4">
                                    <div class="brk-sc-shop-item__info">
                                      
                                        <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-light line__height-32">
                                         
                                            <span class="font__weight-bold">Vivo Y91i (Fusion Black, 32 GB)  (2 GB RAM)


</span>
                                        </h2>
                                        <div class="brk-sc-divider mt-10 mb-20"></div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h3 class="brk-color-filter__title font__family-montserrat font__weight-bold font__size-16 text-uppercase text-left">
                                                    <span>Highlights</span>
                                                </h3>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>2 GB RAM | 32 GB ROM | Expandable Upto 256 GB
                                                    </span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>15.8 cm (6.22 inch) HD+ Display
                                                    </span>
                                                </h3>
                                             
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>13MP Rear Camera | 5MP Front Camera
                                                    </span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>4030 mAh Li-ion Battery
                                                    </span>
                                                </h3>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase font-weight-bold text-left">
                                                    <span>MTK Helio P22 Processor
                                                    </span>
                                                </h3>
                                            </div>
                                          
                                            <div class="col-lg-12">
                                                <hr class="underline-white">
                                                <div class="text-center text-lg-left mt-5">
                                                    <a href="{{url('/contact-us')}}" class="btn btn-inside-out btn-lg border-radius-25 btn-shadow ml-0 pl-50 pr-50 brk-library-rendered rendered" data-brk-library="component__button" tabindex="-1">
                                                        <span class="before">Enquire Now!</span><span class="text" style="min-width: 90.4375px;">Enquire Now!</span><span class="after">Enquire Now!</span>
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="brk-tabs brk-tabs_canted" data-brk-library="component__shop_item_page_sidebar">
                                <ul class="brk-tabs-nav font__family-montserrat font__size-14 font__weight-semibold line__height-14 text-uppercase">
                                    <li class="brk-tab">
                                        <span>Description</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Specifications</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Similar Products</span>
                                    </li>
                                </ul>
                                <div class="brk-tabs-content">
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                
                                                    The Vivo Y91i smartphone is here to make your life simpler. Its Halo Fullview Display paves the way for an enhanced viewing experience. Its 13 MP rear camera and the AI-powered 5 MP front camera, ensure that every picture you click is extremely likeable. With the presence of a whopping 4030 mAh battery, this phone will keep you engrossed, even when you are on the go.


                                                </p>
                                                
                                            </div>
                                            <div class="col-md-12 col-lg-12">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    An Expansive Viewing Experience Awaits You
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This smartphone’s 15.8 cm (6.22) Halo FullView display invites you to an expansive viewing experience to make your favourite shows and games even more delightful.


                                                </p>
                                            </div>
                                           
                                            
                                            <div class="brk-sc-divider mt-10 mb-20"></div>

                                         
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    A High-capacity Battery to Keep You Going

                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    With the presence of the 4030 mAh battery, this smartphone lets you work, play, and post, even when you are on the go, without worrying about the charge remaining. 
                                                </p>
                                            </div>
                                            <div class="brk-sc-divider mt-10 mb-20"></div>

                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    A Rear Camera to Capture Your Favourite Moments

                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    You can convert moments into memories using this smartphone’s 13 MP rear camera. It also facilitates beautiful bokeh shots.


                                                </p>
                                                </div>
                                            
                                        
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    A Front Camera for Picture-perfect Selfies
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This smartphone’s 5 MP front camera uses AI photo algorithms to automatically make facial enhancements to make every picture you click look perfect.


                                                </p>
                                            </div>
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                          
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    A Power-packed Performance Awaits You
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This smartphone is powered by the MediaTek Helio P22 Octa-core processor, with a clock speed of up to 2.0 GHz, that paves the way for faster processing while consuming less power. It also lets you open apps faster and switch between them seamlessly. This phone comes with 2 GB of RAM and 32 GB of memory to let you store all your files in one place.


                                                </p>
                                            </div>
                                        
                                            <div class="brk-sc-divider mt-10 mb-20"></div>
   
                                            <div class="col-md-12 col-lg-12 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">
                                                    A Easier Way to Unlock Your Phone

                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This smartphone comes with the Face Access technology which lets you unlock your phone instantly, the moment you pick it up.


                                                </p>
                                            </div>
                                       
                                         
                                        </div>
                                    </div>
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-16 font__weight-bold line__height-32">General</h2>
                                                <hr class="underline-white">
                                            </div>
                                        
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Model Number
                                                </h2>
                                                <p>vivo 1820

                                                </p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Model Name
                                                </h2>
                                                <p>Y91i</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Color
                                                </h2>
                                                <p>Fusion Black

                                                </p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Browse Type
                                                </h2>
                                                <p>Smartphones</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    SIM Type
                                                </h2>
                                                <p>Dual Sim</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Hybrid Sim Slot
                                                </h2>
                                                <p>No</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Touchscreen</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    OTG Compatible</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    Quick Charging
                                                </h2>
                                                <p>No</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-3">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">
                                                    SAR Value
                                                </h2>
                                                <p>Head: 1.283 W/kg, Body: 0.609 W/kg

                                                </p>
                                                <hr class="underline-white">
                                            </div>
                                           
                                            <div class="mb-3 col-md-6 col-lg-10">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">In The Box</h2>
                                                <p>
                                                    Handset, User Manual, Micro-USB to USB Cable, USB Power Adapter, SIM Ejector Pin, Protective Case, Protective Film (1 Applied)

                                                </p>
                                                    <hr class="underline-white">
                                            </div>
                                            

                                        </div>
                                    </div>
                                      


                                    <div class="brk-tab-item pt-35">
                                        <div class="brk-grid__item Vivo" style="max-height:550px;">
                                            <div class="container mt-50">
                                                
                                                <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows": false, "responsive": [
                                        {"breakpoint": 1200, "settings": {"slidesToShow": 3}},
                                        {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                                        {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                                        {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                                        {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                                        ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/vivo-v17-vivo-1919-pd1948f-ex-original-imafmth6ezsgmfgj.jpg')}}" alt="iFFalcon tv by TCL">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo V17 (8GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/vivo-s1-pro-vivo-1920-original-imafnhwp7zhvmqkk.jpg')}}" alt="vivo mobiles distributors">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo S1 Pro (8GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/s1-64-d-1907-pd1913f-ex-vivo-6-original-imafgyfqfgswgq4w.jpg')}}" alt="tata chemicals distributors">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo S1 (4GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/vivo-y19-vivo-1915-pd1934f-ex-original-imafmcpd6cezg94d.jpg')}}" alt="telecom service distributors">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y19 (4GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/y91i-32-c-1820-vivo-2-original-imafegpdpjymsrwe.jpg')}}" alt="distributors in Pune">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y91i (2GB+32GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/y91i-32-d-1820-vivo-2-original-imafegpd6zyrmaqh.jpg')}}" alt="consumer goods company">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y91i (3GB+32GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/y15-64-a-1901-vivo-4-original-imafh432tfgwqatf.jpg')}}" alt="Electronic Products">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y15 (4GB+128GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/y12-64-b-1904-pd1901ef-ex-vivo-3-original-imafh4zjvmpfdy7v.jpg')}}" alt="consumer goods">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y12 (3GB+64GB)
                                                                </p>
                                                             
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="swiper-slide">
                                                            <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                <img original src="{{('public/img/products/vivo-y11-vivo-1906-pd1930cf-ex-original-imafngxhtqbfdydg.jpg')}}" alt="consumer goods company in India">
                                                                <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                    Vivo Y11 (3GB+32GB)
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 
                                                </div>
                                            </div>
                                        </div>
                                     
                                    </div>


                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

    </main>
</div>

@endsection
              