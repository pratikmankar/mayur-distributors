@extends('layouts.layout')

@section('title')
Procurement | Supply Chain | A distribution partner of consumer products
@endsection

@section('metas')
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1">
<meta name="theme-color" content="#2775FF">
<meta name="title" content="Procurement | Supply Chain | A distribution partner of consumer products">
<meta name="description" content="Mayur Distributors services are distributorship. Serviced approximately 5000 Retailers and Wholesalers in the entire Maharashtra.✓Get a Free Quote Today 020-26430632">
<meta name="keywords" content="mayur distributors, consumer goods, consumer goods company in india, consumer goods company, Electronic Products, telecom service distributors, distributors in pune, vivo mobiles distributors, tata chemicals distributors, iFFalcon tv by TCL, iFFalcon tv, smart led tv, led tv, micromax mobiles distributors, nikon india, nikon distributors">
<link rel="canonical" href="{{url('/about-us')}}">
<meta property="og:title" content="Procurement | Supply Chain | A distribution partner of consumer products">
<meta property="og:type" content="website">
<meta property="og:url" content="http://mayurdistributors.in/about-us">
<meta property="og:image" content="{{URL::to('public/img/mayur-distributors.png')}}">
<meta property="og:image:alt" content="Mayur Distributors">
<meta property="og:description"content="Mayur Distributors services are distributorship. Serviced approximately 5000 Retailers and Wholesalers in the entire Maharashtra.✓Get a Free Quote Today 020-26430632">
<meta property="og:site_name" content="Mayur Distributors">
<meta name="language" content="english">
<meta name="robots" content="index, follow">
<meta name="distribution" content="global">
<meta http-equiv="content-language" content="en-us">
@endsection

@section('headers')
<style>
    .bg-strengths{
        background: url("{{URL::to('public/img/bg-patterns/bg-strength.jpg')}}");
        background-size:100%;
        padding:40px 0px;
        background-attachment: fixed;
    }
    .timeline_hidden{
        display:none;
    }
</style>
@endsection

@section('content')
<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-48 line__height-68 font__family-montserrat">
                            About Us
                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">About Us</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <main class="main-container">
        <section class="brk-left-overflow-image">
            <div class="container">
                <div class="row justify-content-center no-gutters">
                    <div class="col-xl-5 position-relative">
                        <div class="brk-z-index-10 pt-xl-20 pt-100 mt-80">
                            <div id="about-logo"></div>
                        </div>
                    </div>
                    <div class="col-xl-7">
                        <div class="d-flex flex-column pb-md-20 pb-sm-20 pb-20 pl-xl-30 pt-40">
                            <div class="text-center text-xl-left">
                                <h2 class="font__family-montserrat font__weight-bold font__size-45 line__height-60 mt-10"
                                    data-brk-library="component__title">
                                    <span class="highlight-massive font__weight-bold">
                                        Who We Are?<span class="after wow zoomIn"></span>
                                    </span>
                                </h2>
                            </div>
                            <h4 class="mt-5">Mayur Distributors is been in the Distribution sector since 1975.
                            </h4>
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                Mayur distribution has its roots in the industry since 1975. We at Mayur Distribution started off in the niche of distribution with the intent to have the latest and trendy Global technology, soonest at the local level. 
                            </p>
                            
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                In our initial stages, we started with FMCG for renowned brands namely - Britannia, Nestle, Cadbury’s. Working with such brands, our expertise and ethics in the distributorship business got stronger by each passing day. 

                            </p>
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                It was early 2001 when we actually stepped into tech product distributorship. Back then, idea cellular SIM card was our distribution product which was quite in a demand. Thus started our journey of Mobiles and laptops distribution thereon. With our previous hands-on experience in distributorship, we earned good remarks from brands like HTC, Motorola, Gionee, Nokia and so. We worked as a Nokia mobile distributor for 9 years. 
                            </p>
                        
                          
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <p class="brk-dark-font-color font__size-16 line__height-26 text-center text-lg-left">
                            With so much of reputation and compliments, we are now associated as a distribution partner of Vivo Mobiles, Micromax, Panasonic & Canon camera. Since 2017, we also started with the Nikon Cameras distribution and Iffalcon by TCL in the Division of entire Maharashtra. 
                        </p>
                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-15 text-center text-lg-left">
                        Apart from this, we are also distribution partners of Tata chemicals, for their products - Tata Salt, Pulses and other spices across Maharashtra & Goa.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="brk-left-overflow-image pt-60 pb-20">
            <div class="container">
                <div class="row justify-content-center no-gutters">
                    <div class="col-xl-7 position-relative">
                        <div class="text-center text-xl-left">
                            <h2 class="font__family-montserrat font__weight-bold font__size-45 line__height-60 mt-10"
                                data-brk-library="component__title">
                                <span class="highlight-massive font__weight-bold">
                                    What We Do?<span class="after wow zoomIn"></span>
                                </span>
                            </h2>
                        </div>
                            <h5 class="mt-5">We distribute not the brands with fellows, but indeed distribute the development of fellows to the nation.
                            </h5>
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                Acing the distribution of the brands from manufacturing end to the user end is what we have excelled in for years. We exercise the Management of product supply, to quench the demand.  We are walking a mile for Maintaining healthy relations with manufacturers, distributors, and retailers. For our business values lie in distributing the products happily, neatly, and timely. With the smooth team and smooth relations, happens the smooth distribution. 

                            </p>
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                
We are encouraging and implementing all the essential Business Intelligence tools and the smart tools that could make our distribution any better at any time.

                            </p>
                            
                         
                    </div>
                    <div class="col-xl-5" style="vertical-align:middle;">
                    <img class="abt-us-img" src="{{URL::to('public/img/about-us/distribute.png')}}" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section class="bg-white">
			<div class="text-center mb-50">
				<h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 brk-library-rendered" data-brk-library="component__title">OUR CULTURE</h2>
				<div class="divider-cross wow zoomIn brk-library-rendered" data-brk-library="component__title" style="visibility: visible; animation-name: zoomIn;">
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="container">
                <div class="row">
                  
                    <div class="col-xl-5">
                    <img src="{{URL::to('public/img/about-us/culture.png')}}" alt="" class="img-fluid">
                    </div>		
                    <div class="col-xl-7">
                        <h5>With the right environment and accessories, happens the great business</h5>
                        <hr>
                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                            We believe in building a team and working in a particular environment brings the best out of everyone. We are all set with the fully furnished Offices area of 5000 sq.ft. located at Shankarseth road, Pune
                        </p>
                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                            Our Godown area is 700 sq.ft. located at Shankarseth road, Pune. We have Training & 3 Conference Rooms 2000 Sq.ft. equipped with Projectors. And an ample area for the Warehouse with 3000 Sq.ft at Wagholi.
                        </p>
                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                            We are implementing and deploying the tools and software that make our business perform better. 
                            Here’s a listing of our assisting tools that make our business happen successful and satisfactory, all the time:    
                        </p>
                        <ul class="mt-30">
                            <li>
                                <p class="pl-20 mt-10 font__size-16 brk-dark-font-color">
                                    &bull; Bizz Analyst Mobile App for business & complete accounts analysis.
                                </p>
                            </li>
                            <li>
                                <p class="pl-20 mt-20 font__size-16 brk-dark-font-color">
                                    &bull; PowerBI for 360-degree sales analysis with both numerical & graphical views.
                                </p>
                            </li>
                            <li>
                                <p class="pl-20 mt-20 font__size-16 brk-dark-font-color">
                                    &bull; Access to Lease line and Wi-Fi 4G connectivity
                                </p>
                            </li>
                            <li>
                                <p class="pl-20 mt-20 font__size-16 brk-dark-font-color">
                                    &bull; LAN Connectivity and Windows Server 2016
                                </p>
                            </li>
                            <li>
                                <p class="pl-20 mt-20 font__size-16 brk-dark-font-color">
                                    &bull; Enterprise Resource Planning software's
                                </p>
                            </li>

                        </ul>
                    </div>	
                    <div class="col-xl-12">
                        <div class="text-center mb-50">
                            <h2 class="font__family-montserrat font__weight-bold font__size-22 line__height-52 mt-30 brk-library-rendered" data-brk-library="component__title">What We Believe?</h2>
                            <p class="font__family-montserrat pl-20 mt-20 font__size-16 font__style-italic brk-dark-font-color font__weight-bold">
                                “Great things are not done by impulse, but by a series of small things brought together.” <br><br>
                                “To accomplish great things, we must not only act but also dream, not only plan but also believe”.
                            </p>
                        </div>
                    </div>	
				</div>
			</div>
        </section>
 
        <section class="bg-white">
			<div class="text-center mb-50">
				<h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52  brk-library-rendered" data-brk-library="component__title">What is our Experience?</h2>
				<div class="divider-cross wow zoomIn brk-library-rendered" data-brk-library="component__title" style="visibility: visible; animation-name: zoomIn;">
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="container">
                <div class="row">
                    <div class="col-xl-7 col-md-12">
                        <h5>With each brand that we distributed, we came out distributing even smarter. </h5>
                        <hr>
                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                            Starting right from the year 1975 - we started with Cadbury and Britannia. We served them successfully till 1992 and 2001 respectively. From 1996 to 2001 we were distributing Novartis. 
                            From 2002 to 2018, we were distributing Idea cellular sims across India. Brands like HTC, Nokia, Motorola, Panasonic, and Apple were distributed with us in the period between 2006-08, 2008-15, 2006-08, 2015-18, 2017-19 respectively. Cameras of Canon were also distributed through us from the year 2009 to 2018.
                        </p>
                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                            From 2004 to date, we have been the most trusted distribution partner of Tata chemicals. And similarly been the trusted partners with Nikon, Vivo, Micromax, and Iffalcon TV by TCL, from 2018, 2015, 2017, and 2019 to date, respectively.
                        </p>
                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                            Mr. Sharad Shah - MBA In Marketing & Finance, who carries a rich Business Experience of more than 30 years in the distribution realm, is directing and managing Mayur Distribution with the total manpower strength of more than 80 people.
                        </p>
                    </div>
                    <div class="col-xl-5" style="vertical-align:middle;">
                        <img class="abt-us-img" src="{{URL::to('public/img/about-us/experience.png')}}" class="img-fluid" alt="">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                            We have a result-oriented director who manages the cost-effectively and establishes the strategies that mutually benefit and nurture partnerships and relationships with users, vendors, and Clients. 
                        </p>
                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                            We have a skillful team that creates strategic alliances with Organization Leaders to effectively align with and boost the Business Initiatives. A Highly Trained Sales Team with Complete Knowledge of Products and Market. And an Efficient Backend Team that Manages Daily Reporting, Claim Processing, Accounting & Analytical Works.
                        </p>
                    </div>
                </div>
			</div>
        </section>
 


        <div class="mt-20 mt-md-20">
          
            <div class="bg-timeline pt-40 ">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-xl-5 col-md-12">
                          
                            <img class="abt-us-img" src="{{URL::to('public/img/about-us/team.png')}}" class="img-fluid" alt="">
                           
                        </div>
                        <div class="col-lg-7 col-xl-7 col-md-12">
                            <div class="text-center text-xl-left mb-40">
                                <h2 class="font__family-montserrat font__weight-bold font__size-32 line__height-60"
                                    data-brk-library="component__title">
                                    <span class="highlight-massive font__weight-bold">
                                        Our Story<span class="after wow zoomIn"></span>
                                    </span>
                                </h2>
                            </div>
                            <h4>
                                Connecting lives with lifestyle
                            </h4>

                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                Products that you crave for and the products that enhance your living, are distributed with our fingerprints on it. We distribute Consumer Goods, Electronic Products, and Telecom Services across Maharashtra. Our domains of distributorship were diversifying and thus our experiences with them. By each new distributorship partnering, we came out to be stronger than before. 
                            </p>
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                Our distributorship business is a Professional Management with the Involvement of three Family Members. Besides having a strong Financial Background with a capacity to Invest our own funds, we had excellent networking and relations with almost all the major retailers of Maharashtra. At Mayur Distributors, we have developed ourselves to touch the excellency of the technical, business performance, business intelligence, and customer experience attributes.
                            </p>
                          
                        </div>
                        <div class="col-lg-12 col-xl-12 col-md-12">
                            <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                With the experience of more than 40 years in distributorship, we are successfully carrying the brands like Nokia/Microsoft, Reliance Jio, Britannia, Cadbury, Novartis India, Canon, Panasonic, Airtel, Sony Ericsson, O2, Motorola, HTC, Apple, Idea Cellular, Tata chemicals, Nikon, Vivo, Micromax, TCL, under our distribution management. 
                            </p>
                        </div>
                    </div>
                    <div class="row mt-40">
                        
                                <div class="col-lg-7 col-xl-7 col-md-12">
                                    <div class="text-center text-xl-left">
                                        <h2 class="font__family-montserrat font__weight-bold font__size-32 line__height-60"
                                            data-brk-library="component__title">
                                            <span class="highlight-massive font__weight-bold">
                                                Our History<span class="after wow zoomIn"></span>
                                            </span>
                                        </h2>
                                    </div>
                                        <h4 class="mt-5">
                                            The farther we go back with our experiences, the farther we go ahead with our expertise!
                                        </h4>
        
                                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                            Starting in 1975, we have dealt with and distributed the luxury of brands to your doorsteps with the sound management of distributorship. From Britannia and Cadbury to Novartis, HTC, we have traveled the distribution path to Vivo Mobiles, NIKON Camera, Tata Chemicals, Micromax Mobiles & Iffalcon Televisions By TCL.

                                        </p>
                                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                            We have also successfully served around 5000 Retailers and Wholesalers in entire Maharashtra. The relation with the team and retailers to excel in their performance was started to be in practice from our very initial days itself. On the foundation of strong business ethics, we have built an intelligent business as of today. 
                                        </p>
                                        <p class="brk-dark-font-color font__size-16 line__height-26 mt-20 text-center text-lg-left">
                                            We mark the Excellent Long-Lasting Relationship with Bankers since 1956. The relation and networking that we have had for decades, holds our largest share of business success. 
                                        </p>
                                </div>
                                <div class="col-lg-5 col-xl-5 col-md-12 pt-40">
                                    <img class="abt-us-img" src="{{URL::to('public/img/about-us/story.png')}}" class="img-fluid" alt="">
                                   
                                </div>
                    </div>
    
                </div>
            <div class="container pt-20">
                <div class="container mb-50">
                    <div class="text-center">
                        <h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30" data-brk-library="component__title">OUR JOURNEY</h2>
                        <div class="divider-cross wow zoomIn" data-brk-library="component__title">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="timeline timeline--vertical-squares" data-brk-library="component__timelines_css,component__timelines_js">
                    <div class="timeline__wrapper">
                        <div class="timeline__item">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            1956
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        We mark the Excellent Long-Lasting Relationship with Bankers since 1956. The relation and networking that we have had for decades, holds our largest share of business success.  
                                    </p>
                                </div>

                                <span class="after"><i class="fal fa-archive"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item">
                            <span class="timeline__active-line"></span>
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                              
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            1975
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                   
                                        Starting in 1975, we have dealt with and distributed the luxury of brands to your doorsteps with the sound management of distributorship. 
                                    </p>
                                </div>
                                <span class="after"><i class="fa fa-eyedropper"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            1975
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        With Cadbury and Britannia products, our start happened sweet. We served them successfully till 1992 and 2001 respectively. 
                                    </p>
                                </div>
                                <span class="after"><i class="fa fa-fire"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            1996 to 2001
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        The opportunity of working with Novartis was not only a deal with a new product, but we were entering into a new distribution domain. It was a shift from food to the pharmaceutical industry.
                                    </p>
                                </div>
                                <span class="after"><i class="fal fa-archive"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2002 to 2018
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        We distributed idea cellular sims across India. 

                                    </p>
                                </div>
                                <span class="after"><i class="fa fa-fire"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2006-08
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        From cellular sims to cellular phones, the journey of distribution was again progressing. 
                                    </p>
                                </div>
                                <span class="after"><i class="fal fa-archive"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2008-15
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        Nokia mobile phones were distributed to the entire Maharashtra, through us. 
                                    </p>
                                </div>
                                <span class="after"><i class="fa fa-fire"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2015-18
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        With hands-on experience in electronics distributorship, we had yet another brand to partner with Panasonic.
                                    </p>
                                </div>
                                <span class="after"><i class="fal fa-archive"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2017-19
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        In the electronics domain, we were experienced in distributing the products on time and securely. Becoming a distributor-partner of Apple was indeed our pleasure. 
                                    </p>
                                </div>
                                <span class="after"><i class="fa fa-fire"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2009-18
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        We distributed the Cameras from Canon across Maharashtra. 

                                    </p>
                                </div>
                                <span class="after"><i class="fal fa-archive"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            From 2004 to date
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        We have been the most trusted distribution partner of Tata chemicals. 
                                    </p>
                                </div>
                                <span class="after"><i class="fa fa-fire"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2015 to date
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        Distributing Vivo mobile devices and laptops.
                                    </p>
                                </div>
                                <span class="after"><i class="fal fa-archive"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2017 to date
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        Mobile phone distributorship of Micromax.
                                    </p>
                                </div>
                                <span class="after"><i class="fa fa-fire"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2018 to date
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        Cameras ranging from basic to advance to the latest and the most trending ones are all distributed safely with us.

                                    </p>
                                </div>
                                <span class="after"><i class="fal fa-archive"></i></span>
                            </div>
                        </div>
                        <div class="timeline__item timeline_hidden" id="">
                            <div class="timeline__box">
                                <span class="before brk-base-bg-gradient-bottom-blue"></span>
                                <img class="lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/370x370_1.jpg')}}" alt="alt">
                                <div class="timeline__content text-left brk-dark-font-color">
                                    <div class="timeline__date">
                                        <div class="font__family-montserrat text-left font__weight-bold font__size-20 brk-black-font-color line__height-h2">
                                            2019 to date
                                        </div>
                                    </div>
                                    <p class="mt-25 text-left font__family-oxygen font__weight-medium font__size-16 letter-spacing--1 line__height-26">
                                        Distributing Iffalcon Televisions by TCL, with utmost safety and security.
                                    </p>
                                </div>
                                <span class="after"><i class="fa fa-fire"></i></span>
                            </div>
                        </div>
                      
                    </div>
                    <div class="timeline__progress-bar">
                        <button type="button" id="show_timeline" class="btn btn--bg__icon font__family-open-sans font-weight-bold font__size-16 line__height-16" data-brk-library="component__button">
                            <i class="far fa-sync"></i>
                            LOAD MORE
                        </button>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="pt-90 text-center position-relative" data-brk-library="component__social_block,component__button">
          
            <span class="brk-abs-overlay brk-light-gradient-90deg-94"></span>
            <div class="container all-light">
                <div class="text-center">
                    <h3 class="brk-white-font-color font__family-montserrat font__size-56 font__weight-bold lh-60">Our Socials</h3>
                </div>
                <div class="row no-gutters mt-70">
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://business.facebook.com/mayurdistributors/?business_id=2833294386752219"><i class="brk-icon slide-bg-wrap fab fa-facebook-f"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">Facebook</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://www.instagram.com/mayur_distributors/"><i class="brk-icon slide-bg-wrap fab fa-instagram"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">Instagram</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://twitter.com/MayurDistribut1"><i class="brk-icon slide-bg-wrap fab fa-twitter"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">Twitter</h4>
                        </div>
                    </div>
                    
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="#"><i class="brk-icon slide-bg-wrap fab fa-linkedin"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">LinkedIn</h4>
                        </div>
                    </div>
                    
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://www.google.co.in/search?q=Mayur+Distributors&ludocid=10682126935951604583&lsig=AB86z5VF4Sn_EXvgolzQhi3zgSSY#fpstate=lie"><i class="brk-icon slide-bg-wrap fab fa-google"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">Google</h4>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="social__icon-square">
                            <a href="https://api.whatsapp.com/send?phone=917774076072"><i class="brk-icon slide-bg-wrap fab fa-whatsapp"><span class="slide-bg"></span></i></a>
                            <h4 class="font__family-montserrat font__size-16 text">WhatsApp</h4>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>  

       
        

        {{-- <section class="mb-5 mt-5">
            <div class="brk-tabs brk-tabs_tabbed-intro" data-index="2" data-brk-library="component__tabbed_contents">
                <ul class="brk-tabs-nav">
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/nikon.jpg')}}" alt="micromax mobiles distributors" style="width:120px;"> </li>
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/tata.jpg')}}" alt="nikon india" style="width:120px;"> </li>
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/vivo.jpg')}}" alt="nikon distributors" style="width:120px;"> </li>
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/micromax.jpg')}}" alt="telecom service distributors" style="width:120px;"> </li>
                    <li class="brk-tab"><img src="{{URL::to('public/img/brands/iffalcon.jpg')}}" alt="iFFalcon tv" style="width:120px;"> </li>
                </ul>
                <div class="brk-tabs-content position-relative">
                    <div class="brk-abs-overlay" data-bg="{{URL::to('public/img/bg-patterns/3.png')}}">
                        <span class="brk-abs-overlay brk-bg-grad opacity-95"></span>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-xl-left text-center brk-white-font-color mb-20 mt-10">Mayur Distributors</h4>
                                    <p class="font__size-16 line__height-26 text-xl-left text-justify">Nikon is a Japanese multinational corporation headquartered in Tokyo, Japan, specializing in optics and imaging products.
                                        <br>Nikon is the name trusted by millions of photographers around the world. With our cutting-edge innovations and relentless pursuit of quality, Nikon products are one of the global leader in digital imaging and optics. The company is the eighth-largest chip equipment maker as reported in 2017.
                                        <br>Nikon's products include cameras, camera lenses, binoculars, microscopes, ophthalmic lenses, measurement instruments, rifle scopes, spotting scopes, and the steppers used in the photolithography steps of semiconductor fabrication, of which it is the world's second largest manufacturer.
                                        <br>Mayur Distributors is associated with Nikon since 2017 as authorized Super Distributor for South & Central Maharashtra.
                                        </p>
                                </div>
                                
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/nikon.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/nikon.jpg')}}" class="image-border-1 lazyload" alt="telecom service distributors">
                                     
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-xl-left text-center brk-white-font-color mb-20 mt-10">Mayur Distributors</h4>
                                    <p class="font__size-16 line__height-26 text-xl-left text-justify">
                                        
                                        Tata Chemicals Limited is an Indian global company with interests in chemicals, crop nutrition and consumer products. The company is one of the largest chemical companies in India with significant operations in India and Africa. Tata Chemicals is a subsidiary of Tata Group conglomerate.
                                        <br> Tata Chemicals have the largest salt works in Asia, and are the 3rd largest soda ash manufacturer and the 6th largest sodium bicarbonate manufacturer in the world.
                                        <br> Tata Chemicals has huge range of products in various ( ) such as Spices, Chemicals, Fertilizers, Consumer Products, Biofuels, Cement, Pulses, Nutrition solutions, water purifiers etc.
                                        <br> Mayur Distributors is associated with Tata Chemicals since 2004 as Authorized Super Distributor for 70% area of Maharashtra state & entire Goa State. And takes care of products like Tata Salt, Tata Lite, I-shakti, Soda, Besan & all Pulses for above said area.

                                    </p>
                                </div>
                                
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/tata.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/tata.jpg')}}" class="image-border-1 lazyload" alt="vivo mobiles distributors">
                                     
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-lg-left text-center brk-white-font-color mb-20 mt-10">Harsh Communication</h4>
                                    <p class="font__size-16 line__height-26 text-lg-left text-center">Vivo Communication Technology Co. Ltd. is a Chinese technology company owned by BBK Electronics that designs and manufactures smartphones and smartphone accessories in China, software and online services.

                                        <br>
                                        Vivo products are one of the preferred brands of young people around the world.
                                        <br>Vivo launched its products in India in 2015 and since then Harsh Communication is associated with Vivo.
                                        </p>
                                </div>
                               
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/vivo.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/vivo.jpg')}}" class="image-border-1 lazyload" alt="Electronic Products">
                                     
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8 col-lg-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-lg-left text-center brk-white-font-color mb-20 mt-10">Mayur Telecom Pvt. Ltd.</h4>
                                  <p class="text-white">
                                    Micromax Informatics Limited is one of the leading consumer electronics company in India, and the 10th largest mobile phone player in the world. The company has many firsts to its credit when it comes to the mobile handset market; including the 30-day battery backup, Dual SIM Dual Standby phones, QWERTY keypads, universal remote control mobile phones & first quad-core budget smart phone. The brand's product portfolio embraces more than 60 models today, ranging from feature rich, dual-SIM phones, 3G Android smartphones, tablets, LED televisions and data cards.
                                    <br>Since 2018 Mayur Telecom Pvt. Ltd is an authorized Super Distributor of Micromax Mobile for Rest of Maharashtra.
                                    
                                  </p>
                                </div>
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/micromax.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/micromax.jpg')}}" class="image-border-1 lazyload" alt="consumer goods">
                                     
                                    </a>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                    <div class="brk-tab-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-8 col-lg-8">
                                    <h4 class="font__family-montserrat font__weight-semibold font__size-28 text-lg-left text-center brk-white-font-color mb-20 mt-10">Mayur Telecom Pvt. Ltd.</h4>
                                  <p class="text-white">
                                    iFFalcon Technology is responsible for the operations of global smart TV platforms of TCL Multimedia and its subsidiary companies. It is also engaged in the design, production, manufacture and sales of smart TVs with iFFalcon or other new brands. 
                                    <br>Mayur Telecom Pvt Ltd is an authorized Super Distributor of Micromax Mobile for Rest of Maharashtra since 2019.

                                </div>
                                <div class="col-sm-4 col-lg-4 text-center">
                                    <a href="{{URL::to('public/img/brands/iffalcon.jpg')}}" class="frame-image image-link angle-shadow-2 mb-40" style="width:250px;" data-brk-library="component__image_frames">
                                        
                                        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{URL::to('public/img/brands/iffalcon.jpg')}}" class="image-border-1 lazyload" alt="tata chemicals distributors">
                                     
                                    </a>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> --}}
      

        <section>
            <div class="container mb-80 mt-100">
                <div class="text-center">
                    <h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30" data-brk-library="component__title">Our Stregths</h2>
                    <div class="divider-cross wow zoomIn" data-brk-library="component__title">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="bg-strengths">

            <div class="container">
                <div class="row">
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">01.</h4>
                                <p class="text-white">Professional Management With The Involvement Of Three Family Members.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">02.</h4>
                                <p class="text-white">Strong Financial Background With Capacity To Invest Our Own Funds.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">03.</h4>
                                <p class="text-white">Excellent Personal Touch With All Major Retailers Of Pune.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">04.</h4>
                                <p class="text-white">Highly Trained Sales Team With Complete Knowledge Of Products And Market.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">05.</h4>
                                <p class="text-white">Efficient Backend Team Which Can  Manage  Daily Reporting, Claim Processing, Accounting & Analytical Works.</p>
                           
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 mt-3">
                        <article class="brk-tiles-simple" data-brk-library="component__tiles">
                            <div class="before"></div>
                            <div class="brk-tiles-simple__content">
                                <h4 class="font__family-montserrat font__weight-semibold font__size-21">06.</h4>
                                <p class="text-white">Excellent Long Lasting Relationship With Bankers Since Year 1956.</p>
                           
                            </div>
                        </article>
                    </div>
               
                    
                </div>
            </div>
        </div>

        </section>


        <section>
            <div class="container mb-100">
                <div class="text-center">
                    <h2 class="font__family-montserrat font__weight-bold font__size-34 line__height-52 mt-30" data-brk-library="component__title">Vision & Mission</h2>
                    <div class="divider-cross wow zoomIn" data-brk-library="component__title">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="container mb-50">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="flip-box" data-brk-library="component__flip_box">
                            <div class="flip flip_horizontal flip-box__multiply">
                                <div class="flip__front">
                                    <div class="flip-box__multiply-title font__family-montserrat font__weight-bold font__size-58 text-uppercase mt-35 pl-xl-50 pr-xl-50 pl-20 pr-20 lazyload" data-bg="{{URL::to('public/img/about-us/vision.jpg')}}">Our Vision
                                    </div>
                                    <div class="flip-box__multiply-decoration lazyload border-radius-25 ml-xl-50 ml-20 mt-90 mb-40" data-bg="{{URL::to('public/img/about-us/vision.jpg')}}">
                                        <i class="fal fa-angle-right font__size-28 font__weight-thin" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="flip__back flip-box__bg flip-box__bg_overlay lazyload" data-bg="{{URL::to('public/img/about-us/vision.jpg')}}">
                                    <div class="flip-box__position flip-box__position_35 text-center">
                                        <h4 class="flip-box__multiply-h4 font__family-montserrat font__weight-bold font__size-58">01</h4>
                                        {{-- <p class="font__family-open-sans font__size-16 line__height-31 text-white mt-40 pl-30 pr-30">&bullet; To have a Global and Transparent relationship from businesses and brands to the local vendors and resellers.
                                            <br>&bullet; To have an Eco-system in the forte of Distribution of the latest technologies and products.
                                            </p> --}}
                                        <p class="font__family-open-sans font__size-16 line__height-31 text-white mt-40 pl-30 pr-30">
                                            To Be Globally Trusted Brand Creating Opportunities For Companies And Business Partners Which Builds An Eco-system Of Growth By Virtue Of Innovation & Passion.
                                        </p>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="flip-box" data-brk-library="component__flip_box">
                            <div class="flip flip_horizontal flip-box__multiply">
                                <div class="flip__front">
                                    <div class="flip-box__multiply-title font__family-montserrat font__weight-bold font__size-58 text-uppercase mt-35 pl-xl-50 pr-xl-50 pl-20 pr-20 lazyload" data-bg="{{URL::to('public/img/about-us/mission.jpg')}}">Our Mission
                                    </div>
                                    <div class="flip-box__multiply-decoration lazyload border-radius-25 ml-xl-50 ml-20 mt-90 mb-40" data-bg="{{URL::to('public/img/about-us/mission.jpg')}}">
                                        <i class="fal fa-angle-right font__size-28 font__weight-thin" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="flip__back flip-box__bg flip-box__bg_overlay lazyload" data-bg="{{URL::to('public/img/about-us/mission.jpg')}}">
                                    <div class="flip-box__position flip-box__position_35 text-center">
                                        <h4 class="flip-box__multiply-h4 font__family-montserrat font__weight-bold font__size-58">02</h4>
                                        <p class="font__family-open-sans font__size-16 line__height-31 text-white mt-40 pl-30 pr-30">
                                            Our Mission Is To Establish Last Mile Distribution Platform For Companies And Business Partners By Implementing Breakthrough Technologies, Process Improvisation & People Development.
                                        </p>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="flip-box" data-brk-library="component__flip_box">
                            <div class="flip flip_horizontal flip-box__multiply">
                                <div class="flip__front">
                                    <div class="flip-box__multiply-title font__family-montserrat font__weight-bold font__size-58 text-uppercase mt-35 pl-xl-50 pr-xl-50 pl-20 pr-20 lazyload" data-bg="{{URL::to('public/img/about-us/values.jpg')}}">Our Values
                                    </div>
                                    <div class="flip-box__multiply-decoration lazyload border-radius-25 ml-xl-50 ml-20 mt-90 mb-40" data-bg="{{URL::to('public/img/about-us/values.jpg')}}">
                                        <i class="fal fa-angle-right font__size-28 font__weight-thin" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="flip__back flip-box__bg flip-box__bg_overlay lazyload" data-bg="{{URL::to('public/img/about-us/values.jpg')}}">
                                    <div class="flip-box__position flip-box__position_35 text-center">
                                        <h4 class="flip-box__multiply-h4 font__family-montserrat font__weight-bold font__size-58">03</h4>
                                        <p class="font__family-open-sans font__size-16 line__height-31 text-white mt-40 pl-30 pr-30">
                                            We encourage <b>learning</b> at every step,<br> 
                                            We remain <b>passionate</b> and <b>disciplined</b> through our process, <br>
                                            Our distribution process is <br>innovating</b> and <b>excelling</b> at every stage,<br>
                                            <b>Customers’ ease</b> is what we put above all.
                                        </p>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </section>

       
    </main>
</div>
@endsection


@section('scripts')
<script>
    $(document).ready(function(){
        $("#show_timeline").click(function(){
            $(".timeline_hidden").css("display","block");
        });
    });

</script>
@endsection