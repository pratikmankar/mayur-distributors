@extends('layouts.layout')

@section('title')
Mayur Distributors | IFFALCON
@endsection


@section('headers')
    <style>
        .product-name:hover{
            color:#fff;
            transition:.1s;
        }
    </style>
@endsection

@section('content')

<div class="breadcrumbs__section breadcrumbs__section-thin brk-bg-center-cover lazyload" data-bg="{{URL::to('public/img/1920x258_1.jpg')}}" data-brk-library="component__breadcrumbs_css">
    <span class="brk-abs-bg-overlay brk-bg-grad opacity-80"></span>
    <div class="breadcrumbs__wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-12">
                    <div class="justify-content-lg-center">
                        <h2 class="brk-white-font-color text-center font__weight-semibold font__size-28 line__height-68 font__family-montserrat">
                            V2A 163.83cm (65 inch) Ultra HD (4K) QLED Smart Android TV  (65V2A)

                        </h2>
                    </div>
                    <div class="text-center pt-25 pb-35 position-static position-lg-relative">
                      
                        <ol class="breadcrumb font__family-montserrat font__size-15 line__height-16 brk-white-font-color">
                            <li>
                                <a href="{{url('/')}}">Home</a>
                                <i class="fal fa-chevron-right icon"></i>
                            </li>
                            <li class="active">V2A 163.83cm (65 inch) Ultra HD (4K) QLED Smart Android TV  (65V2A)
</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<div class="main-wrapper">
    <main class="main-container pt-30">
        <div class="container">
            <div class="brk-sc-shop-item mb-30" data-brk-library="component__shop_item_page_sidebar">
                <div class="row">
                    <div class="col-12 col-lg-12 col-xl-12">
                        <div class="brk-sc-shop-item__main">
                            <div class="row pb-20 justify-content-md-start justify-content-center">
                                <div class="col-lg-6 col-md-12 mb-4">
                                    
                                    <div class="slider-thumbnailed slick-loading fa-req">
                                        <div class="slider-thumbnailed-for arrows-modern" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "fade": true, "arrows": true, "autoplay": false, "autoplaySpeed": 4200}' data-brk-library="slider__slick" style="min-height: auto">
                                            <div>
                                                <img src="{{('public/img/products/iffalcon/65v2a.jpeg')}}" alt="alt">
                                            </div>
                                            <div>
                                                <img src="{{('public/img/products/iffalcon/65v2a-back.jpeg')}}" alt="alt">
                                            </div>
                                          
                                        </div>
                                        <div class="slider-thumbnailed-nav" data-slick='{"slidesToShow": 2, "slidesToScroll": 1}'>
                                           
                                        </div>
                                    </div>
                                    
                            </div>
                                <div class="col-lg-6 col-md-12 mt-4">
                                    <div class="brk-sc-shop-item__info">
                                        <div class="brk-sc-category d-flex align-items-baseline mb-15" data-brk-library="component__elements">
                                            <span class="brk-sc-category__title font__family-montserrat font__size-14 font__weight-bold mr-10 text-uppercase">
                                                Category:
                                            </span>
                                            <div class="brk-sc-category__items d-flex align-items-center flex-wrap">
                                                <a href="javascript:void();" class="brk-dark-font-color font__family-open-sans font__size-14 mr-1">Televisions</a>
                                            </div>
                                        </div>
                                        <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-light line__height-32">iFFALCON
                                            <br>
                                            <span class="font__weight-bold">V2A 163.83cm (65 inch) Ultra HD (4K) QLED Smart Android TV  (65V2A)
</span>
                                        </h2>
                                        <div class="brk-sc-divider mt-10 mb-20"></div>
                                        <div class="row no-gutters">
                                            <div class="col-lg-12">
                                                <h3 class="brk-color-filter__title font__family-montserrat font__weight-bold font__size-16 text-uppercase text-left">
                                                    <span>Highlights</span>
                                                </h3>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Supported Apps:</span>
                                                </h3>
                                                <p>Netflix|Prime Video<br>Disney+Hotstar|Youtube</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Operating System:</span>
                                                </h3>
                                                <p>Android (Google Assistant & Chromecast in-built)
                                                </p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Resolution:</span>
                                                </h3>
                                                <p>Ultra HD (4K) 3840 x 2160 Pixels</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Sound Output:</span>
                                                </h3>
                                                <p>30 W</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Refresh Rate:</span>
                                                </h3>
                                                <p>60 Hz</p>
                                            </div>
                                            <div class="col-lg-4 mt-2">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>Smart TV</span>
                                                </h3>
                                                <p>Yes</p>
                                            </div>
                                            <div class="col-lg-12 mt-3">
                                                <h3 class="font__family-montserrat font__size-14 text-uppercase text-left">
                                                    <span>In the box:</span>
                                                </h3>
                                                <p>1 TV Unit, Remote, 2 Battery, Manual Book, Warranty Card, Power Cord, Table Stand</p>
                                            </div>
                                            <div class="col-lg-12">
                                                <hr class="underline-white">
                                                <div class="text-center text-lg-left mt-5">
                                                    <a href="{{url('/contact-us')}}" class="btn btn-inside-out btn-lg border-radius-25 btn-shadow ml-0 pl-50 pr-50 brk-library-rendered rendered" data-brk-library="component__button" tabindex="-1">
                                                        <span class="before">Enquire Now!</span><span class="text" style="min-width: 90.4375px;">Enquire Now!</span><span class="after">Enquire Now!</span>
                                                    </a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="brk-tabs brk-tabs_canted" data-brk-library="component__shop_item_page_sidebar">
                                <ul class="brk-tabs-nav font__family-montserrat font__size-14 font__weight-semibold line__height-14 text-uppercase">
                                    <li class="brk-tab">
                                        <span>Description</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Specifications</span>
                                    </li>
                                    <li class="brk-tab">
                                        <span>Similar Products</span>
                                    </li>
                                </ul>
                                <div class="brk-tabs-content">
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    Your home entertainment will become bigger, better, and true-to-life with this powerful and stunning iFFalcon 4K UHD TV. This essential entertainment appliance features a 3,840 x 2,160 Pixels Display, an Onkyo Speaker, and the Quantum Dot Technology for an immersive experience in your living room or bedroom.
                                                </p>
                                                
                                            </div>
                                            <div class="col-md-12 col-lg-7">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">Quantum Dots Technology</h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    This technology offers true-to-life colors and enhances the picture quality. It increases the color volume by upscaling the NTSC color range by up to 103%. Also, the 10-bit panel delivers 1.07 billion color details.
                                                </p>
                                            </div>
                                            <div class="col-md-12 col-lg-5">
                                                <img src="{{('public/img/products/iffalcon/d-1.jpeg')}}" class="img-fluid">
                                            </div>
                                            
                                            <div class="brk-sc-divider mt-10 mb-20"></div>

                                            <div class="col-md-12 col-lg-5">
                                                <img src="{{('public/img/products/iffalcon/d-2.jpeg')}}" class="img-fluid mb-4">
                                            </div>
                                            <div class="col-md-12 col-lg-7 mt-4">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-28 font__weight-bold line__height-32">HDR Pro
                                                </h2>
                                                <hr class="underline-white">
                                                <p class="brk-dark-font-color font__family-open-sans font__size-16 line__height-26 font__weight-normal mb-45">
                                                    HDR offers detailed images via accurate reproduction of dark and light shades. This TV supports HDR 10 to offer improved brightness, contrast, and rich colors.


                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-16 font__weight-bold line__height-32">General</h2>
                                                <hr class="underline-white">
                                            </div>
                                        
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Model Name</h2>
                                                <p>65V2A</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Display Size</h2>
                                                <p>163.83 cm (65)</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Screen Type</h2>
                                                <p>QLED</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Resolution</h2>
                                                <p>Ultra HD (4K), 3840 x 2160</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">3D</h2>
                                                <p>No</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Smart TV</h2>
                                                <p>Yes</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Curve TV</h2>
                                                <p>No</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Series</h2>
                                                <p>V2A</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Motion Sensor</h2>
                                                <p>No</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">HDMI</h2>
                                                <p>3</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">USB</h2>
                                                <p>2</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Built In Wi-Fi</h2>
                                                <p>2.4GHz, 802.11 b/g/n 2T2R</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-4 col-lg-2">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">Launch Year</h2>
                                                <p>2019</p>
                                                <hr class="underline-white">
                                            </div>
                                            <div class="mb-3 col-md-6 col-lg-10">
                                                <h2 class="font__family-montserrat brk-black-font-color font__size-14 font__weight-bold line__height-32">In The Box</h2>
                                                <p>1 TV Unit, Remote, 2 Battery, Manual Book, Warranty Card, Power Cord, Table Stand</p>
                                                    <hr class="underline-white">
                                            </div>
                                            

                                        </div>
                                    </div>
                                    <div class="brk-tab-item pt-35">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="default-slider slick-loading default-slider_big default-slider_no-gutters arrows-classic-ellipse-mini fa-req text-center" data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "arrows": false, "responsive": [
                                                    {"breakpoint": 1200, "settings": {"slidesToShow": 4}},
                                                    {"breakpoint": 992, "settings": {"slidesToShow": 3}},
                                                    {"breakpoint": 768, "settings": {"slidesToShow": 3}},
                                                    {"breakpoint": 576, "settings": {"slidesToShow": 1}},
                                                    {"breakpoint": 375, "settings": {"slidesToShow": 1}}
                                                    ], "autoplay": true, "autoplaySpeed": 3000}' data-brk-library="slider__slick">
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/iffalcon-1080p-wide-angle-12mp-waterproof-under-water-portable-original-imafq2fncukhwrc4.jpg')}}" alt="">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/iffalcon-32F2A')}}"> iFFALCON 32F2A</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/iffalcon-43k31-original-imafm59fudczqhuy.jpg')}}" alt="mayur distributors">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/index')}}"> iFFALCON 43K31</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/iffalcon-50k31-original-imafm59fmsdhh8uj.jpg')}}" alt="Tata Chemicals distributors">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/index')}}"> iFFALCON 50K31</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/iffalcon-1080p-wide-angle-12mp-waterproof-under-water-portable-original-imafq2fncukhwrc43.jpg')}}" alt="iFFalcon Televisions by TCL">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/index')}}"> iFFALCON 40F2A</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/iffalcon-55k31-original-imafm59fa7ftgpkm.jpg')}}" alt="Vivo Mobiles">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/index')}}"> iFFALCON 55K31</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/iffalcon-65v2a-65v2a-original-imafm59fpm5wnxbb.jpg')}}" alt="Micromax Mobiles">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/index')}}"> iFFALCON 65V2A</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/iffalcon-1080p-wide-angle-12mp-waterproof-under-water-portable-original-imafq2fncukhwrc44.jpg')}}" alt="distribution partner">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/index')}}"> iFFALCON 49F2A</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="swiper-slide">
                                                                <div class="info-box-icon-simple d-flex flex-column align-items-center pt-10 pb-10 pr-10 pl-10 position-relative" data-brk-library="component__info_box">
                                                                    <span class="brk-abs-overlay brk-base-bg-gradient-50deg"></span>
                                                                    <img original src="{{('public/img/products/iffalcon-led-32e3-1.jpg')}}" alt="Nikon India">
                                                                    <p class="font__family-montserrat info-box-icon-simple__title font__size-24 font__weight-bold line__height-30 pt-20 mb-20 text-center">
                                                                       <a class="product-name" href="{{url('/index')}}"> iFFALCON 32E3</a>
                                                                    </p>
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                      
                                                     
                                                    </div>
                                            </div>
                                        
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

    </main>
</div>

@endsection